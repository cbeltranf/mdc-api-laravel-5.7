<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('login/app', 'AuthController@loginApp');
});

//, 'jwt'
Route::middleware(['api'])->group(function () {
    Route::group([
        'prefix' => 'auth'
    ], function () {
        Route::get('refresh', 'AuthController@refresh');
        Route::get('invalidate', 'AuthController@invalidate');
    });

//Admin Profile
    Route::group([
        'prefix' => 'admin'
    ], function () {
        Route::apiResource('user', 'UserController');
        Route::post('user/userlogin', 'UserController@userLoginValidate');
        Route::get('user/company/{id}', 'UserController@CompanyUsers')->middleware('jwt');
        Route::post('user/status', 'UserController@status');
        Route::post('user/goals', 'UserController@goals');
        Route::PUT('user/notification/{id}', 'UserController@Notification');

        Route::apiResource('company', 'CompanyController')->middleware('jwt');
        Route::put('company/location/{id}', 'CompanyController@UpdateLocation');
        Route::apiResource('profile', 'ProfileController')->middleware('jwt');
        Route::apiResource('goal', 'GoalController');
        Route::apiResource('position', 'PositionController')->middleware('jwt');
        Route::apiResource('news', 'NewsController')->middleware('jwt');
        Route::post('news/image', 'NewsController@uploadTMP');
        Route::apiResource('gallery/news', 'NewsGalleryController')->middleware('jwt');

        Route::post('reports/contacts/user', 'ReportsController@contacts');
        Route::post('reports/news', 'ReportsController@news');
        Route::post('reports/companies', 'ReportsController@companies');
        Route::get('reports/dashboard', 'ReportsController@dashboard');
        Route::get('reports/dashboard/kpi2', 'ReportsController@kpi2');
        Route::get('reports/dashboard/users/contacts/general', 'ReportsController@contactGeneral');
        Route::get('reports/dashboard/users/contacts/specific', 'ReportsController@dashboard');
        Route::get('reports/dashboard/users/points', 'ReportsController@points');
        Route::get('reports/dashboard/users/contacts/detail/{id}', 'ReportsController@contactDetail');
        Route::get('reports/dashboard/users/contacts/companies', 'ReportsController@companiesContacts');

        Route::get('reports/dashboard/users/contacts/positions', 'ReportsController@positionsContacts');
    });
});

