/*
SQLyog Ultimate v12.2.4 (64 bit)
MySQL - 5.6.34-log : Database - mydigita_dbv1
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`mydigita_dbv1` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `mydigita_dbv1`;

/*Table structure for table `companycards` */

DROP TABLE IF EXISTS `companycards`;

CREATE TABLE `companycards` (
  `CompanyCardsId` int(11) NOT NULL AUTO_INCREMENT,
  `Company_CompanyId` int(11) NOT NULL,
  `CompanyCardsName` varchar(150) COLLATE utf8mb4_bin NOT NULL,
  `CompanyCardsInfo` text COLLATE utf8mb4_bin NOT NULL COMMENT 'Json Info',
  `CompanyCardsState` enum('1','0') COLLATE utf8mb4_bin DEFAULT '1' COMMENT '1 Active, 0 Inactive',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`CompanyCardsId`),
  KEY `Company_x_companyCards` (`Company_CompanyId`),
  CONSTRAINT `Company_x_companyCards` FOREIGN KEY (`Company_CompanyId`) REFERENCES `company` (`CompanyId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
