/*
SQLyog Ultimate v12.2.4 (64 bit)
MySQL - 5.5.60-cll : Database - mydigita_dbv1
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`mydigita_dbv1` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `mydigita_dbv1`;

/*Table structure for table `APPINFO` */

DROP TABLE IF EXISTS `APPINFO`;

CREATE TABLE `APPINFO` (
  `AppInfoOS` enum('Android','iOS') COLLATE utf8mb4_bin NOT NULL,
  `AppInfoVersion` varchar(30) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`AppInfoOS`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `APPINFO` */

insert  into `APPINFO`(`AppInfoOS`,`AppInfoVersion`) values 
('iOS','1.1.1005'),
('Android','1.1.1005');

/*Table structure for table `COMPANY` */

DROP TABLE IF EXISTS `COMPANY`;

CREATE TABLE `COMPANY` (
  `CompanyId` int(11) NOT NULL AUTO_INCREMENT,
  `CompanyName` char(150) DEFAULT NULL,
  `CompanyExtra` varchar(80) DEFAULT NULL,
  `CompanyCode` char(20) DEFAULT NULL,
  `CompanyStatus` enum('A','S') DEFAULT NULL,
  `Companylogo` varchar(100) DEFAULT 'nologo.png',
  `CompanyAlias` varchar(45) DEFAULT NULL,
  `CompanyAddress` varchar(150) DEFAULT NULL,
  `CompanyPhone` varchar(45) DEFAULT NULL,
  `CompanyLat` varchar(20) DEFAULT NULL,
  `CompanyLog` varchar(20) DEFAULT NULL,
  `CompanyMapPlaceID` varchar(80) DEFAULT NULL,
  `CompanyCityContry` varchar(45) DEFAULT NULL,
  `CompanyWeb` varchar(250) DEFAULT NULL,
  `CompanyFB` varchar(250) DEFAULT NULL,
  `CompanyIG` varchar(250) DEFAULT NULL,
  `CompanyIN` varchar(250) DEFAULT NULL,
  `CompanyTW` varchar(250) DEFAULT NULL,
  `CompanyYT` varchar(250) DEFAULT NULL,
  `CompanyGP` varchar(250) DEFAULT NULL,
  `CompanyColor` varchar(12) DEFAULT NULL,
  `CompanyColorText` varchar(12) DEFAULT NULL,
  `CompanyLogoClass` varchar(100) DEFAULT NULL,
  `CompanyLang` enum('es','en') DEFAULT 'es',
  PRIMARY KEY (`CompanyId`),
  UNIQUE KEY `CompanyAlias` (`CompanyAlias`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `COMPANY` */

insert  into `COMPANY`(`CompanyId`,`CompanyName`,`CompanyExtra`,`CompanyCode`,`CompanyStatus`,`Companylogo`,`CompanyAlias`,`CompanyAddress`,`CompanyPhone`,`CompanyLat`,`CompanyLog`,`CompanyMapPlaceID`,`CompanyCityContry`,`CompanyWeb`,`CompanyFB`,`CompanyIG`,`CompanyIN`,`CompanyTW`,`CompanyYT`,`CompanyGP`,`CompanyColor`,`CompanyColorText`,`CompanyLogoClass`,`CompanyLang`) values 
(1,'Inmov SAS','Sede Principal','MA','A','inmov.png','inmov','Cra 7 No. 156 - 78 Piso 17 ','+57(1)7455152 ','4.732784','-74.023703','ChIJ1TBv1o6PP44RtXfScQLrRlU','Bogotá, Colombia','http://www.inmov.com','https://www.facebook.com/InmovGN/','https://www.instagram.com/inmovgn/','https://www.linkedin.com/company/22328600/','https://twitter.com/InmovGN',NULL,NULL,'#DF4B08','#FFF','img-responsive','es'),
(2,'Mejía Gestión Empresarial','Sede Principal','MG','A','mejia.png','mejiagestion','Calle 100 No 17-09 Piso 4','+57(1)6354566','4.732763','-74.023702',NULL,'Bogotá, Colombia','http://www.mejia.com.co',NULL,NULL,NULL,NULL,NULL,NULL,'#005fbd','#FFF',NULL,'es'),
(4,'Inmov SAS','Sede Barranquilla','MA','A','inmov.png','inmovbq','Carrera 51B No 80-58 Of 909','+57(5)3561441','11.004283','-74.813163','ChIJY3ubvIIt9I4RCtBrpcX9xR0','Barranquilla, Colombia','http://www.inmov.com','https://www.facebook.com/InmovGN/','https://www.instagram.com/inmovgn/','https://www.linkedin.com/company/22328600/','https://twitter.com/InmovGN',NULL,NULL,'#DF4B08','#FFF','img-responsive','es'),
(5,'Mejia Marketing US Inc',NULL,'MU','A','mejia.png','mejiaus','3333 Lee Parkway, Suite 642. Dallas, TX 75219','+19728910233','32.809433','-96.805291',NULL,'Dallas, USA','http://www.inmov.com','https://www.facebook.com/InmovGN/','https://www.instagram.com/inmovgn/','https://www.linkedin.com/company/22328600/','https://twitter.com/InmovGN',NULL,NULL,'#213038','#FFF',NULL,'en'),
(6,'Inmov SAS','Sede Panamá','PA','A','inmov.png','inmovpa','Punta Pacífica, Torres de Las Américas - Torre C, Oficina 1504','+50762381100','8.982921','-79.509578','ChIJw8v1YBuprI8RhWlap7RkPMM','Ciudad Panamá, Panamá','http://www.inmov.com','https://www.facebook.com/InmovGN/','https://www.instagram.com/inmovgn/','https://www.linkedin.com/company/22328600/','https://twitter.com/InmovGN',NULL,NULL,'#DF4B08','#FFF','img-responsive','es'),
(7,'Banco de Bogotá','Sede Principal','BB','A','bdb.png','bdb','Centro Empresarial North Point - Carrera 7 # 156 - 78, Bogotá','+16738585','4.732784','-74.023703',NULL,'Bogotá, Colombia','https://www.bancodebogota.com','https://www.facebook.com/BancodeBogota/','https://www.instagram.com/bancodebogota/','https://www.linkedin.com/company/banco-de-bogota','https://twitter.com/bancodebogota',NULL,NULL,'#004a8d','#FFF',NULL,'es'),
(8,'Inmov SAS','Sede México','MX','A','inmov.png','inmovmx','Carretera México Toluca 5454 Suite 801A','+5563791844','19.362639','-99.282711','ChIJzUwNwDMH0oURVtD0twVE3S4',NULL,'http://www.inmov.com','https://www.facebook.com/InmovGN/','https://www.instagram.com/inmovgn/','https://www.linkedin.com/company/22328600/','https://twitter.com/InmovGN',NULL,NULL,'#DF4B08','#FFF','img-responsive','es'),
(9,'Inmov Global Network','Sede Estado Unidos','US','A','inmov.png','inmovusa','2323 Ross Ave, 17th Floor , Dallas, TX 75201','+1800.329.5702 ','32.9343976','-96.4610694',NULL,NULL,'http://www.inmov.com','https://www.facebook.com/InmovGN/',NULL,NULL,'https://twitter.com/InmovGN',NULL,NULL,'#DF4B08','#FFF',NULL,'en'),
(10,'Claro Empresas','Sede Principal','CE','A','claro.png','claro','Cra 7 No. 156 - 78 Piso 17 ','','32.9343976','-96.4610694',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'#c12a21','#FFF',NULL,'es'),
(11,'AX Marketing','Sede Principal','AXM','A','axm.png','axmarketing','Cra 7 No. 156 - 78 Piso 17 ','+57(1)7455152 ','4.732784','-74.023703',NULL,'Bogotá, Colombia','https://www.axmarketing.com.co/',NULL,NULL,NULL,NULL,NULL,NULL,'#29abe2','#FFF',NULL,'es');

/*Table structure for table `CONTACT` */

DROP TABLE IF EXISTS `CONTACT`;

CREATE TABLE `CONTACT` (
  `ContactId` int(11) NOT NULL AUTO_INCREMENT,
  `ContactCreated_by` int(11) DEFAULT NULL,
  `ContactCreated` datetime DEFAULT NULL,
  `ContactPhone` varchar(45) DEFAULT NULL,
  `ContactName` varchar(150) DEFAULT NULL,
  `ContactCompany` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`ContactId`),
  KEY `ContactCreated_by_idx` (`ContactCreated_by`),
  CONSTRAINT `ContactCreated_by` FOREIGN KEY (`ContactCreated_by`) REFERENCES `USER` (`UserId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `CONTACT` */

/*Table structure for table `DRAW` */

DROP TABLE IF EXISTS `DRAW`;

CREATE TABLE `DRAW` (
  `Movil` char(25) COLLATE utf8mb4_bin NOT NULL,
  `Numero` char(4) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`Movil`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `DRAW` */

insert  into `DRAW`(`Movil`,`Numero`) values 
('3145965211','1000');

/*Table structure for table `MODULE` */

DROP TABLE IF EXISTS `MODULE`;

CREATE TABLE `MODULE` (
  `ModuleId` int(11) NOT NULL AUTO_INCREMENT,
  `ModuleName` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `ModuleIcon` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL,
  `ModuleOrder` int(11) DEFAULT NULL,
  PRIMARY KEY (`ModuleId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `MODULE` */

insert  into `MODULE`(`ModuleId`,`ModuleName`,`ModuleIcon`,`ModuleOrder`) values 
(1,'Configuración','fa-gear',1),
(2,'Enmovimiento','fa-newspaper-o',2);

/*Table structure for table `NEWS` */

DROP TABLE IF EXISTS `NEWS`;

CREATE TABLE `NEWS` (
  `NewsId` int(11) NOT NULL AUTO_INCREMENT,
  `NewsTitle` varchar(255) DEFAULT NULL,
  `NewsIntro` mediumtext,
  `NewsFull` mediumtext,
  `NewsCover` varchar(200) DEFAULT NULL,
  `NewsCreated` datetime DEFAULT NULL,
  `NewsCreated_by` int(11) DEFAULT NULL,
  `NewsPublishUp` datetime DEFAULT NULL,
  `NewsPublishDown` datetime DEFAULT '0000-00-00 00:00:00',
  `NewsStatus` enum('A','S') DEFAULT 'A',
  `NewsHits` int(3) DEFAULT '0',
  `idGallery` int(11) DEFAULT NULL,
  PRIMARY KEY (`NewsId`),
  KEY `created_idx` (`NewsCreated_by`),
  KEY `idGallery` (`idGallery`),
  CONSTRAINT `created` FOREIGN KEY (`NewsCreated_by`) REFERENCES `USER` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `NEWS_ibfk_1` FOREIGN KEY (`idGallery`) REFERENCES `NEWSGALLERY` (`idGallery`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `NEWS` */

insert  into `NEWS`(`NewsId`,`NewsTitle`,`NewsIntro`,`NewsFull`,`NewsCover`,`NewsCreated`,`NewsCreated_by`,`NewsPublishUp`,`NewsPublishDown`,`NewsStatus`,`NewsHits`,`idGallery`) values 
(1,'¿Disfrutó el Halloween?','¿Sabía que reirse es una de las mejores medicinas que existe y además es gratis? ','<p >¿Sabía que reirse es una de las mejores medicinas que existe y además es gratis? En medio tanto estres y trabajo es necesario tomar momentos para sonreir y burlarnos de nosotros mismos. Así que, aproveche cuando estos momentos se presentan y cuando usted tiene la posibilidad de compartir con sus compañeros de trabajo algo diferente a un almuerzo a la carrera o una reunión.</p>\r\n\r\n<p >Los niños se rien más o menos 300 veces en el día y los adultos entre 15 y 100 veces, es una estadística que muestra que entre más crecemos menos sonreimos. Es necesario y urgente que aprendamos a disfrutar de las cosas que nos permiten tener espacio para el esparcimiento y disfrutar.</p>\r\n\r\n<p >Así que muy bien por los que se disfrazaron y se uniero a la celebración y ahora si un aplauso para los ganadores:</p>\r\n\r\n<p >CONCURSO ÁREA&nbsp;MEJOR DECORADA<br>\r\n  <br>\r\n  1. PUESTO : Producción&nbsp;&nbsp;<br>\r\n  2. PUESTO : Contabilidad&nbsp;&nbsp;<br>\r\n  <br>\r\n  DISFRAZ HALLOWEN<br>\r\n  <br>\r\n  1. PUESTO:&nbsp; Daniel Hernandez Don Maximo&nbsp;<br>\r\n  2. PUESTO : Juan Sebastian Carvajal y Juan Sebastian Peña&nbsp;</p>\r\n\r\n<p >¡Gracias gente INMOV!&nbsp;&nbsp;</p>\r\n','hallowen.jpg','2017-10-31 16:48:13',377,'2017-10-31 16:48:28','0000-00-00 00:00:00','A',27,NULL),
(2,'¡Llegamos a la Televisión Colombiana!','Este lunes se estreno por Señal Colombia el programa ¿Qué inventa? En el que se saca a relucir todo el talento que tenemos los colombianos al momento de crear y hacer modificaciones a cosas ya existentes de acuerdo a nuestras necesidades.','<p >Este lunes se estreno por Señal Colombia el programa ¿Qué inventa? En el que se saca a relucir todo el talento</p>\r\n<p >que tenemos los colombianos al momento de crear y hacer modificaciones a cosas ya existentes de acuerdo a nuestras necesidades.</p>\r\n\r\n<p >Si este lunes estaba ocupado y no pudo prender su televisor, esta semana que viene no se quede sin verlo, porque es todo un orgullo!</p>\r\n\r\n<p >Enterese de datos curiosos y disfrute de 8 capitulos que muy seguramente van a dejarlo sorprendido.</p>\r\n','senalColombia.jpg','2017-10-31 16:59:17',377,'2017-10-31 16:59:17','0000-00-00 00:00:00','A',9,NULL),
(3,'Con la MBL, Inmov se tomo Cartagena','Para hacer ejecuciones impecables se necesita un equipo con todas las de la ley así como el de INMOV Global Network.','\r\n<p >Para hacer ejecuciones impecables se necesita un equipo con todas las de la ley así como el de INMOV Global Network.</p>\r\n<p >Todo un éxito fue el Festival que se realizó en Cartagena para celebrar el Juego 4 de la Serie Mundial 2017 que se replicó en pantallas gigantes en la Plaza de la Aduana.</p>\r\n\r\n<p >Fueron recibidas 3260 personas en un publico rotativo que vivió esta gran experiencia y al final participaron en diferentes actividades. Incluidas exposiciones de uniformes, jaulas de batear, jaulas de lanzar y mucho más.</p>\r\n\r\n<p >Felicidades al equipo INMOV!</p>\r\n','mlb.jpg','2017-11-01 17:01:00',377,'2017-11-01 17:01:00','0000-00-00 00:00:00','A',31,NULL),
(4,'Nuestras nuevas redes sociales','Ingresa ya y síguenos','Ingresa ya y síguenos\r\n<iframe width=\"100%\" src=\"https://www.youtube-nocookie.com/embed/3QGV1d9HP38?rel=0&amp;showinfo=0\" frameborder=\"0\" allowfullscreen></iframe>',NULL,'2017-07-21 11:20:26',377,'2017-07-21 11:20:26','0000-00-00 00:00:00','A',33,NULL),
(5,'Charlie, el poderoso de las ideas','“El poder de las ideas”, eso es lo que Carlos Sigua Director General de Creatividad ama profundamente de su profesión, más que marketing, creatividad, publicidad, trasnocho o estrés, Charlie, (como le decimos de cariño) es un enamorado de las IDEAS.','<p>&ldquo;El poder de las ideas&rdquo;, eso es lo que Carlos Sigua Director General de Creatividad ama profundamente de su profesión, más que marketing, creatividad, publicidad, trasnocho o estrés, Charlie, (como le decimos de cariño) es un enamorado de las IDEAS.</p>\r\n<p>De eso se dio cuenta en tercer semestre cuando de &ldquo;pelaó se ganó un concurso  frente a otras Universidades del país, con una idea contundente, por eso para Charlie, la imaginación se va fortaleciendo con le pasar del tiempo, porque como el mismo dice: &ldquo;uno va más a cine, lee más libros, viaja más y te rodeas de grandes amigos que aportan para ello\"</p>\r\n<p>Sin embargo, no se aparta de que uno de los retos más grandes de su profesión es lograr balancear el tiempo para el trabajo, la familia y el mismo, aunque siendo muy honesto, asegura que vale la pena, ganarse un proyecto a través de una idea.</p>\r\n<p>Al momento de elegir entre un libro, una película y teatro, lo de Charlie, es la lectura, porque claro, su poderosa imaginación logra crear su propio mundo.</p>\r\n<p>Para los que no saben Charlie vivió en Brasil durante algún tiempo, a esa experiencia le agradece haber podido estudiar y trabajar en ese país considerado potencia mundial en todo este rollo de la publicidad. Pero como todo buen hijo, siempre vuelve a casa, a su regreso las puertas de Inmov estaban abiertas para el.</p>\r\n<p>Charlie pertenece a una familia unida, pero muy unidad, aclara. Su mamá es súper amorosa y es el pilar de su familia, en esta navidad, harán lo de costumbre: tener charlas alrededor de la chimenea y al son de un buen vino.</p>\r\n<p>Al preguntarle que le gusta comer en navidad su respuesta fue contundente: &ldquo;El tamal, los buñuelos, el sancochito, los huevitos con chocolate, la carne, el pollo, el pavo, el ajiaco.... jajjaja creo que todos&rdquo; mejor dicho, para Charlie todo se vale en navidad.</p>\r\n<p>Pero al momento de ponernos mas serios, me contó que uno de sus planes para el próximo año es inscribir una idea en un premio y ganárselo y retomar el fútbol. De matrimonio, dijo que no podíamos hablar, así que los chismosos que andan diciendo que se casa… se quedarán con las ganas de saber.</p>\r\n<p>En todo caso, espero ser invitada al matrimonio, si es que se casa…</p>\r\n<p>¡Feliz Navidad Charlie y gracias por estar ENMOVIMIENTO!</p>\r\n','charlie.jpg','2017-12-18 11:18:18',377,'2017-12-18 11:18:23','0000-00-00 00:00:00','A',39,NULL),
(6,'¡Coca-Cola regresó de la mano de los mejores, Inmov!','Cuando hablamos de navidad en Colombia, la caravana Coca-Cola es una de esas celebraciones que se espera con ansias y este año, Coca-Cola regresó de la mano de los mejores: Nosotros, Inmov Global Network.','<p>Cuando hablamos de navidad en Colombia, la caravana Coca-Cola es una de esas celebraciones que se espera con ansias y este año, Coca-Cola regresó de la mano de los mejores: Nosotros, Inmov Global Network.</p>\r\n\r\n<p>Un gran equipo fue el que puso el alma en este caravana 2017 que convocó muchas más personas de las que se esperaban. Se paralizaron las calles de Bogotá ante semejante espectáculo que trajo de regreso el espíritu navideño</p>\r\n\r\n<p>Felicidades a todos los que participaron, desde el equipo comercial, creativo, producción hasta administración, se fajaron!</p>','caravana-cocacola.jpg','2017-12-18 10:31:47',377,'2017-12-18 10:31:56','0000-00-00 00:00:00','A',18,NULL),
(7,'Usted es una gran herramienta de mercadeo','Pensar que las redes sociales solo sirven para conocer lo que le pasa a nuestros amigos es un tema del pasado, hoy, les muestro un pequeño listado del impacto que tiene una red como Facebook en este momento, a nivel empresarial, aunque existen otro tipo de redes que las nuevas generaciones esta usando','\r\n<p>Pensar que las redes sociales solo sirven para conocer lo que le pasa a nuestros amigos es un tema del pasado, hoy, les muestro un pequeño listado del impacto que tiene una red como Facebook en este momento, a nivel empresarial, aunque existen otro tipo de redes que las nuevas generaciones esta usando, Facebook  sigue siendo una gran herramienta para dar a conocer nuestros productos, porque lo que publica su empresa o la empresa donde usted trabaja usted lo ve, le da like y lo comparte, mejor dicho usted puede aumentar el número de personas que se enteran de las cosas que son interesantes e importantes para usted, en este caso el negocio del que forma parte.</p>\r\n\r\n<p>Lea para que se sorprenda:</p>\r\n<ul>\r\n  <li>\r\n  Casi un cuarto de la población mundial <a href=\"http://www.business2community.com/facebook/20-facebook-statistics-2017-01874493#IKJ6XVLd79L1IL6K.97\" class=\"external link\"  >(un 22,9%) usa Facebook.</a\r\n  </li>\r\n  <li>\r\n   Facebook tiene<a href=\"https://zephoria.com/top-15-valuable-facebook-statistics/\"  class=\"external link\"> 2,01 mil millones de usuarios activos al mes</a>, a nivel mundial\r\n  </li>\r\n  <li>\r\n   Los botones de &ldquo;me gusta&rdquo; y &ldquo;compartir&rdquo; se ven en <a href=\"https://zephoria.com/top-15-valuable-facebook-statistics/\"  class=\"external link\">10 millones de sitios cada día</a>\r\n  </li>\r\n  <li>\r\n    <a href=\"https://zephoria.com/top-15-valuable-facebook-statistics/\"  class=\"external link\">Un 29,7% de los usuarios de Facebook tiene entre 25 y 34 años</a> y representan el grupo demográfico más grande de la plataforma.\r\n  </li>\r\n  <li>\r\n    <a href=\"https://zephoria.com/top-15-valuable-facebook-statistics/\"  class=\"external link\">Cada segundo se crean cinco perfiles nuevos.</a>\r\n  </li>\r\n  <li>\r\n    El pico más grande de tráfico de usuarios en la plataforma<a href=\"https://zephoria.com/top-15-valuable-facebook-statistics/\"  class=\"external link\"> se da de 1 a 3 pm</a>.\r\n  </li>\r\n  <li>\r\n    Los jueves y los viernes, el nivel de interacción (engagement) <a href=\"https://zephoria.com/top-15-valuable-facebook-statistics/\"  class=\"external link\">es un 18% más alto que el resto de la semana</a>.\r\n  </li>\r\n  <li>\r\n    Existen cerca de <a href=\"https://zephoria.com/top-15-valuable-facebook-statistics/\"  class=\"external link\">83 millones de perfiles falsos</a>.\r\n  </li>\r\n  <li>\r\n    Se suben un total de <a href=\"https://zephoria.com/top-15-valuable-facebook-statistics/\"  class=\"external link\">300 millones de fotografías al día en Facebook</a>.\r\n  </li>\r\n  <li>\r\n    Los usuarios pasan alrededor de<a href=\"https://zephoria.com/top-15-valuable-facebook-statistics/\"  class=\"external link\"> 20 minutos en Facebook</a> cada vez que se conectan a la plataforma.\r\n  </li>\r\n  <li>\r\n    El usuario promedio tiene <a href=\"https://www.omnicoreagency.com/facebook-statistics/\"  class=\"external link\">155 amigos en Facebook.</a>\r\n  </li>\r\n  <li>\r\n    Más de <a href=\"https://www.omnicoreagency.com/facebook-statistics/\"  class=\"external link\">40 millones de pequeños negocios</a> tienen cuenta en Facebook.\r\n  </li>\r\n  <li>\r\n   Cada 20 minutos se comparten<a href=\"https://www.omnicoreagency.com/facebook-statistics/\"  class=\"external link\"> 1 millón de links, se mandan 20 millones de solicitudes de amistad y se envían 3 millones de mensajes</a>.\r\n  </li>\r\n  <li>\r\n  Facebook está <a href=\"https://www.omnicoreagency.com/facebook-statistics/\"  class=\"external link\">disponible en 101 idiomas.</a\r\n  </li>\r\n  <li>\r\n    <a href=\"http://www.wordstream.com/blog/ws/2017/01/05/social-media-marketing-statistics\"  class=\"external link\">2 millones de negocios</a> contratan publicidad en Facebook.\r\n  </li>\r\n</ul>\r\n<p>Así que aproveche que hoy es Jueves y síganos en nuestro Facebook. Gracias por compartir nuestras publicaciones.</p>\r\n<p>Fuente: <a href=\"https://www.brandwatch.com/es/2017/09/98-estadisticas-de-las-redes-sociales-para-2017\" class=\"external link\">Brandwatch.com</a></p>\r\n\r\n','nota-Ustedesunagranherramientademercadeo.jpg?v2','2018-02-21 16:10:48',377,'2018-02-21 19:11:04','0000-00-00 00:00:00','A',13,NULL),
(8,'La dura de las licitaciones','Se tiene que tener mucha templanza y constancia para ser capaz de licitar en las convocatorias del gobierno, por eso, ante Olga Lucía Useche (mi tocaya), me quito el sombrero, es común verla por la agencia buscando hojas de vida, pidiendo papeles y rebuscando  la forma de reunir una cantidad de documentos que parecen imposibles, pero ella, siempre lo logra, es que a Olga Lucía el tema de las licitaciones se le da.','<p>Se tiene que tener mucha templanza y constancia para ser capaz de licitar en las convocatorias del gobierno, por eso, ante Olga Lucía Useche (mi tocaya), me quito el sombrero, es común verla por la agencia buscando hojas de vida, pidiendo papeles y rebuscando  la forma de reunir una cantidad de documentos que parecen imposibles, pero ella, siempre lo logra, es que a Olga Lucía el tema de las licitaciones se le da.\r\n</p>\r\n<p>Sentarse horas eternas a leer papeles para prepararse y representar la agencia en medio de una cantidad de gente que más bien parecen tiburones, es una de sus grandes fortalezas, será por eso, que al momento de preguntarle que libro planea leerse en este 2018, con toda la tranquilidad del mundo me dijo, que no le gustaba leer.</p>\r\n<p>Lo que si sabe Olga Lucía es que este año quiere, ahorrar, tomar un curso de cocina y conocer Tahilandia. Estaremos pendientes de su Facebook para verla desde Bangkok.</p>\r\n<p>Sus sueños más grandes son: Tener un hijo, mucha salud  y poder llegar a la vejez con su esposo. Cuando le pregunté si había bailado mucho en diciembre me dijo que no pero que en todas las reuniones que estuvo canto una canción de despecho de Dario Dario y se llama: muñeco de vitrina.</p>\r\n<p>Olga Lucía tuvo la oportunidad de viajar a Chicago y ejecutar la Convención de uno de nuestros clientes. Quedó muy sorprendida de la forma en que cobran  cualquier trabajo.  &ldquo;Tienen una claridad de Costo – Oportunidad increíble, a todo le quieren ganar el 200% y no enciman nada y no dan adicionales, al contrario te cobran por solo decirte hello&rdquo;, dijo Olga Lucía.</p>\r\n<p>Por último le pedi que me contará una anécdota de su vida y aquí va, con sus palabras: &ldquo;Con mi primera suegra, una señora muy estudiada e intelectual, nos pusimos hablar del país y en ese momento habían matado a Andres Escobar y a Pablo Escobar y yo me puse a decir  que terrible esas cosas que pasaban que para mí era muy triste la muerte de Pablo Escobar, porque había sido una persona muy buena, disciplinado, con una familia tan bonita y era injusto que lo hubieran matado y en cambio me alegraba que hubieran matado a Andrés Escobar que era un mal para la sociedad y todo lo que había hecho era malo. La señora se me quedo mirándome y no siguió la conversación, inmediatamente la cambió y solo cuando salí caí en cuenta que la había embarrado y había confundido los personajes ¡plopppp!  La señora diría: Y esa es la novia de mi hijo?</p>\r\n<a href=\"https://www.facebook.com/olga.l.useche\" class=\"link button external\">Facebook de Olga</a>\r\n','olga-useche.jpg','2018-02-22 19:29:21',377,'2018-02-23 14:28:57','0000-00-00 00:00:00','A',30,NULL),
(9,'Test','Texto Intro','<iframe width=\"100%\"  src=\"https://www.youtube.com/embed/OjDKBnDVQCk\" frameborder=\"0\" gesture=\"media\" allow=\"encrypted-media\" allowfullscreen></iframe>',NULL,'2017-08-08 10:53:16',377,'0000-00-00 00:00:00','0000-00-00 00:00:00','A',7,NULL),
(10,'Los patrocinios para las marcas y su legado','Con una gran experiencia en el mundo deportivo Pedro Aguirre es el gerente de nuestra Unidad de Negocio, Hero que busca lograr que las marcas dejen un legado a través de los patrocinios.','<p>Con una gran experiencia en el mundo deportivo Pedro Aguirre es el gerente de nuestra Unidad de Negocio, Hero que busca lograr que las marcas dejen un legado a través de los patrocinios. Podría casi que escribir un libro de todo el camino recorrido que tiene Pedro empezando por su carrera como atleta y su visita a Atenas como voluntario a los Juegos Olimpicos  2004, pero, la intención de este escrito es mostrar como la experiencia de Pedro nos enriquece como agencia.</p>\r\n\r\n<p>Su recorrio ha sido bastante amplio, formó parte de los juegos Bolivarianos en Armenia y Pereira y los juegos centro americanos en Cartagena entre otros muchos eventos deportivos. Tuvo la oportunidad de trabajar para una empresa de Consultoria y fue alli donde adquirió experiencia en este campo. Pasó a trabajar con el comite Olimpico Colombiano, trabajó también en la Cancillería y allí formo el programa de diplomacia deportiva que hoy da sus frutos,  formó parte también de la realización de un documental transmitido por RCN Television donde se narraba las historias de 50 atletas colombianos en su preparación para los juegos Olímpicos y paraolímpicos.</p>\r\n\r\n<p>Hoy, dentro de otras cosas que hace Pedro, esta lograr  que la unidad de negocios Heroe, le enseñe a los patrocinadores de los eventos y programas deportivos que deben ir más allá de tomarse fotos o lograr asociación con el deporte, la idea es que exista más que un compromiso comercial y las marcas sean recordadas de determinada forma y casi que insporacionales.</p>\r\n','hero.jpg','2018-03-23 10:45:05',377,'0000-00-00 00:00:00','0000-00-00 00:00:00','A',8,NULL),
(11,'Nueva certificación nuevos retos','La Certificación de Sostenibilidad Turística, me sonaba como a vacaciones y la primera pregunta que se me vino a la cabeza fue: ¿Por qué, si nosotros no somos un hotel o un centro recreacional?','<p>La Certificación de Sostenibilidad Turística, me sonaba como ha vacaciones y la primera pregunta que se me vino a la cabeza fue: ¿Por qué, si nosotros no somos un hotel o un centro recreacional? Pero resulta que esta nueva certificación involucra a todas las empresas que de una u otra forma estan relacionadas con el aspecto turístico y nosotros somos una agencia que esta constantemente ejecutando producciones a largo y ancho de nuestro país.</p>\r\n<p>Sergio López y Dallán Jauregui son las personas detrás de todo este gran logro que va más allá de lo que nos imaginamos. </p>\r\n<p>La idea de esta certificación es que las empresas que la obtenemos nos comprometemos a aportar en aspectos ambientales, socioculturales y económicos y cuando se le da cumplimiento a esos tres aspectos podemos decir que la empresa es una empresa sostenible. </p>\r\n<p>Usar esta norma es una exigencia y ademas es un requisito en muchas licitaciones así que por el lado comercial, se abren mas las puertas para participar en procesos licitatorios por lo menos con el estado.</p>\r\n<p>Dentro de los principales beneficios de recibir la certificación están: </p>\r\n<ol>\r\n  <li>\r\n    <p>Aportar al medio ambiente y en eso estamos ahorrando dinero. En estos dos últimos años en la agencia se logró un ahorro notable de papel que equivale a millón y medio de pesos.</p>\r\n  </li>\r\n  <li>\r\n    <p>Por el lado cultural es muy relevante porque se promueve determinada zona del país a través de artesanias, contratando mano de obra local y amplificando su aspecto turistico.</p>\r\n  </li>\r\n  <li>\r\n    <p>Tambien hay un componente social y es el tema de voluntariado, porque podemos ayudar a quienes lo necesitan empezando desde nuestra empresa.</p>\r\n  </li>\r\n</ol>\r\n<p>Realmente recibir esta certificación es el comienzo. De ahora en adelante este tema se va a alinear con el sistema de gestión de calidad. Son muchos capacitaciones pero ya pronto nuestra área desarrollo nos entregará un aplicativo  y podremos montar diferentes contenidos de manera que todos puedantener acceso a desde nuestros dispositivos moviles.</p>\r\n','certificacion.jpg','2018-04-03 12:47:36',377,'0000-00-00 00:00:00','0000-00-00 00:00:00','A',17,NULL),
(12,'Inmov en México, Panamá y Argentina','La semana pasado tuvimos el gusto de tener los gerentes de Inmov Latam en nuestra casa matriz en Bogotá. Y en medio de reuniones y entrenamientos tuvieron algo de tiempo para dejarse atender al mejor estilo colombiano. ','<p>La semana pasado tuvimos el gusto de tener los gerentes de Inmov Latam en nuestra casa matriz en Bogotá. Y en medio de reuniones y entrenamientos tuvieron algo de tiempo para dejarse atender al mejor estilo colombiano. \r\n</p>\r\n<p>En la foto ven a <strong>Claudia Bernal</strong> de México, <strong>Pedro Massino</strong> Argentina y <strong>Laura Penagos</strong> de Panamá. No me quedé con las ganas de preguntarles un poquito de su vida y aquí les van los datos.</p>\r\n<p>Pedro Luis Mazzino tiene 35 años es abogado especializado en Derecho Empresarial. Su comida favorita es el  asado de costilla al asador, les gusta el golf, el crossfit, y viajar; el último libro que leyó es &ldquo;La Caída de Albert Camus. Entre el cine, el teatro o una buena cena prefiera la última, lo mismo que <strong>Laura Penagos</strong> nuestra gerente de Panamá, pero a diferencia del asado, Laura se queda con la pizza y disfruta su tiempo libre en familia, el último libro que leyó fue &ldquo;El evangelio del mal&rdquo;.</p>\r\n<p>Su corazón regreso a Panamá lleno de amor por Colombia y por Inmov, porque según sus palabras las atenciones del equipo de Inmov fueron insuperables. Más adelante les cuento un poco más de Claudia, que según me enteré su acento mexicano fue todo un éxito en Inmov.</p>\r\n','mx-pan-arg.png','2018-05-17 08:10:50',377,'0000-00-00 00:00:00','0000-00-00 00:00:00','A',11,NULL),
(13,'Vientos de cambio','Dice el reconocido proverbio chino: “Cuando hay vientos de cambio unos levantan muros y otros molinos” en Inmov Global Network pertenecemos al segundo equipo, somos una empresa multinacional que ha venido mutando y en los últimos cinco años hemos construido molinos que nos han fortalecido como una agencia que va más allá de vender un software o una metodología de operación, nos hemos convertido en una integración de talentos unidos por productos o por proyectos. ','<p>Dice el reconocido proverbio chino: &ldquo;Cuando hay vientos de cambio unos levantan muros y otros molinos&rdquo; en Inmov Global Network pertenecemos al segundo equipo, somos una empresa multinacional que ha venido mutando y en los últimos cinco años hemos construido molinos que nos han fortalecido como una agencia que va más allá de vender un software o una metodología de operación, nos hemos convertido en una integración de talentos unidos por productos o por proyectos. \r\n</p>\r\n<p>Hoy, competimos a otro nivel, vamos por proyectos macro y hemos creado un universo que hemos venido conquistando poco a poco. Todo esto ha traído algunos cambios de estructura, hemos pasado a ser una estructura más móvil enfocada en sacar adelante un proyecto trascendente con un nivel de calidad y profesionalismo absoluto.</p>\r\n<p>En este proceso, necesitamos generar competencias de colaboración y comenzar a rodar con una tecnología de colaboración. Esto requiere que tengamos un nivel de desarrollo de talento al máximo. Dallán Jauregui tiene el reto de construir, en talento temporal, un proyecto que termine generando un nivel de independencia empresarial y unos logros de reclutar cada vez talento más preparado. Además seguirá frente a calidad, seguridad y salud en el trabajo que siguen siendo procesos transversales a toda la organización.</p>\r\n<p>Esto abre la oportunidad para crear un equipo adicional solo para talento humano que es liderado por Juan Camilo Orduz, con el apoyo de Angela Torres que es consultora y viene a trabajar en el proceso de gestación del servicio, es importante entender que si tenemos talentos recurrentes que están aquí todo el año tenemos que hablar directamente con Angela y con Juan Camilo, lo que vamos a buscar ahí es un esquema de profundidad y generar un esquema de mayor tracción.</p>\r\n<p>El objetivo es que se sienta mejor impacto en Talento Temporal, se están dedicando más recursos para construir alrededor de eso un mejor proyecto que tiene que crecer y que tenemos toda la expectativa y los ojos allí, que talento sea más transversal, que haya más acompañamiento, más compañía, porque venimos creciendo en el área pero tenemos el reto de seguir haciéndolo y eso depende de que ustedes también nos retro alimenten.</p>\r\n','vientos-de-cambio.png','2018-05-17 08:40:49',377,'0000-00-00 00:00:00','0000-00-00 00:00:00','A',15,NULL),
(14,'¡Bienvenido! Sergio David Cruz','Asistente de Talento Humano ','<p>27 años<br>\r\nTécnologo en gestión de Talendo Humano<br>\r\nLe gusta ek fútbol y esta aquí, INMOV S.A.S.\r\n</p>','Sergio-Cruz.jpg','2018-05-30 18:38:03',NULL,'2018-05-30 18:37:59','0000-00-00 00:00:00','A',24,NULL),
(15,'Novedades de Gmail','El nuevo Gmail es más atractivo, productivo y privado que nunca','\r\n  <p>Las funciones de Gmail que usas habitualmente cuentan ahora con un diseño renovado. Además, se han añadido nuevas formas de organizar la bandeja de entrada, ver los próximos eventos y gestionar los correos. Estas son algunas de las novedades:</p>\r\n  <h2>Gestionar mensajes rápidamente</h2>\r\n  <h3>Organizar correos desde la bandeja de entrada</h3>\r\n  <p>Cuando sitúes el puntero sobre un mensaje de tu bandeja de entrada, podrás gestionarlo rápidamente sin abrirlo:</p>\r\n  <p><img src=\"https://lh3.googleusercontent.com/xuuSm_fOnqTQs0yj_468kHSqsHkeUPDPOsqnGgCxO0Yh4LduHCnOL3PRxWwg668BOt8=w399\" width=\"100%\"  alt=\"Acciones de la bandeja de entrada\" title=\"Acciones de la bandeja de entrada\"></p>\r\n  <ul>\r\n    <li>Archivar <img src=\"https://lh3.googleusercontent.com/9qjMizRsseQajmoL0wmG4krmR4HLqei7DZtzTHVZN8CFiw4ZfYtJJbkf-M9w5sG7eR8\" alt=\"Archivar\" title=\"Archivar\"></li>\r\n    <li>Eliminar <img src=\"https://storage.googleapis.com/support-kms-prod/AD2EDAAA63DB618DCAD54BB58513E2351A79\" width=\"18\" height=\"18\" alt=\"Eliminar\" title=\"Eliminar\"></li>\r\n    <li>Marcar como no leído <img src=\"https://lh3.googleusercontent.com/_Lx1d93AxKCjE0k1W-n2W9SS-VkcUYqoIOe8GhUEn1bfhtm4EZOWeedYG8MJaDG_MGA=w18-h18\" width=\"18\" height=\"18\" alt=\"No leído\" title=\"No leído\"> o leído <img src=\"https://lh3.googleusercontent.com/tpaX9jWPLhPOiTCcZ1N4EvNo1omGqbvWfjO3UwOnX8IzpC0L1DF5Zd9RDKR8t_eQc7g=w18-h18\" width=\"18\" height=\"18\" alt=\"Leído\" title=\"Leído\"></li>\r\n    <li>Posponer <img src=\"https://lh3.googleusercontent.com/1O4-oCGpzMQwcZ9CoBJ18JlMhVdXEjMMP1xYoRgaPkgdEdewW722pfbq17KguWmwDeI=w18-h18\" width=\"18\" height=\"18\" alt=\"Posponer\" title=\"Posponer\"></li>\r\n  </ul>\r\n  <h3>Posponer correos hasta más tarde</h3>\r\n  <p>Puedes posponer correos hasta una fecha u hora futuras que te vengan mejor. Consulta cómo <a href=\"https://support.google.com/mail/answer/7622010\" class=\"external\">posponer correos</a>.</p>\r\n  <img src=\"https://lh3.googleusercontent.com/8Fj84M-lIh_2rL6nm5r9hZzXR07SapW8r-J2ynozQwYQ0j31hCe60IB9ftThBAoxSAo=w600\" width=\"100%\"  alt=\"Posponer\" title=\"Posponer\">\r\n  <h3>Utilizar las sugerencias de respuesta y seguimiento</h3>\r\n  <p>Puedes responder rápidamente a los correos mediante las frases que te aparecen en función del mensaje que has recibido. Consulta cómo <a href=\"https://support.google.com/mail/answer/6585#smartreply?hl=es\" class=\"external\">usar </a><a href=\"https://support.google.com/mail/answer/6585#smartreply?hl=es\" class=\"external\">Respuesta Inteligente</a>.</p>\r\n  <img src=\"https://lh3.googleusercontent.com/jl9b20A2zehyO_YFyWfrTEYrqU0cXz3pVaTggcw-Jp0H8xNs1j27mzYB-iYS1l2YVYdI=w401\" width=\"100%\"  alt=\"\">\r\n  <p> </p>\r\n  <p>Es posible que veas correos antiguos al principio de tu bandeja de entrada con una sugerencia para que respondas a ellos o para que realices algún tipo de acción.</p>\r\n  <img src=\"https://lh3.googleusercontent.com/EgwKsmevk1wVh3c9ht6C2hJhXgfTWlFb3y8FxcoOoH78Ys7ESJeKqj275YXeTRhYHOW0=w600\" width=\"100%\" alt=\"\">\r\n  <h2>Personalizar la vista de la bandeja de entrada</h2>\r\n  <h3>Ampliar o reducir la bandeja de entrada </h3>\r\n  <p>Puedes cambiar la vista de la bandeja de entrada en función de si quieres que se vea más amplia o más compacta. Arriba a la derecha, haz clic en Configuración <img src=\"https://storage.googleapis.com/support-kms-prod/A36BE11E088559CDC333448B2AC7F76B6C48\" width=\"18\" height=\"18\" alt=\"Ajustes\" title=\"Ajustes\"><img src=\"https://lh3.googleusercontent.com/sDpfETHk7K0ryVo50RvXGzPtfrDQ2W0xK4sJdOKYqerlc79U0MaNKQggC7nI6gJY0A=w13-h18\" width=\"13\" height=\"18\" alt=\"a continuación\" title=\"a continuación\"> <strong>Densidad de visualización</strong>.</p>\r\n  <p><img src=\"https://lh3.googleusercontent.com/9wxEMUvvhYsrswtQeX81ovq_4x-DgQqR_H0zbk8IWnTUyl8LtjQHhBRnmyycRKAQQHo=w401\" width=\"100%\" alt=\"\"></p>\r\n  <p><strong>Consejo:</strong> Si eliges <strong>Predeterminada</strong>, se muestra una vista previa de los archivos adjuntos en la bandeja de entrada.</p>\r\n  <h3>Mostrar u ocultar el menú</h3>\r\n  <p>Si quieres tener más espacio para ver tu correo, puedes ampliar o reducir la vista de la bandeja de entrada haciendo clic en el menú <img src=\"https://storage.googleapis.com/support-kms-prod/CD148BFC3EE3B5328DAFE08E2B6AA95B73B7\" width=\"18\" height=\"18\" alt=\"Menú\" title=\"Menú\">.</p>\r\n  <p><img src=\"https://lh3.googleusercontent.com/ZpO15BviN3VUqh5Uq6F-OwwS6Sjs87hHp7fQtAPRcXPtiwa7yDs_kpNU1geApSbApQHc=w600\" width=\"100%\"  alt=\"\"></p>\r\n  <h3>Ver Calendar, Tareas, Keep y complementos</h3>\r\n  <p>Ahora puedes <a href=\"https://support.google.com/mail/answer/106237\">usar </a><a href=\"https://support.google.com/mail/answer/106237\" class=\"external\">Google Calendar, Keep, Tareas y complementos</a> desde Gmail.</p>\r\n  <p><img src=\"https://lh3.googleusercontent.com/eCKKA4kaoG-1Sd0bBq3qkEuxrNridJAY0sd8hf_TGDzUJMcIKhhBxBZyqYHhn-JB7wE=w600\" width=\"100%\"  alt=\"Calendar\" title=\"Calendar\"></p>\r\n  <p>A la derecha de la bandeja de entrada, haz clic en estos iconos:</p>\r\n  <ul>\r\n    <li><strong>Calendar</strong> <img src=\"https://lh3.googleusercontent.com/oZtnrznngZFu9lPVZNCMMDuJpsjLyMxEknlXVVkAsee2XLXsbw5K1QMmctBMdal2JYg=w18-h18\" width=\"18\" height=\"18\" alt=\"Calendar\" title=\"Calendar\">: consulta tu agenda diaria, crea eventos, edítalos y ve a los que tienes programados.</li>\r\n    <li><strong>Keep</strong> <img src=\"https://lh3.googleusercontent.com/Go0KiEWdG5auL4MiFerqvKZjujMXTSeL9XlogUG3tIWNEDA43YeJimqLHbsSqhibAsA=w18-h18\" width=\"18\" height=\"18\" alt=\"Google Keep\" title=\"Google Keep\">: crea listas de comprobación y toma notas.</li>\r\n    <li><strong>Tareas</strong> <img src=\"https://lh3.googleusercontent.com/WwhhkZceod74lA34JkdAdWTAOmIV8g57l03cxAIoI0O_ng9NAkrQSQ1_y53d3ZRFIQ=w18-h18\" width=\"18\" height=\"18\" alt=\"Logotipo de Tareas\" title=\"Logotipo de Tareas\">: añade tareas y fechas límite.</li>\r\n    <li><strong>Complementos</strong> <img src=\"https://lh3.googleusercontent.com/5XXH0IVZcPteZ1ZyYABmy9MmxlMv_8EvE2JrvaBZvNPlIwhMEHhPun4LhukvjjxViQ=w18-h18\" width=\"18\" height=\"18\" alt=\"Añadir\" title=\"Añadir\">: descarga otras herramientas de Gmail para gestionar mejor tu correo.</li>\r\n  </ul>\r\n  <p>Consulta cómo <a href=\"https://support.google.com/mail/answer/106237\" class=\"external\">usar Calendar, Keep, Tareas y complementos</a> con Gmail.</p>\r\n\r\n','nuevo-gmail-service.jpg','2018-08-02 15:42:27',377,'2018-08-02 15:42:34','0000-00-00 00:00:00','A',12,NULL);

/*Table structure for table `NEWSGALLERY` */

DROP TABLE IF EXISTS `NEWSGALLERY`;

CREATE TABLE `NEWSGALLERY` (
  `idGallery` int(11) NOT NULL AUTO_INCREMENT,
  `galleryName` varchar(80) DEFAULT NULL,
  `folder` varchar(30) DEFAULT NULL,
  `hits` int(4) DEFAULT '0',
  PRIMARY KEY (`idGallery`),
  KEY `idGallery` (`folder`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `NEWSGALLERY` */

/*Table structure for table `NEWSLIKES` */

DROP TABLE IF EXISTS `NEWSLIKES`;

CREATE TABLE `NEWSLIKES` (
  `idNewsStats` int(11) NOT NULL AUTO_INCREMENT,
  `idNews` int(11) DEFAULT NULL,
  `newsStatsLikes` int(4) DEFAULT '1',
  `idUser` varchar(45) NOT NULL,
  `timeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idNewsStats`),
  UNIQUE KEY `idpres` (`idNews`,`idUser`),
  KEY `idUser` (`idUser`),
  CONSTRAINT `NEWSLIKES_ibfk_1` FOREIGN KEY (`idNews`) REFERENCES `NEWS` (`NewsId`),
  CONSTRAINT `NEWSLIKES_ibfk_2` FOREIGN KEY (`idUser`) REFERENCES `USER` (`UserLogin`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=latin1;

/*Data for the table `NEWSLIKES` */

insert  into `NEWSLIKES`(`idNewsStats`,`idNews`,`newsStatsLikes`,`idUser`,`timeStamp`) values 
(94,3,1,'mmiranda','2017-11-16 17:55:46'),
(95,3,1,'fburgos','2017-11-23 10:42:31'),
(96,2,1,'mmiranda ','2017-11-24 13:10:39'),
(97,1,1,'mmiranda ','2017-11-24 17:04:29'),
(98,5,1,'mmiranda ','2017-12-18 11:57:13'),
(99,5,1,'csigua','2017-12-18 11:57:28'),
(101,6,1,'mmiranda ','2017-12-21 16:20:10'),
(102,6,1,'okappler','2017-12-24 21:52:12'),
(103,5,1,'fburgos','2017-12-26 12:47:58'),
(104,6,1,'fburgos','2017-12-26 12:48:38'),
(105,4,1,'mmiranda ','2018-01-03 09:19:08'),
(106,5,1,'ouseche','2018-02-08 10:22:08'),
(107,8,1,'darias','2018-02-23 15:13:07'),
(108,8,1,'jbotero','2018-02-23 16:02:37'),
(109,8,1,'jtorres','2018-03-13 13:45:02'),
(110,8,1,'slopez','2018-03-13 21:08:03'),
(111,7,1,'slopez','2018-03-13 21:09:49'),
(112,5,1,'slopez','2018-03-13 21:10:48'),
(113,11,1,'slopez','2018-04-04 07:33:54'),
(114,11,1,'djauregui','2018-04-10 18:57:37'),
(115,10,1,'djauregui','2018-04-10 18:58:01'),
(116,12,1,'cbernal','2018-05-17 20:38:09'),
(117,12,1,'mmiranda','2018-05-17 21:27:48'),
(118,13,1,'mmiranda','2018-05-17 21:27:55'),
(119,13,1,'slopez','2018-05-20 12:09:59'),
(120,12,1,'slopez','2018-05-20 12:11:54'),
(121,14,1,'jorduz','2018-05-31 20:09:53'),
(122,14,1,'jbotero','2018-06-01 18:34:51'),
(123,14,1,'slopez','2018-06-04 19:56:02'),
(124,14,1,'djauregui','2018-06-06 10:21:39'),
(125,13,1,'pmazzino','2018-06-19 06:47:43'),
(126,12,1,'pmazzino','2018-06-19 06:48:16'),
(127,10,1,'pmazzino','2018-06-19 06:49:21'),
(128,7,1,'pmazzino','2018-06-19 06:50:53'),
(129,14,1,'mmiranda','2018-07-03 18:00:46'),
(130,14,1,'fburgos','2018-07-30 15:13:56'),
(131,15,1,'slopez','2018-08-06 20:02:41');

/*Table structure for table `NOTIFICATION` */

DROP TABLE IF EXISTS `NOTIFICATION`;

CREATE TABLE `NOTIFICATION` (
  `NotifiId` int(11) NOT NULL,
  `NotifiName` char(255) DEFAULT NULL,
  `NotifiTxt` mediumblob,
  `NotifiStatus` enum('A','S') DEFAULT NULL,
  `NotifiCreated` datetime DEFAULT NULL,
  `NotifiCreate_by` int(11) DEFAULT NULL,
  `NotifiPublishDown` datetime DEFAULT NULL,
  PRIMARY KEY (`NotifiId`),
  KEY `usuario_idx` (`NotifiCreate_by`),
  CONSTRAINT `usuario` FOREIGN KEY (`NotifiCreate_by`) REFERENCES `USER` (`UserId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `NOTIFICATION` */

/*Table structure for table `NOTIFIPROFILE` */

DROP TABLE IF EXISTS `NOTIFIPROFILE`;

CREATE TABLE `NOTIFIPROFILE` (
  `NOTIFICATION_NotifiId` int(11) NOT NULL,
  `PROFILE_ProfileId` int(11) NOT NULL,
  PRIMARY KEY (`NOTIFICATION_NotifiId`,`PROFILE_ProfileId`),
  KEY `fk_NOTIFIPROFILE_PROFILE1_idx` (`PROFILE_ProfileId`),
  CONSTRAINT `fk_NOTIFIPROFILE_NOTIFICATION1` FOREIGN KEY (`NOTIFICATION_NotifiId`) REFERENCES `NOTIFICATION` (`NotifiId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_NOTIFIPROFILE_PROFILE1` FOREIGN KEY (`PROFILE_ProfileId`) REFERENCES `PROFILE` (`ProfileId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `NOTIFIPROFILE` */

/*Table structure for table `OPTIONS` */

DROP TABLE IF EXISTS `OPTIONS`;

CREATE TABLE `OPTIONS` (
  `OptionsId` int(11) NOT NULL AUTO_INCREMENT,
  `OptionsName` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `OptionsUrl` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `OptionsStatus` enum('A','S') COLLATE utf8mb4_bin DEFAULT 'A',
  `OptionsIcon` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL,
  `OptionsLevel` enum('1','2') COLLATE utf8mb4_bin DEFAULT NULL,
  `OptionsOrder` int(11) DEFAULT NULL,
  `MODULE_ModuleId` int(11) NOT NULL,
  PRIMARY KEY (`OptionsId`),
  KEY `fk_OPTIONS_MODULE1_idx` (`MODULE_ModuleId`),
  CONSTRAINT `fk_OPTIONS_MODULE1` FOREIGN KEY (`MODULE_ModuleId`) REFERENCES `MODULE` (`ModuleId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `OPTIONS` */

insert  into `OPTIONS`(`OptionsId`,`OptionsName`,`OptionsUrl`,`OptionsStatus`,`OptionsIcon`,`OptionsLevel`,`OptionsOrder`,`MODULE_ModuleId`) values 
(1,'Módulos','ModuleCreate.php','A','fa-cube ','1',1,1),
(2,'Opciones','OptionsCreateNew.php','A','fa-cogs','1',2,1),
(3,'Usuario','UserCreate.php','A','fa-user-circle','1',3,1),
(4,'Asignación Opciones','ProfileOption.php','A','fa-chain-broken','1',4,1),
(5,'Subir Archivos','NewGalleryUpImg.php','A','fa-file-image-o','1',1,2),
(6,'Crear Galería ','NewGalleryCreate.php','A','fa-file-image-o','1',2,2),
(7,'Editar imagen','NewsEditImages.php','A','fa-newspaper-o','1',3,2),
(8,'Listado de noticas','NewsLists.php','A','fa-newspaper-o','1',4,2),
(9,'Crear noticias ','NewsCreate.php','A','fa-newspaper-o','1',5,2),
(14,'Dashboard','dashboard-news.php','A',NULL,'2',1,1),
(15,'Dashboard','dashboard-social.php','A',NULL,'2',2,1);

/*Table structure for table `POSITION` */

DROP TABLE IF EXISTS `POSITION`;

CREATE TABLE `POSITION` (
  `PositionId` int(11) NOT NULL AUTO_INCREMENT,
  `PositionName` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`PositionId`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=latin1;

/*Data for the table `POSITION` */

insert  into `POSITION`(`PositionId`,`PositionName`) values 
(1,'Analista De Compras'),
(2,'Analista De Contabilidad'),
(3,'Analista De Informacion'),
(4,'Analista Junior  Talento Humano'),
(5,'Analista Junior Cuentas Por Cobrar'),
(6,'Analista Junior De Compras'),
(7,'Analista Junior De Nomina'),
(8,'Analista Unidad Hero'),
(9,'Asistente De Nómina'),
(10,'Auditora'),
(11,'Auxiliar De Sistemas'),
(12,'Auxiliar Servicios Generales'),
(13,'Back Office Atl                         '),
(14,'Back Office Comercial'),
(15,'Coordinador Contabilidad'),
(16,'Coordinador De Contabilidad Y Legalizaciones'),
(17,'Coordinador De Cuentas Por Pagar y Tesoreria'),
(18,'Coordinador De Facturación Y CxC'),
(19,'Coordinador De Nomina Y Talento Humano'),
(20,'Coordinador Logistico'),
(21,'Coordinadora TTHH Temporal'),
(22,'Creativo Copy Senior'),
(23,'Director de Arte'),
(24,'Director De Cuentas'),
(25,'Director De Innovacion         '),
(26,'Director General Creativo'),
(27,'Director Unidad De Desarrollo'),
(28,'Directora Comercial ATL'),
(29,'Diseñador Grafico Senior       '),
(30,'Diseñador Grafico Senior         '),
(31,'Diseñador Industrial Senior      '),
(32,'Diseñador Industrial Senior                '),
(33,'Diseñadora Comercial'),
(34,'Ejecutivo De Control y Optimizacion           '),
(35,'Ejecutivo De Cuenta Senior                   '),
(36,'Gerente'),
(37,'Gerente Administrativo Y Financiero'),
(38,'Gerente Comercial'),
(39,'Gerente De Control Y Optimizacion'),
(40,'Gerente De Cuenta'),
(41,'Gerente De Desarrollo E Ingenieria'),
(42,'Gerente De Mercadeo'),
(43,'Gerente De Operaciones'),
(44,'Gerente De Produccion Y Proyectos Especiales'),
(45,'Gerente De Proyecto'),
(46,'Gerente De Talentos Y Calidad'),
(47,'Gerente Proyecto Don Máximo     '),
(48,'Jefe De Bodega'),
(49,'Key Account Gobierno'),
(50,'Key Account Manager'),
(51,'Lider De Negocio'),
(52,'Mensajero                               '),
(53,'Operador SAP'),
(54,'Presidente'),
(55,'Productor Ejecutivo'),
(56,'Productor Senior'),
(58,'Productor Senior Gobierno'),
(59,'Programador Movil Phonegap'),
(60,'Recepcionista'),
(61,'General Director'),
(62,'Directora de Comunicaciones'),
(63,'Gerente Plataforma Tiendon'),
(64,'Vicepresidente Desarrollo Corporativo'),
(65,'Vicepresidente'),
(66,'Director de Talento Humano y Compensación'),
(67,'Coordinador Comercial'),
(68,'Desarrollador Senior'),
(69,'Coordinador de sistemas integrados'),
(70,'Director de estrategia'),
(71,'Coordinadora Essilor'),
(72,'Coordinadora de Compras'),
(73,'Consultora Talento Humano'),
(74,'Creativo Copy Junior'),
(75,'Country Manager México'),
(76,'Auxiliar de Nómina'),
(77,'Socio Gerente en Argentina'),
(78,'Agente comercial'),
(79,'Chief Connector'),
(80,'Director comercial'),
(81,'Gerente movil'),
(82,'Coordinador de IoT\n'),
(83,'Asistente TTHH'),
(84,'Directora Ejecutiva'),
(85,'Chief Executive Officer North America'),
(86,'Business Leader North America'),
(87,'Chief Operations Officer North America'),
(88,'General Manager North America'),
(89,'Marketing and Communications'),
(90,'Coordinador de proyectos');

/*Table structure for table `PROFILE` */

DROP TABLE IF EXISTS `PROFILE`;

CREATE TABLE `PROFILE` (
  `ProfileId` int(11) NOT NULL AUTO_INCREMENT,
  `ProfileName` varchar(45) DEFAULT NULL,
  `ProfileStatus` enum('A','S') DEFAULT NULL,
  `ProfileApp` enum('SI','NO') DEFAULT NULL,
  PRIMARY KEY (`ProfileId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `PROFILE` */

insert  into `PROFILE`(`ProfileId`,`ProfileName`,`ProfileStatus`,`ProfileApp`) values 
(1,'Administrator','A','NO'),
(2,'Cards','A','SI');

/*Table structure for table `PROFILEOPTIONS` */

DROP TABLE IF EXISTS `PROFILEOPTIONS`;

CREATE TABLE `PROFILEOPTIONS` (
  `PROFILE_ProfileId` int(11) NOT NULL,
  `OPTIONS_OptionsId` int(11) NOT NULL,
  KEY `fk_PROFILEOPTIONS_PROFILE1_idx` (`PROFILE_ProfileId`),
  KEY `fk_PROFILEOPTIONS_OPTIONS1_idx` (`OPTIONS_OptionsId`),
  CONSTRAINT `PROFILEOPTIONS_ibfk_1` FOREIGN KEY (`PROFILE_ProfileId`) REFERENCES `PROFILE` (`ProfileId`),
  CONSTRAINT `PROFILEOPTIONS_ibfk_2` FOREIGN KEY (`OPTIONS_OptionsId`) REFERENCES `OPTIONS` (`OptionsId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `PROFILEOPTIONS` */

insert  into `PROFILEOPTIONS`(`PROFILE_ProfileId`,`OPTIONS_OptionsId`) values 
(1,1),
(1,2),
(1,3),
(1,4),
(1,5),
(1,6),
(1,7),
(1,8),
(1,9);

/*Table structure for table `USER` */

DROP TABLE IF EXISTS `USER`;

CREATE TABLE `USER` (
  `UserId` int(11) NOT NULL AUTO_INCREMENT,
  `UserCedula` int(11) DEFAULT NULL,
  `UserName` varchar(100) DEFAULT NULL,
  `UserLastName` varchar(100) DEFAULT NULL,
  `UserLogin` varchar(45) NOT NULL,
  `UserPassword` varchar(35) DEFAULT NULL,
  `UserStatus` enum('A','S') DEFAULT 'A',
  `UserCountryPhoneCode` varchar(4) DEFAULT '+57',
  `UserMobile` varchar(45) DEFAULT NULL,
  `PROFILE_ProfileId` int(11) NOT NULL DEFAULT '2',
  `UserNotification` enum('SI','NO') DEFAULT 'NO',
  `UserAvatar` varchar(60) DEFAULT 'nouser.png',
  `UserUpdated` int(2) DEFAULT '0',
  `UserFirstLogin` datetime DEFAULT NULL,
  `UserLastLogin` datetime DEFAULT NULL,
  `UserUpdateTime` datetime DEFAULT NULL,
  `UserLastOpenTime` datetime DEFAULT NULL,
  `UserUUID` varchar(80) DEFAULT NULL,
  `UserModel` varchar(60) DEFAULT NULL,
  `UserPlatform` varchar(60) DEFAULT NULL,
  `UserVersion` varchar(60) DEFAULT NULL,
  `UserToken` varchar(134) DEFAULT NULL,
  `UserHits` int(3) DEFAULT '0',
  `UserBDay` date DEFAULT NULL,
  `UserLang` enum('es','en') DEFAULT 'es',
  PRIMARY KEY (`UserId`,`PROFILE_ProfileId`),
  UNIQUE KEY `UserLogin` (`UserLogin`),
  KEY `fk_USER_PROFILE1_idx` (`PROFILE_ProfileId`),
  CONSTRAINT `fk_USER_PROFILE1` FOREIGN KEY (`PROFILE_ProfileId`) REFERENCES `PROFILE` (`ProfileId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1110 DEFAULT CHARSET=latin1;

/*Data for the table `USER` */

insert  into `USER`(`UserId`,`UserCedula`,`UserName`,`UserLastName`,`UserLogin`,`UserPassword`,`UserStatus`,`UserCountryPhoneCode`,`UserMobile`,`PROFILE_ProfileId`,`UserNotification`,`UserAvatar`,`UserUpdated`,`UserFirstLogin`,`UserLastLogin`,`UserUpdateTime`,`UserLastOpenTime`,`UserUUID`,`UserModel`,`UserPlatform`,`UserVersion`,`UserToken`,`UserHits`,`UserBDay`,`UserLang`) values 
(47,8565441,'Flower','Burgos','fburgos','e10adc3949ba59abbe56e057f20f883e','A','+57','9728146304',1,'SI','1511884278869-355.jpg',1,'2017-11-23 10:33:11','2018-09-13 15:56:30','2018-06-28 15:17:57','2018-09-13 15:57:37','60950f9aeb369a42','XT1068','Android','6.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImZidXJnb3MiLCJleHAiOjE1Njc5NzYxOTB9.Qkls_S0T1-mTnSECNbHFhuPYBmZfSk9GfkObfB6jM_g',46,'1980-01-21','en'),
(83,80424606,'Juan ','Botero','jbotero','e10adc3949ba59abbe56e057f20f883e','A','+1','2145430598',2,'SI','1511823268283-696.jpg',1,'2017-11-27 17:52:00','2018-09-12 21:10:42','2018-09-10 10:23:14','2018-09-13 15:37:11','9462bbb19746f11d','SM-G930U','Android','8.0.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6Impib3Rlcm8iLCJleHAiOjE1Njc5MDg2NDJ9.-o1wncgvRjylDVfVpyKnZF1pmpXDU1eGMM3u3F2ceLk',34,'1972-08-01','en'),
(377,72282015,'Milton','Miranda Polo','mmiranda','e10adc3949ba59abbe56e057f20f883e','A','+57','3003315480',1,'SI','1531260372522-512.jpg',1,'2017-11-14 16:05:31','2018-07-10 16:56:00','2018-07-03 17:53:22','2018-09-14 10:09:07','dc987287025b2414','LG-K350','Android','6.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6Im1taXJhbmRhIiwiZXhwIjoxNTYyMzYzNzYwfQ.1AhUfedyjj3EOYhe7VIK5CwkXqxfBIzN_A_K-vzj5Mw',69,'1983-09-13','en'),
(555,1024515066,'Aric','Gutierrez','agutierrez','e10adc3949ba59abbe56e057f20f883e','S','+57','3114914314',2,'NO','1511971264667-355.jpg',1,'2017-11-27 15:08:59','2018-05-09 11:30:33','2017-11-27 15:10:41','2018-05-19 08:38:47','971c5ecdccd4e34d','G3123','Android','7.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImFndXRpZXJyZXoiLCJleHAiOjE1MzEwNjc0MzN9.YWl3dR6Flj2UZJtvRmLC72RwLwes4Udq4H0zkTm06Ms',37,'1991-02-04','es'),
(1000,52802569,'Adriana Marcela','Acuña Melo','aacuna','ac4cb63722e519e08b5e874657d35f22','A','+57','3013364661',2,'NO','nouser.png',1,'2018-06-22 16:39:33','2018-06-22 16:39:33','2018-06-22 16:40:35',NULL,'239be6b9de4aee31','LG-K350','Android','6.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImFhY3VuYSIsImV4cCI6MTU2MDgwNzU3M30.lvYo4uKRUkvs4vef4ecNfaO3DiPuPJzFUG2XY-7tSrk',27,'1981-04-30','es'),
(1001,1020716994,'Felipe Andres','Aguillon Calderon','faguillon','27a325a2804b83506d576da021b930ad','S','+57','3103746327',2,'NO','nouser.png',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'1986-07-10','es'),
(1002,52699656,'Andrea Marcela','Aponte Bohórquez','aaponte','dab4a0dd1ddf19845c9b79390a3c2484','S','+57','3143592569',2,'NO','nouser.png',1,'2018-06-26 10:32:01','2018-06-26 10:32:01','2018-06-26 10:32:16','2018-07-24 10:22:48','60F4CEB7-F018-42DE-9D61-A5509B82ACA6','iPhone7,2','iOS','10.0.2','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImFhcG9udGUiLCJleHAiOjE1NjExMzExMjF9.dEVWAaue5MvAxiolhhSyPRv8-zy-x1Yt4h1VA7mOfV8',9,'1980-08-31','es'),
(1003,43912564,'Diana Yaneth','Arias Noguera','darias','582501e3a9481e4f8f8b0dfb5eaf764b','S','+57','3202922325',2,'NO','nouser.png',1,'2017-12-29 09:40:45','2017-12-29 09:40:45','2017-12-29 09:40:58','2018-03-08 10:26:14','6727fceee2a300cf','Moto G (5)','Android','7.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImRhcmlhcyIsImV4cCI6MTUxOTc0MjQ0NX0.uiy0FsFWTq8LpUNuAyHLRCCo6MOvekErIq4FQFKTyxY',10,'1983-01-28','es'),
(1004,1026581785,'Yarid Katherine','Ballen Guzman','yballen','7ac9a425c4842792a904219cf61ff566','A','+57','3124241439',2,'NO','1519076502896-331.jpg',1,'2018-02-19 16:41:19','2018-05-18 20:09:12','2018-02-19 16:45:03','2018-08-31 21:50:13','bc4acf468b40d07b','Moto E (4) Plus','Android','7.1.1','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6InliYWxsZW4iLCJleHAiOjE1NTc3OTYxNTJ9.TJQRhbCi-fRpcWOhOzTUAUymkitqzpXX1eeV0TYYOV4',25,'1995-02-18','es'),
(1005,1020791553,'Andres Felipe','Barahona Linares','abarahona','a5690161cb1a828ccaa23e3a0c8172ec','A','+57','3118476261',2,'NO','1527192160451-447.jpg',1,'2018-05-24 15:00:06','2018-06-22 17:36:50','2018-05-24 15:02:49','2018-06-27 19:07:17','6176d318dee4383a','Moto G (5)','Android','7.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImFiYXJhaG9uYSIsImV4cCI6MTU2MDgxMTAxMH0.5pvRdp4fqP-kUF4aF-jnlndxtQuEtwByab-dy7kihro',11,'1994-04-04','es'),
(1006,52017960,'Adriana Marcela','Becerra D´alleman','abecerra','34605919315921a14b024e4c04a8ad05','A','+57','3215683733',2,'SI','nouser.png',1,'2018-01-05 16:01:09','2018-06-07 10:42:43','2018-01-05 16:01:17','2018-09-11 20:07:25','373b6928eaf69e17','Moto G (5)','Android','7.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImFiZWNlcnJhIiwiZXhwIjoxNTU5NDkwMTYzfQ.GPsxeTx09ZUS-s9VcWq3ZVHTOrQETrQkVPQ8hCmV55Y',18,'1971-03-30','es'),
(1007,1019071078,'Laura Victoria','Buitrago Diaz','lbuitrago','93152829e930d7ad866ffe3dd07577d8','A','+57','3183760290',2,'NO','nouser.png',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,9,'1992-04-15','es'),
(1008,52183622,'Diana Carolina','Buitrago Orozco','dbuitrago','f1c497c3f6493030df49e0484412e3a0','A','+57','3148752042',2,'NO','nouser.png',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,8,'1975-03-09','es'),
(1010,79688844,'Sergio Andres','Camargo Cáceres','scamargo','8e6c843ffefc199847883537417be292','A','+57','3215180006',2,'NO','nouser.png',1,'2017-11-30 11:07:07','2018-05-08 15:45:11','2017-11-30 11:07:38','2018-07-05 11:27:51','61353D65-1F16-4E1E-814B-0B79253BAE54','iPhone5,1','iOS','10.3.3','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6InNjYW1hcmdvIiwiZXhwIjoxNTMwOTk2MzExfQ.ftfxi-7K-8LT3YWU04wwhkaoA41-A0d_WT7Jplbx9Aw',11,'1975-07-24','es'),
(1011,1019050669,'Francisco Javier','Cardenas Moreno','fcardenas','b8ee9cc12d2a0f4362b362e7d554df86','S','+57','3106470452',2,'NO','nouser.png',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'1990-07-24','es'),
(1012,1010199514,'Juan Sebastian','Carvajal Tarazona','jcarvajal','6c351b4836defdf8eb34563d2ab40d9d','A','+57','3153928935',2,'NO','1516243765703-793.jpg',1,'2018-01-17 11:11:51','2018-01-17 11:11:51','2018-01-17 11:12:00','2018-02-14 07:27:57','aa315f57fede6105','HUAWEI GRA-L09','Android','5.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImpjYXJ2YWphbCIsImV4cCI6MTUyMTM4OTUxMX0.iI1dxYcMjjEqw_SuRtl6OfatQ2CWWOclJ3qpvvVkqew',12,'1991-09-17','es'),
(1013,1020768830,'Lizeth Paola','Castro Munoz','lcastro','90363d42e16813aecbfc8a08992fa997','S','+57','3132437835',2,'NO','nouser.png',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'1992-02-03','es'),
(1014,22520440,'Shirley Maria','Cervantes Cantillo','scervantes','b76e5d2ab22adc7f33c2beec907eae86','S','+57','3145953046',2,'NO','nouser.png',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'1979-09-30','es'),
(1015,32794996,'Angela Patricia','Cuello Barrios','acuello','1f2612a59b1c37545c50533a74d799b9','A','+57','3013566274',2,'NO','nouser.png',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,13,'1976-07-24','es'),
(1016,1143425107,'Nelson Manuel','Cuentas Muñoz','ncuentas','d0406c2600df7e318542f89c8e40e65e','A','+57','3002945202',2,'NO','1511971474354-854.jpg',1,'2017-11-29 10:57:36','2018-05-18 20:08:26','2017-11-29 11:05:06','2018-08-19 19:10:23','41a5d4c0f9be2243','SM-G935F','Android','7.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6Im5jdWVudGFzIiwiZXhwIjoxNTU3Nzk2MTA2fQ._JVFjmOZOPcZf0VknjEX0gt4ZDZtW_WpjoUnApvWMN0',14,'1989-12-21','es'),
(1017,52909435,'Patricia','Daza Baquero','pdaza','bb9dc627f0929bf8d4aea42f12fa7631','A','+57','3214522538',2,'NO','1519746121214-114.jpg',1,'2018-02-27 10:28:57','2018-07-05 16:33:01','2018-02-27 10:42:14','2018-09-01 10:11:11','3e077b6c84bbfd87','PRA-LX3','Android','7.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6InBkYXphIiwiZXhwIjoxNTYxOTMwMzgxfQ.7PZ9-Tc8VtrMKagwvwAEblAw1CnGpe742aRHnJbVFiA',14,'1983-02-17','es'),
(1018,37712378,'Zulma Xiomara','Delgado Cabeza','zdelgado','0119c080e0d56805f36477bae3f89f5e','A','+57','3103746352',2,'NO','1521133283038-1034.jpg',1,'2018-03-06 14:54:57','2018-05-18 20:13:25','2018-03-15 12:01:32','2018-09-09 10:02:28','5f82995d702beb66','TRT-L53','Android','7.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6InpkZWxnYWRvIiwiZXhwIjoxNTU3Nzk2NDA1fQ.PO1ysIYeOF5VSBz7TS2o8mprej5itKeHvrcWwv1DUio',15,'1978-08-11','es'),
(1019,1019015496,'Ángela','Díaz Contreras','adiaz','6886556ccaa51f6f1e173ff4960e2569','A','+57','3174003803',2,'NO','1527004751253-623.jpg',1,'2018-02-26 10:12:16','2018-05-21 09:58:41','2018-05-22 11:00:44','2018-09-11 09:23:25','C22ACD94-900B-4265-80CE-C45E601094D1','iPhone9,3','iOS','11.3.1','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImFkaWF6IiwiZXhwIjoxNTU4MDE4NzIxfQ.h1oolqkxDzecTi7qOZLwTgFXcyL9pavy6n6Sm9ySQ1g',31,'1987-05-31','en'),
(1020,1020738651,'Daniel Andres','Fernandez Rodriguez','dfernandez','0b0a1c6e26d6c6c4f3ea62715ccce016','S','+57','3183623280',2,'NO','nouser.png',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'1988-09-19','es'),
(1021,1072663367,'Carolina','Garcia Galvis','dgarcia','89e6c159f01e157425cc95416634445f','A','+57','3057018904',2,'NO','1524186745478-644.jpg',1,'2017-11-29 14:40:48','2018-04-19 10:15:29','2018-04-30 12:16:04','2018-06-07 13:16:00','abef00d0a0a1769d','SM-A520F','Android','7.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImRnYXJjaWEiLCJleHAiOjE1MjkzMzQ5Mjl9.5W4tblZP7faQclDbJe0M667TsGTcXFIGuCZQWyEYxT0',46,'1991-12-04','es'),
(1022,1014181334,'Sergio Andres','Garzon Corzo','sgarzon','0f6edb089b9aa4b0116467c27dbc12dd','S','+57','3102177420',2,'NO','nouser.png',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,'1986-12-01','es'),
(1023,1032367058,'Andrea','Gualtero Polania','agualtero','8a82994fb4fe2b7b0f9664f364b169a0','A','+57','3205818612',2,'NO','nouser.png',1,'2018-08-21 10:19:41','2018-08-21 10:19:41','2018-08-21 10:19:45','2018-09-09 12:11:02','682AA937-B8BC-4639-8396-5799A4916E8E','iPhone9,1','iOS','11.4.1','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImFndWFsdGVybyIsImV4cCI6MTU2NTk2ODc4MX0.ZOs1mJhNlTNkJX5oB_pkN-Bssf8ohRmJpjRfOQIMvvc',19,'1986-07-21','es'),
(1024,36348030,'Lilibeth','Gutierrez Cordoba','lgutierrez','e10adc3949ba59abbe56e057f20f883e','A','+57','3215198853',2,'NO','1517638589748-1080.jpg',1,'2017-12-06 18:49:55','2018-07-27 09:44:45','2018-07-27 10:00:38','2018-09-13 21:00:11','503f7402a7cae307','LG-K350','Android','6.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImxndXRpZXJyZXoiLCJleHAiOjE1NjM4MDY2ODV9.TTN2OqrZtHYPbRVBQ_ygQwQRFwl-If5OocmPO8-CP3o',48,'1985-02-08','es'),
(1026,1030560874,'Jose Hernan','Herrera Rodriguez','jherrera','f012eefbc984383fbc284995cf2e4e84','A','+57','3232038248',2,'NO','nouser.png',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,'1989-09-01','es'),
(1027,79880887,'Leonardo','Iregui Albarracin','liregui','375038bd9b0a7f5f8f0a33061600606a','A','+57','3143674571',2,'SI','1511989518157-537.jpg',1,'2017-11-29 16:04:20','2018-08-30 12:46:31','2017-11-29 16:05:20','2018-09-10 10:14:56','DDDC5331-A413-476E-A936-3C9CBC599148','iPhone7,1','iOS','11.4.1','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImxpcmVndWkiLCJleHAiOjE1NjY3NTUxOTF9.JAkBzPouWEWddggjKdZershPsoZIR6ehefTeNiWBpCo',29,'1979-09-20','es'),
(1028,52811450,'Dallan Astrid','Jauregui Orozco','djauregui','3a932caa09d06fbcbcd705cd30f39a80','S','+57','3205422879',2,'SI','1532362847061-422.jpg',1,'2017-12-01 16:34:32','2018-08-09 10:37:19','2018-04-22 06:20:00','2018-09-11 05:49:20','83f7db9183d58b8','RNE-L03','Android','8.0.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImRqYXVyZWd1aSIsImV4cCI6MTU2NDkzMzAzOX0.hLVA_vNa5qm5UQbyCUE_Oi623w3IOk9kVrz0csyHV00',24,'1982-05-15','es'),
(1029,53068062,'Carolina','Jimenez Gutierrez','yjimenez','b793ad2d6fdaf63808b79d769435d4b0','S','+57','3203133743',2,'NO','1513281798622-466.jpg',1,'2017-12-14 14:47:57','2018-08-01 10:26:13','2017-12-14 14:50:51','2018-04-24 13:44:01','a41ae0bc552620a5','TRT-L53','Android','7.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6InlqaW1lbmV6IiwiZXhwIjoxNTY0MjQxMTczfQ.ZnliyTNqgur66HT9Tk5dWqZtIhIFAsXmXWm78cJ3mUw',25,'1985-04-11','es'),
(1030,1020783592,'Maicol Dayan','Jimenez Quintero','mjimenez','b7dccdb31a5a7274edfd9cf1fedfb6fc','S','+57','3184085440',2,'NO','nouser.png',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'1993-07-31','es'),
(1031,80001822,'Oswaldo ','Kappler Castro','okappler','aebdfe8c31385b1568c14b5e964dd170','S','+57','3215392296',2,'NO','1511984417325-880.jpg',1,'2017-11-29 14:38:53','2018-01-26 15:28:24','2017-11-29 14:40:20','2018-02-01 23:01:41','947475943c4126ca','TRT-L53','Android','7.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6Im9rYXBwbGVyIiwiZXhwIjoxNTIyMTgyNTA0fQ.EiWsM2YWxcSIKna5FWZLhiMhEOQUmKlY9T2aSoLE5Zg',11,'1978-12-22','es'),
(1032,1016001613,'Diana Katherine','Lagos Primiciero','dlagos','394e85a9e7229c6deb7ffa97cc64cd5c','S','+57','3103281317',2,'NO','nouser.png',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,'1987-03-18','es'),
(1033,80064634,'Juan Pablo','Londoño Villaraga','jlondono','051e8302ad01391428954f76ae9f74b7','A','+57','3006006187',2,'SI','nouser.png',1,'2017-11-30 08:10:05','2018-05-18 09:29:48','2017-11-30 08:10:12','2018-09-13 10:42:45','f5d0ddfd013899a6','SM-A910F','Android','7.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6Impsb25kb25vIiwiZXhwIjoxNTU3NzU3Nzg4fQ.VWuNgemxYL9izi2MgrHEegZwZRyj4TTBc-O2Ty6SBJY',7,'1979-07-19','es'),
(1034,80439148,'Camilo Javier','Mariño Reyes','cmarino','a2fc033f8a8b5ff135d898b8c72b441f','S','+57','3135741395',2,'NO','nouser.png',1,'2017-12-04 17:02:02','2018-07-24 09:47:30','2017-12-04 17:02:16','2018-07-06 10:10:39','91f643ff2b90d96f','BLA-L29','Android','8.0.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImNtYXJpbm8iLCJleHAiOjE1NjM1NDc2NTB9.QLX57NoEn1nwf8ZdOvL9_FIgtlgRE9eccSe4G8ybMRM',12,'1971-04-20','es'),
(1035,52497261,'Andrea','Martinez Ospina','mmartinez','018a586104ea252d9caf564b70debe28','A','+57','3224227828',2,'NO','nouser.png',1,'2018-03-21 08:18:41','2018-08-23 10:03:48','2018-05-18 20:08:56','2018-05-29 11:31:05','10C68EFC-529D-4C0A-85C4-82FB22036D83','iPhone7,2','iOS','11.4','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6Im1tYXJ0aW5leiIsImV4cCI6MTU2NjE0MDYyOH0.XnXHPWWU5FYpABElS1BaWtdr5ZTSFJ58JlouEwZGifY',13,'1979-03-28','es'),
(1036,1023921665,'Ginna Paola','Mendez Tenza','gmendez','c085e00b8e4a6951399983b256ef0ed1','S','+57','3115255267',2,'NO','nouser.png',1,'2017-12-11 17:15:15','2018-03-09 12:22:24','2017-12-11 17:15:22','2018-02-27 11:54:52','83f7db9183d58b8','RNE-L03','Android','7.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImdtZW5kZXoiLCJleHAiOjE1MjU4MDAxNDR9.iRR-iCYU3-r7m7kWwur8vAk38FnEL8Mq0IFARSzXf6Q',11,'1992-12-26','es'),
(1038,7318160,'Pablo Yair','Murcia Roncancio','pmurcia','43426edead757dd0baae48f8f74ec0dd','A','+57','3212546635',2,'NO','1519330145567-385.jpg',1,'2017-11-29 19:17:44','2018-06-24 20:12:11','2018-02-22 15:09:17','2018-09-13 10:05:52','1f21928dab693884','Redmi Note 4','Android','7.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6InBtdXJjaWEiLCJleHAiOjE1NjA5OTMxMzF9.aIV9GXahN7n7MN3PQKoolZaaS1-L0ppbMNqBaiAruas',15,'1982-05-07','es'),
(1039,74082329,'Diego Andrés','Naranjo Rincón','dnaranjo','92a5a266ffe0d886a1ebb6cbf1846b22','A','+57','3115028113',2,'NO','nouser.png',1,'2017-11-29 15:34:55','2018-06-22 17:34:51','2017-11-29 15:35:19','2018-09-08 15:07:01','936ecf0e471779d7','SM-G610M','Android','7.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImRuYXJhbmpvIiwiZXhwIjoxNTYwODEwODkxfQ.5gYnW4QxuTmPI67wEZ6K0Pcr-Mj2KPPvvwXB4gQu1Ww',3,'1983-12-11','es'),
(1040,98400411,'Carlos Alberto','Ocampo Moncada','cocampo','971c7e46ffbcc46403a09dbf9b599fcc','A','+57','3118597899',2,'NO','1523037663203-854.jpg',1,'2017-12-21 11:00:26','2018-07-18 11:04:20','2018-04-06 13:01:16','2018-08-24 05:49:12','e270012532c11328','ZTE Blade A475','Android','5.1','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImNvY2FtcG8iLCJleHAiOjE1NjMwMzM4NjB9.4DEoMOYLpBFWFs-iPCU2d6_zJ5PbSUbmXzKRZp9l7s4',16,'1978-06-20','en'),
(1041,80001861,'Juan Camilo','Orduz Alfonso','jorduz','b7fedc22ec2d801ee3dd61822a984f8d','A','+57','3013368497',2,'NO','nouser.png',1,'2017-11-29 14:12:14','2018-05-18 20:09:33','2017-11-29 14:20:02','2018-09-13 13:39:03','7BC221F8-8C7C-44DA-94B2-D8B38FE233B8','iPhone9,1','iOS','11.3.1','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImpvcmR1eiIsImV4cCI6MTU1Nzc5NjE3M30.bxRyE_uNASibFpfa3_gB3oiz2BXYGyBZSuhk26NpG20',16,'1979-01-17','es'),
(1042,52098800,'Omaira','Paez Muñoz','opaez','da7d3bd094be48a12330c01e62ae42cb','A','+57','3133008837',2,'NO','1512535508598-633.jpg',1,'2017-12-05 23:33:07','2017-12-05 23:33:07','2017-12-05 23:45:35','2018-01-14 11:56:37','ca7da27dc3fc91b2','HUAWEI LUA-L23','Android','5.1','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6Im9wYWV6IiwiZXhwIjoxNTE3NzE4Nzg3fQ.lzbowZtx94ElQ9hSrpFgkpF8gssDxJLX585EMkGkhBI',15,'1971-11-18','es'),
(1043,1024517585,'Dana Jilieth','Paez Pacheco','dpaez','c346964341d1b35e7e952181a9e330db','S','+57','3102868828',2,'NO','nouser.png',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,'1991-05-22','es'),
(1044,1032464092,'Andres Felipe','Patiño Guaneme','apatino','bcf49787dd67c646d3381a8e4657ebe0','S','+57','3058197614',2,'NO','nouser.png',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,8,'1994-06-04','es'),
(1045,1032423372,'Sebastian','Peña Ocampo','jpena','568ea582018e8ae702be2f22f24abd0b','A','+57','3125247741',2,'NO','1531398326656-1006.jpg',1,'2017-11-29 14:11:04','2018-06-07 17:20:04','2017-11-29 14:11:21','2018-09-06 07:38:41','8ACEBDD1-7311-4503-9CF6-54EEDB745212','iPhone7,2','iOS','11.3.1','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImpwZW5hIiwiZXhwIjoxNTU5NTE0MDA0fQ.dt8HstloRb99Tpuy4qGGciaSzpCmxP6-HgnglMyyRqI',14,'1988-10-17','es'),
(1046,1033791027,'Brayan','Pinilla','bpinilla','eabe6968d359009f34cda8602ecd325a','A','+57','3224759362',2,'NO','1531271296351-661.jpg',1,'2017-12-11 13:30:53','2018-07-15 12:42:25','2018-07-10 20:08:24','2018-08-12 22:22:18','2743323dea96c43f','ALE-L23','Android','5.0.1','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImJwaW5pbGxhIiwiZXhwIjoxNTYyNzgwNTQ1fQ.8QISSFEh-6zefBZ3uYCcaBhCWta1u1VVK-_CS2HbC7c',34,'1996-08-30','es'),
(1047,1020830376,'Juliana Andrea','Plata Mojica','jplata','1a9ebeca1abfaa9201057b997b0266f0','S','+57','3193140598',2,'NO','1517835561873-790.jpg',1,'2018-02-05 07:58:40','2018-05-09 19:29:01','2018-02-05 07:59:29','2018-05-21 09:48:56','452335731d3406f2','PRA-LX3','Android','7.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImpwbGF0YSIsImV4cCI6MTUzMTA5NjE0MX0._ogtpln-GYo_2PDqYWXiz3mkgMBdRpUXSp264Mcov00',27,'1998-02-04','es'),
(1048,79687536,'Carlos Alberto','Preciado Barahona','cpreciado','7c5a09ae136937623a8e373274178872','A','+57','3006085779',2,'NO','nouser.png',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,18,'1975-06-11','es'),
(1049,79497338,'Julio Cesar','Pulido Combita','jpulido','5a68c5b3c4b65b475e675bc2e6e8f993','A','+57','3148753315',2,'NO','nouser.png',1,'2018-02-08 10:08:35','2018-06-22 09:13:09','2018-02-08 10:08:44','2018-09-14 09:25:57','7e42729b8164ac18','Moto G (4)','Android','7.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImpwdWxpZG8iLCJleHAiOjE1NjA3ODA3ODl9.2k44b18gLuQosFq2EcugUFrFSWdoUCEtQcIQ23su-IY',15,'1969-08-10','es'),
(1050,1151934523,'Daniella','Racines Lalinde','dracines','53898888a69960d485d9040abfc08d21','S','+57','3174230500',2,'NO','nouser.png',1,'2018-01-05 09:15:31','2018-05-16 22:35:37','2018-01-05 09:15:36','2018-05-18 20:16:06','6DE1EA1D-8E9C-46AC-B514-2FC30F230385','iPhone9,1','iOS','10.2.1','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImRyYWNpbmVzIiwiZXhwIjoxNTMxNzEyMTM3fQ.JRFM9qAAUgjBkBM6ZayTA0_LKI3mjHJnYlOIIyKaH9g',5,'1990-03-08','en'),
(1051,1073694507,'Jeisson David','Ramirez Huelgos','jramirez','82f41cc9625bd676b19a67ab3a1e9ea4','S','+57','3046033931',2,'NO','nouser.png',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'1992-05-29','es'),
(1052,1013643644,'Sebastian Camilo','Ramirez Riano','sramirez','561b4f500c82341fee842c34b620c849','A','+57','3192338067',2,'NO','1511986121468-465.jpg',1,'2017-11-29 15:07:03','2018-06-22 16:13:48','2017-11-29 15:08:48','2018-06-23 20:56:16','bfc425a932745602','ASUS_X018D','Android','7.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6InNyYW1pcmV6IiwiZXhwIjoxNTYwODA2MDI4fQ.KDKqkrnjLdzT6OUohFk5cB-w3UxiRaAZd-jJUOdT0oM',19,'1993-09-19','es'),
(1053,1032446687,'Christian Camilo','Ramirez Urrego','cramirez','ea2b18a2a9fbe39e915cb4ab153a2d2c','S','+57','3195530546',2,'NO','nouser.png',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,'1991-10-21','es'),
(1054,32649144,'Emma Cecilia','Reatiga De La Hoz','ereatiga','7ceedb1b6ca1bf743dc4b33e8768e1e7','A','+57','3114188906',2,'NO','nouser.png',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,'1961-05-11','es'),
(1055,91500243,'Juan Pablo','Remolina Botero','jremolina','3204aec5961d2e64f6eb3deee8ae42ad','A','+57','3118766293',2,'NO','nouser.png',1,'2017-11-29 14:36:16','2018-07-12 10:04:53','2017-11-29 14:36:26','2018-09-13 12:24:15','76891190-0A77-42CE-9748-61C6B2DB1845','iPhone9,3','iOS','11.4','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImpyZW1vbGluYSIsImV4cCI6MTU2MjUxMTg5M30.3c-ZYdNmmuXfneCuOtyJMuyewHcRNcetoj0wT8Uxv58',11,'1977-06-14','es'),
(1056,52694831,'Claudia Patricia','Rico Mogollon','crico','cea64f8a46d33a6095a68fb2addd9b3d','A','+57','3215180007',2,'NO','nouser.png',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,'1979-01-28','es'),
(1057,1013612470,'Camilo Andres','Rincon Rodriguez','crincon','aa492410448fdef53c5ffa9d9f7347fc','A','+57','3105874698',2,'NO','nouser.png',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,9,'1990-06-03','es'),
(1058,1019025618,'Ivan Felipe','Rodriguez Aguilar','irodriguez','71ec1b10b3071fa78290a12a2df162e6','S','+57','3183939203',2,'NO','nouser.png',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'1988-07-15','es'),
(1059,1020756176,'Oscar Daniel','Rodriguez Nudelman','orodriguez','f8fd1fb8e30a17215fefdc9c1eaec45c','A','+57','3162212016',2,'NO','nouser.png',1,'2018-06-08 18:49:49','2018-06-08 18:49:49','2018-06-08 18:49:57','2018-07-19 23:01:22','C93CC4D0-E3A9-4660-892A-EEFB383BD49C','iPhone7,1','iOS','11.4','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6Im9yb2RyaWd1ZXoiLCJleHAiOjE1NTk2MDU3ODl9.Qi5GO80C4JnogW5BqHWoeT4gHE0tOhR9ISRcpgqU4ak',9,'1990-09-26','en'),
(1060,52253099,'Carolina','Romero Garcia','cromero','bb52e6e28bfb0fedbcb12ad8c65b7363','A','+57','3114188908',2,'SI','nouser.png',1,'2017-12-07 07:46:05','2018-08-29 16:12:01','2017-12-07 07:46:09','2018-05-01 19:42:38','465A4DB0-503B-4AF1-921C-29E59BE04856','iPhone8,1','iOS','11.4.1','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImNyb21lcm8iLCJleHAiOjE1NjY2ODExMjF9.ju5aIzj7RLvOonSEdD7OIoYFpzGs2K1boIoVQeZgQt8',15,'1975-01-02','es'),
(1061,1031138982,'Martha Julieth','Santana Ayala','msantana','dc59ffaaf0367077c113044afe36645d','S','+57','3102306519',2,'NO','nouser.png',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,'1992-06-17','es'),
(1062,52780970,'Laura Angelica','Serrano Alvarez','lserrano','2b76a6361be61b01b594aaa43bb5dd7e','A','+57','3135420993',2,'NO','nouser.png',1,'2017-11-29 11:02:26','2018-08-30 16:54:10','2017-11-29 11:02:36','2018-09-13 10:42:51','4d0c622a551c94dd','SM-J700M','Android','6.0.1','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImxzZXJyYW5vIiwiZXhwIjoxNTY2NzcwMDUwfQ.c2ttkdpvG8yH79CwCC9qXsX6Ug8KDauNWM1Uh0IWwoQ',8,'1984-11-27','es'),
(1063,7365896,'Carlos','Sigua Rodriguez','csigua','b856371d564801a823d825c1d4886fb6','A','+57','3114814139',2,'SI','1512497073808-802.jpg',1,'2017-11-29 10:58:34','2018-05-21 15:37:40','2017-12-05 13:04:41','2018-08-22 23:24:55','D69781E6-C7F9-4A9D-90B7-B1D80460E081','iPhone9,3','iOS','11.3.1','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImNzaWd1YSIsImV4cCI6MTU1ODAzOTA2MH0.akJ_Om7VFCeohMTYOa3qqTNj3J8y_GMgZI2-KoI3l0k',23,'1983-06-14','es'),
(1064,1014259744,'Ivan Isidro','Tibana Sepulveda','itibana','dd61a9e6cd986b87439911a0d202e1f5','A','+57','3103054626',2,'NO','1518103828620-853.jpg',1,'2018-02-08 10:29:39','2018-05-21 13:30:25','2018-02-08 10:30:31','2018-08-29 08:33:18','401b2582abe50cb','MotoG3','Android','6.0.1','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6Iml0aWJhbmEiLCJleHAiOjE1NTgwMzE0MjV9.Noxfd2LGcwMkTuqaLz8bSizXv8xqHYacOgRFAFLLMX8',22,'1994-11-07','es'),
(1065,80503430,'Jaime Francisco','Torres Romero','jtorres','e9dc983f59442fd46d0721f469175861','A','+57','3114188911',2,'SI','1512354840136-913.jpg',1,'2017-12-03 21:27:23','2018-06-29 12:19:24','2017-12-03 21:34:05','2018-09-14 10:45:45','544FB97B-CE25-4F6E-B191-9CA043EEEB3C','iPhone9,4','iOS','11.4','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6Imp0b3JyZXMiLCJleHAiOjE1NjEzOTY3NjR9.XlJWzrtOU7OypjBsf_XlTAlT4X8G14dvGa2uZ0sPDNY',39,'1973-07-21','es'),
(1066,52518845,'Olga Lucía','Useche Pérez','ouseche','910e967e3a1da3ade029cc92a56d6923','A','+57','3113369644',2,'NO','nouser.png',1,'2018-02-08 10:21:41','2018-04-20 18:56:42','2018-02-08 10:21:46','2018-04-30 13:42:37','f4e66b4eb0f3d8cb','SM-G903M','Android','5.1.1','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6Im91c2VjaGUiLCJleHAiOjE1Mjk0NTI2MDJ9.PV03EaXTyPoWdKHXrgfjZdWyuHCq5225ysNzbzDLZQk',13,'1977-09-24','es'),
(1067,1098689321,'Hans Cristian','Vargas Cuervo','hvargas','1fd4444482301ff30a81b275d07e2821','S','+57','3204596449',2,'NO','nouser.png',1,'2017-11-29 14:44:41','2018-07-22 20:51:32','2018-01-04 13:23:20','2018-06-29 18:25:58','682AA937-B8BC-4639-8396-5799A4916E8E','iPhone9,1','iOS','11.4','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6Imh2YXJnYXMiLCJleHAiOjE1NjM0MTQ2OTJ9.Q5iI4FyxnTuop-AVeOETDj-fmYtzKGha8B9aoYUVcnk',9,'1990-04-14','es'),
(1068,1020789593,'Maria Camila','Vargas Sanchez','mvargas','422b22fc2fbff38a071ec5cfa0eae38c','A','+57','3138175590',2,'NO','nouser.png',1,'2018-02-09 10:29:39','2018-02-09 10:29:39','2018-02-09 10:29:50','2018-05-21 08:41:01','B23F1174-B7A7-43E2-8486-55F2BAB54D81','iPhone9,3','iOS','11.2.2','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6Im12YXJnYXMiLCJleHAiOjE1MjMzNzQxNzl9.nGOgNDMzW_pbawBAFPcqSPKjZfyC3KOQYPoRkeiOaLM',11,'1994-01-27','es'),
(1069,80883134,'Hervin Yarley','Velez Torres','hvelez','a8fd07dd9bccd378f7749fa65eef747f','A','+57','3145810969',2,'NO','nouser.png',1,'2018-06-22 16:26:36','2018-06-22 16:26:36','2018-06-22 16:26:46','2018-08-05 09:26:28','bd5d7b01902b977c','ASUS_X00BD','Android','5.1.1','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6Imh2ZWxleiIsImV4cCI6MTU2MDgwNjc5Nn0.xKYiiNFzjxs6brKxJB9PDPPzy98PjrvVHyhWihkTIVE',9,'1986-01-04','es'),
(1070,52252421,'Olga Lucia','Arciniegas','oarciniegas','92ddfed297362132e356697fef136814','A','+1','9728910678',2,'SI','1512101188135-999.jpg',1,'2017-11-30 23:01:32','2018-07-10 12:46:49','2017-11-30 23:06:37','2018-09-03 10:09:24','D533271F-FA11-4A91-9AC9-71B761C8AD18','iPhone9,4','iOS','11.4','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6Im9hcmNpbmllZ2FzIiwiZXhwIjoxNTYyMzQ4ODA5fQ.Vv_2lBLDeLd9aqVzGcKWHF1nka5iZHjL0u2Nfd_NNT',28,NULL,'en'),
(1071,65737885,'Laura','Penagos','lpenagos','2496fd8a36a94872aea250235dc03866','A','+507','62381100',2,'NO','1535313582975-396.jpg',1,'2017-12-05 09:50:03','2018-07-14 14:42:00','2018-08-23 19:54:41','2018-09-14 09:05:09','FD942F79-A052-448E-BE66-6938F201D9BD','iPhone10,5','iOS','11.4','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImxwZW5hZ29zIiwiZXhwIjoxNTYyNzAxMzIwfQ.Llw40Xbxp7F-0WqaBlUeBYhgpcHeXsOb9Yp4c19XC2E',26,'1971-04-20','es'),
(1072,6320814,'Andrés','Carvajal Prado','acarvajal','bf4682e4378f441cc92903aa16188996','A','+57','3134627505',2,'NO','1516129111706-353.jpg',1,'2017-12-05 15:01:21','2018-06-14 12:03:06','2018-01-16 13:58:37','2018-09-06 07:53:14','3c7d05c6ff96f7db','WAS-LX3','Android','7.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImFjYXJ2YWphbCIsImV4cCI6MTU2MDA5OTc4Nn0.NzCl5T6NIdqn2_SMrQ1TGImTwmDHJIE1TS0aUlSQ_qc',27,'1978-08-23','es'),
(1073,1031165354,'Leidy','Lopez Bonilla','llopez','d60a7b6af33fb202c7974ef40f1952d4','A','+57','3107502123',2,'NO','1515075211692-873.jpg',1,'2018-01-04 09:12:54','2018-05-18 14:40:51','2018-01-04 09:13:38','2018-09-01 18:36:15','bbeb40d424f8428b','SM-G610M','Android','7.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6Imxsb3BleiIsImV4cCI6MTU1Nzc3NjQ1MX0.t008issGEBLm53hDQY-tgOcOK4AigbP2W2kWTDTxaj0',28,'1996-06-11','es'),
(1074,1122330011,'Adriana','Mejía','amejia','e10adc3949ba59abbe56e057f20f883e','A','+57','3175861081',2,'SI','1516380087638-206.jpg',1,'2018-01-19 11:40:02','2018-06-05 13:04:22','2018-03-06 16:54:54','2018-08-24 17:00:04','A7801E39-28DA-4A7F-9817-6E3AA1D33EE9','iPhone9,3','iOS','11.3.1','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImFtZWppYSIsImV4cCI6MTU1OTMyNTg2Mn0.PhKOWZaXsXDsiRCJisb3-AVdZf9VXHGe_L5fGcCRdjc',43,NULL,'es'),
(1075,1144032942,'Luisa','Soto Granada','lsoto','f1b863d06b22835bf89364fd427797f2','A','+57','3163067580',2,'NO','nouser.png',1,'2018-02-08 11:04:18','2018-08-06 12:55:55','2018-02-08 11:04:39','2018-08-06 12:54:42','66AABAD4-D38A-49E1-935D-7C65882F8ED7','iPhone9,3','iOS','11.4.1','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6Imxzb3RvIiwiZXhwIjoxNTY0NjgyMTU1fQ.bXsvVQl6TPvGSg4XoanlqeY0TZiEvDAJMB5eDQ1eERk',21,'2018-02-22','en'),
(1076,79248984,'Jaime','Perez Galindo','jperez','1bc9c77e79dff702c3a3ab7f2cb3c78a','A','+57','3023693390',2,'NO','nouser.png',1,'2018-04-20 12:29:28','2018-07-19 15:22:40','2018-04-20 12:29:41','2018-08-31 21:50:18','422e8240d7c5df22','E6603','Android','7.1.1','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImpwZXJleiAiLCJleHAiOjE1NjMxMzU3NjB9.0MCmvM1Ti2-Rp6urAeBQ9rkzH6kVUDj01VgpDxB2iNg',7,NULL,'es'),
(1077,1031137616,'John Alejandro','Niño Morales','jnino','2e936e2db3145ef2520c55e62d064d97','A','+57','3004872567',2,'NO','nouser.png',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,'1992-04-06','es'),
(1078,80853491,'Leonardo','Sánchez Rodriguez','lsanchez','7bb8101ab14f6065dc7a8f9ba8796e11','A','+57','3142992226',2,'NO','nouser.png',1,'2018-02-09 09:47:42','2018-02-09 09:47:42','2018-02-09 09:47:55','2018-05-31 15:14:03','e6988f472b2ff784','HTC Desire 10 lifestyle','Android','6.0.1','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImxzYW5jaGV6IiwiZXhwIjoxNTIzMzcxNjYyfQ.fkhdPZb9bPy4qn-tE85hI7dFdUefwXIuZyxJ7I1qEsE',1,'1985-05-31','es'),
(1079,1065639534,'Kevin','Arregoces','karregoces','ddc4bd9e94469566d3bdb5055e4b7b4c','S','+57','3046703973',2,'NO','nouser.png',1,'2018-02-19 08:37:50','2018-02-19 08:40:58','2018-02-19 08:41:00','2018-05-31 10:09:37','0AA74E55-868F-4A72-8CF9-56AD37BBE4A4','iPhone7,2','iOS','11.1.2','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImthcnJlZ29jZXMiLCJleHAiOjE1MjQyMzE2NTh9.X2NE5IMC2ryG6Y7dAtAHZCgFiFxkuy3srztmKavmMRY',1,'1992-03-31','es'),
(1080,1014206412,'Sergio','Lopez Veloza','slopez','c8dd3da47f6fc711a2743022af2e214e','A','+57','3015371523',2,'SI','1519173409609-142.jpg',1,'2018-02-19 11:20:14','2018-06-19 15:13:57','2018-02-19 11:20:22','2018-09-14 11:44:27','a70f64161df8ebeb','HUAWEI NXT-L09','Android','6.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6InNsb3BleiIsImV4cCI6MTU2MDU0MzIzN30.44g3AxFFJQq7gnmlm-3DMyNksSRRrvRkXLOX5_1n_88',26,'1989-12-21','en'),
(1081,80769063,'Andrés','Méndez','amendez','e91cdb855356329bb76e74ca1cdbdc7b','S','+57','3015525603',2,'NO','1519911608861-633.jpg',1,'2018-02-28 16:36:07','2018-09-02 20:04:46','2018-02-28 16:36:14','2018-05-24 13:02:03','484ec23f5626dd2f','ASUS_Z00AD','Android','6.0.1','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImFtZW5kZXoiLCJleHAiOjE1NjcwNDA2ODZ9.g1XF5ZJ2nnrO2YvDtmvL6Fe8G2rICd3kwZUo9ydyO9g',12,'1984-10-13','es'),
(1082,1013634745,'Helmut Rodrigo','Rico Moreno','hrico','a123bc4ee7ff618ca18b3b83cb9555c1','A','+57','3142495854',2,'NO','nouser.png',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,'1992-08-31','es'),
(1084,1018468246,'Daniel Eduardo ','Cadavid Gonzalez','dcadavid','735317fd56ca3c5c27de76ea0db1a428','A','+57','3167434137',2,'NO','nouser.png',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'1994-07-23','es'),
(1085,91158981,'Jhon Jairo','Alba Montaña','jalba','49e68cb253cfece7da2f01d30d7c8108','A','+57','3132762048',2,'NO','nouser.png',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,'1980-07-12','es'),
(1086,1026250568,'Manuel Fernando ','Palacios Valero','mpalacios','752ae8b8c58e316d8a234f613c709397','A','+57','3165312306',2,'NO','nouser.png',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'1986-03-10','es'),
(1087,80198024,'Andrés Hernando','Morales','amorales','485d7c7adcce79de2e77f75b10294ff2','A','+57','3125815193',2,'NO','nouser.png',1,'2018-08-31 21:56:09','2018-08-31 21:56:09','2018-08-31 21:56:14','2018-09-13 13:34:41','e603e7d92b0d1574','LG-M320','Android','7.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImFtb3JhbGVzIiwiZXhwIjoxNTY2ODc0NTY5fQ.-r955VBP0HrDkijqaBmluDbcPavzV5nut2NS7of-4vk',5,'1984-02-22','es'),
(1088,52708280,'Andrea ','Cortes Chacon','acortes','1ab3fb9d62aba215d46ab72a68cef689','S','+57','3166906632',2,'NO','nouser.png',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,'1980-04-01','es'),
(1089,39693516,'Angela Lucía','Torres Romero','atorres','8100c092ad42dc74c417e9eb2a2a19b8','A','+57','3173737137',2,'SI','nouser.png',1,'2018-04-30 10:53:48','2018-04-30 10:53:48','2018-04-30 10:53:52','2018-07-09 09:38:32','D94EB785-4FB6-490B-A5FE-788BFD72E877','iPhone7,2','iOS','11.2.6','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImF0b3JyZXMiLCJleHAiOjE1MzAyODc2Mjh9.q9y5ma6_weOQ9XDiahMLRRCnMVlwf9m33BC8-t9rVVY',2,'1966-01-10','es'),
(1090,52416151,'Claudia','Bernal Mejía','cbernal','ebb9a9f816732e78c569c2523b8eb1bf','A','+52','15519675729',2,'NO','1526164701657-515.jpg',1,'2018-05-11 10:59:06','2018-05-11 10:59:06','2018-05-12 17:38:26','2018-07-17 13:16:36','0E0BC59B-F4E1-4C36-9689-0EE579C88E45','iPhone6,1','iOS','11.3.1','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImNiZXJuYWwiLCJleHAiOjE1MzEyMzgzNDZ9.0YqcrtXzbce4q4NQZU2danMI72w4_jvIoi94y8ByLBw',33,'1979-05-31','es'),
(1091,1110516152,'Kelly','Oviedo Reinoso','koviedo','88cdbc3b017e77e761f8cd957402b6e2','A','+57','3132955615',2,'NO','nouser.png',1,'2018-05-11 17:51:07','2018-06-05 17:41:29','2018-05-11 17:51:22','2018-09-08 22:26:40','fdbbbf128fa39810','ANE-LX3','Android','8.0.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImtvdmllZG8iLCJleHAiOjE1NTkzNDI0ODl9.F8pXMqknD58F6vdjVPESOEGapLXZGRj2SHusdyVqN3I',11,'1991-09-19','es'),
(1092,82305,'Pedro Luis','Mazzino','pmazzino','2e8a87e9b76468995ff2b45c9f19f0cb','A','+54','91158482305',2,'NO','1531260075265-534.jpg',1,'2018-06-08 11:30:21','2018-06-08 11:30:21','2018-07-10 17:01:20','2018-09-12 07:16:32','BF8BFE54-CD54-4D7F-8DDD-E8D3EF98E80D','iPhone9,2','iOS','11.4','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6InBtYXp6aW5vIiwiZXhwIjoxNTU5NTc5NDIxfQ.NbprcP3kz6fW2sAgn-LEXwXk1UM6Z8VS-FDLTLFkiGY',14,NULL,'es'),
(1093,80426597,'Luis','Lleras','llleras','e10adc3949ba59abbe56e057f20f883e','A','+57','3114188912',2,'NO','nouser.png',1,'2018-05-31 17:26:50','2018-06-15 08:37:23','2018-05-31 18:10:34','2018-07-09 14:22:20','9177dad5acf0cc6e','SM-J500M','Android','5.1.1','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImxsbGVyYXMiLCJleHAiOjE1NjAxNzM4NDN9.7jasR99fswYs2ApOdvr33JST0UUqOT4yMZAr2MZMk2A',1,NULL,'es'),
(1094,75032,'R.W.','Holleman','rwholleman','e10adc3949ba59abbe56e057f20f883e','A','+1','9728146304',2,'NO','1536692813795-380.jpg',1,'2018-06-28 15:16:46','2018-09-13 16:13:43','2018-09-11 14:08:36','2018-09-13 16:57:10','658AFAB6-1395-45EF-9468-D1F98E491A17','iPhone10,2','iOS','6.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6InJ3aG9sbGVtYW4iLCJleHAiOjE1Njc5NzcyMjN9.77ozUydumhfwKRRk4S_XiszlVxPo9crzhp7gSXxz9kA',3,NULL,'es'),
(1095,1013648452,'Diego','Martinez','dmartinez','e10adc3949ba59abbe56e057f20f883e','A','+57','3177952817',2,'NO','nouser.png',1,'2018-07-03 11:32:21','2018-07-03 11:32:21','2018-07-03 11:49:27','2018-09-11 09:49:46','3ea4d0fc29c69b4b','WAS-LX3','Android','7.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImRtYXJ0aW5leiIsImV4cCI6MTU2MTczOTU0MX0.hDJhnJVXVYSFSoPxJm1ayfmymRIL37h-E5tqC17VI9w',2,NULL,'es'),
(1096,111213,'Beatriz','Zapata','bzapata','e10adc3949ba59abbe56e057f20f883e','A','+57','3155588305',2,'NO','nouser.png',1,'2018-07-03 20:08:27','2018-07-04 13:36:00','2018-07-03 20:08:33','2018-07-05 18:10:27','','','','','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImJ6YXBhdGEiLCJleHAiOjE1NjE4MzMzNjB9.kUdc2plIuyDb4g5Rg1GY64bhT2i1talaNgZjQ-w4VgE',1,NULL,'es'),
(1097,121113,'Andres','Montoya','amontoya','e10adc3949ba59abbe56e057f20f883e','A','+57','3104247776',2,'NO','nouser.png',1,'2018-07-03 20:51:28','2018-07-04 14:25:18','2018-07-03 20:51:55','2018-07-05 10:03:53','','','','','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImFtb250b3lhIiwiZXhwIjoxNTYxODM2MzE4fQ.3MK3mnwpo8sReyDNi4LwWlLwmpUdUgfd2lBcDkFmq0g',2,NULL,'es'),
(1099,131222,'Pedro ','Montagut','amontagut','e10adc3949ba59abbe56e057f20f883e','A','+57','3203954516',2,'NO','nouser.png',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,NULL,'es'),
(1100,1020767165,'Carolina','Díaz Parra ','cdiaz','3a0412a4571e6667004beca027133d61','A','+57','3006951514',2,'NO','nouser.png',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,'1991-09-29','es'),
(1101,1014229343,'Joel','Parra','mparra','4c64f6d9d52553ea3905325a02919f96','A','+57','3102385665',2,'NO','1531840256991-603.jpg',1,'2018-07-17 10:09:04','2018-07-17 10:09:04','2018-09-13 20:54:01','2018-09-13 20:49:53','f728e10857609215','HUAWEI CUN-L03','Android','5.1','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6Im1wYXJyYSIsImV4cCI6MTU2Mjk0NDE0NH0.N62ZuwD7dejEvWZGIQhA6xlVqtxZihzphxMU7uX_kV0',4,'1992-01-09','es'),
(1102,7186278,'Cesar','Dueñas','cduenas','31479f183d9e37278fa3691d9048597f','A','+57','3213069281',2,'NO','nouser.png',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'1984-04-14','es'),
(1103,1020763089,'Sergio','Cruz','scruz','6b042fbd51ca819f46f08a95b5236e32','A','+57','3213278460',2,'NO','nouser.png',1,'2018-08-22 11:35:02','2018-08-22 11:35:02','2018-08-22 11:35:11','2018-09-13 13:46:57','310dc519b8d1866','SM-J320M','Android','5.1.1','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6InNjcnV6IiwiZXhwIjoxNTY2MDU5NzAyfQ.yXmz7hOveOQy9hoJJv9Q4Xagr88lpxUYbylK_GzzGtA',1,'1991-05-01','es'),
(1104,1001111,'Greg','Forrest','gforrest','e10adc3949ba59abbe56e057f20f883e','A','+1','2145430598',2,'NO','1536618423365-1032.jpg',1,'2018-09-10 10:21:18','2018-09-10 17:30:30','2018-09-10 17:31:28','2018-09-14 10:36:43','4ce163379715f442','BLA-L09','Android','8.0.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6Imdmb3JyZXN0IiwiZXhwIjoxNTY3NzIyNjMwfQ.4dFwBoOTZQEdzv-ugNYq_5kGlTTkIVtX5Yua1HU4b7Y',0,NULL,'en'),
(1105,101212,'Ron','West','rwest','e10adc3949ba59abbe56e057f20f883e','A','+1','2145072000',2,'NO','1536618495387-317.jpg',1,'2018-09-10 17:27:47','2018-09-12 10:28:49','2018-09-10 17:28:37','2018-09-13 10:54:48','4FEE23A6-89EF-4104-BE2D-50B21A67B0AF','iPhone8,1','iOS','11.4.1','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6InJ3ZXN0IiwiZXhwIjoxNTY3ODcwMTI5fQ.ufaE3rHi4xwmODpZYvnVxmuAVvP3PaQv-rVLD_4-DYM',0,NULL,'en'),
(1106,101313,'Brian D','Blank','bblank','e10adc3949ba59abbe56e057f20f883e','A','+1','7034748502',2,'NO','1536619190141-585.jpg',1,'2018-09-10 17:38:37','2018-09-12 17:46:12','2018-09-12 17:47:22','2018-09-10 17:42:00','aae536047c55fa46','SM-G955U','Android','8.0.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImJibGFuayIsImV4cCI6MTU2Nzg5NjM3Mn0.zTC2TIN3bhiTgxH0GNYBPItKhBhEl2SjtGSLyutnexs',0,NULL,'en'),
(1107,101414,'Anne','Clarrissimeaux','aclarrissimeaux','e10adc3949ba59abbe56e057f20f883e','A','+1','2145520910',2,'NO','1536619241721-550.jpg',1,'2018-09-10 17:40:25','2018-09-10 18:57:24','2018-09-10 17:40:42','2018-09-13 11:51:23','2CE34D60-197F-4324-9FEB-D6346B3D0AC8','iPhone10,6','iOS','11.4.1','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImFjbGFycmlzc2ltZWF1eCIsImV4cCI6MTU2NzcyNzg0NH0.cS1SThumzIqEiGCE21-4lEeCSmJsCB_lgZdfh',1,NULL,'en'),
(1108,1030563509,'Diana','Albarracín','dalbarracin','5c40642f898f43b1f7ca505026d5b37c','A','+57','3142818194',2,'NO','1536765880193-985.jpg',1,'2018-09-12 10:17:11','2018-09-12 10:17:11','2018-09-12 10:18:48',NULL,'dff05af90ff04803','Moto G (5)','Android','7.0','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6ImRhbGJhcnJhY2luIiwiZXhwIjoxNTY3ODY5NDMxfQ.qvSHXkn7gU8Muu35PuKB8qQpyZtI2AJJy3TXBDVHHy',2,'1989-11-03','es'),
(1109,1128406202,'Maria Isabel','Pérez Agudelo','mperez','aa4cdde4333ba5bc828544512f5e2c4f','A','+57','3006981501',2,'NO','nouser.png',1,'2018-09-12 11:01:24','2018-09-12 11:01:24','2018-09-12 11:01:48','2018-09-13 18:33:09','623454E8-03EE-42AD-88A6-254F6A10F5C2','iPhone8,2','iOS','11.4.1','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3JJRCI6Im1wZXJleiIsImV4cCI6MTU2Nzg3MjA4NH0.lZ92FfRFJz5zrt7qVD8dyDJ3Mgb52bGej_LtmCTDUrc',0,'1987-04-18','en');

/*Table structure for table `USERCOMPANY` */

DROP TABLE IF EXISTS `USERCOMPANY`;

CREATE TABLE `USERCOMPANY` (
  `EMPRESA_EmpresaId` int(11) NOT NULL,
  `USER_UserId` int(11) NOT NULL,
  `POSITION_PositionId` int(11) NOT NULL,
  `USERCOMPANYdefault` int(1) DEFAULT '0',
  `USER_Mail` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`EMPRESA_EmpresaId`,`USER_UserId`),
  KEY `fk_USEREMPRESA_USER1_idx` (`USER_UserId`),
  KEY `fk_USEREMPRESA_POSITION1_idx` (`POSITION_PositionId`),
  KEY `USER_Mail` (`USER_Mail`),
  CONSTRAINT `fk_USEREMPRESA_EMPRESA` FOREIGN KEY (`EMPRESA_EmpresaId`) REFERENCES `COMPANY` (`CompanyId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_USEREMPRESA_POSITION1` FOREIGN KEY (`POSITION_PositionId`) REFERENCES `POSITION` (`PositionId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_USEREMPRESA_USER1` FOREIGN KEY (`USER_UserId`) REFERENCES `USER` (`UserId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `USERCOMPANY` */

insert  into `USERCOMPANY`(`EMPRESA_EmpresaId`,`USER_UserId`,`POSITION_PositionId`,`USERCOMPANYdefault`,`USER_Mail`) values 
(1,47,41,1,'fburgos@inmov.com'),
(1,83,87,0,'jbotero@inmov.com'),
(1,377,27,1,'mmiranda@inmov.com'),
(1,555,59,1,'agutierrez@inmov.com'),
(1,1000,17,1,'tesoreria@inmov.com'),
(1,1001,24,1,'faguillon@inmov.com'),
(1,1002,40,1,'aaponte@inmov.com'),
(1,1003,1,1,'darias@inmov.com'),
(1,1004,7,1,'yballen@inmov.com'),
(1,1005,52,1,'felipe1671@hotmail.com'),
(1,1006,43,1,'abecerra@inmov.com'),
(1,1007,29,1,'lbuitrago@inmov.com'),
(1,1008,21,1,'ctemporal@inmov.com'),
(1,1010,38,1,'scamargo@inmov.com'),
(1,1011,56,1,'cproduccion@inmov.com'),
(1,1012,32,1,'jcarvajal@inmov.com'),
(1,1013,14,1,'paoa9202@hotmail.com'),
(1,1014,10,1,'shirleymcc22@gmail.com'),
(1,1016,18,1,'facturacion@inmov.com'),
(1,1017,33,1,'pdaza@inmov.com'),
(1,1018,58,1,'zdelgado@inmov.com'),
(1,1019,35,1,'adiaz@inmov.com'),
(1,1020,14,1,'rodriguez.fernandez06@gmail.com'),
(1,1021,35,1,'dgarcia@inmov.com'),
(1,1022,23,1,'sgarzon@inmov.com'),
(1,1023,51,1,'agualtero@inmov.com'),
(1,1024,34,1,'lgutierrez@inmov.com'),
(1,1026,31,1,'jherrera@inmov.com'),
(1,1027,42,1,'liregui@inmov.com'),
(1,1028,46,1,'djauregui@inmov.com'),
(1,1029,72,1,'coordinacionetb@century-ax.com'),
(1,1030,52,1,'maikanro2903@gmail.com'),
(1,1031,56,1,'okappler@inmov.com'),
(1,1032,7,1,'analistanominatht@inmov.com'),
(1,1033,44,1,'jlondono@inmov.com'),
(1,1034,39,1,'cmarino@inmov.com'),
(1,1035,45,1,'a.martinez@century-ax.com'),
(1,1036,7,1,'pmendez@inmov.com'),
(1,1038,56,1,'pmurcia@inmov.com'),
(1,1039,56,1,'dnaranjo@inmov.com'),
(1,1040,11,1,'sistemas@inmov.com'),
(1,1041,66,1,'jorduz@inmov.com'),
(1,1042,12,1,'omairapaez_@hotmail.com'),
(1,1043,16,1,'legalizaciones@inmov.com'),
(1,1044,6,1,'apatino@inmov.com'),
(1,1045,30,1,'jpena@inmov.com'),
(1,1046,4,1,'afiliaciones@inmov.com'),
(1,1048,20,1,'cpreciado@mejiabtl.com'),
(1,1049,48,1,'bodega@inmov.com'),
(1,1050,51,1,'dracines@inmov.com'),
(1,1051,22,1,'jramirez@inmov.com'),
(1,1052,2,1,'acontable@inmov.com'),
(1,1053,22,1,'cramirez@mejia.net.co'),
(1,1055,50,1,'jremolina@inmov.com'),
(1,1056,3,1,'crico@inmov.com'),
(1,1057,32,1,'crincon@inmov.com'),
(1,1058,22,1,'irodriguez@inmov.com'),
(1,1059,25,1,'orodriguez@inmov.com'),
(1,1060,65,1,'cromero@inmov.com'),
(1,1061,9,1,'msantana@inmov.com'),
(1,1062,36,1,'lserrano@inmov.com'),
(1,1063,26,1,'csigua@inmov.com'),
(1,1064,5,1,'proveedores@inmov.com'),
(1,1065,54,1,'jtorres@inmov.com'),
(1,1066,49,1,'ouseche@inmov.com'),
(1,1067,55,1,'hvargas@inmov.com'),
(1,1068,8,1,'cvargas@inmov.com'),
(1,1069,15,1,'tesoreria@inmov.com'),
(1,1072,63,1,'acarvajal-ct@inmov.com'),
(1,1073,16,1,'legalizaciones@inmov.com'),
(1,1074,64,0,'amejia@mejiaasociados.com'),
(1,1075,47,1,'lsoto@inmov.com'),
(1,1076,67,1,'jperez@mejiabtl.com'),
(1,1077,22,1,'lnino@inmov.com'),
(1,1078,23,1,'lsanchez@inmov.com'),
(1,1079,68,1,'karregoces@inmov.com'),
(1,1080,69,1,'slopez@inmov.com'),
(1,1081,56,1,'amendez@inmov.com'),
(1,1082,70,1,'hrico@inmov.com'),
(1,1084,74,1,'dcadavid@inmov.com'),
(1,1085,23,1,'jalba@inmov.com'),
(1,1086,1,1,'pnud@inmov.com'),
(1,1087,55,1,'amorales@inmov.com'),
(1,1088,71,1,'acortes@inmov.com'),
(1,1089,73,1,'atorres-ct@inmov.com'),
(1,1091,76,1,'koviedo@inmov.com'),
(1,1092,77,1,'pmazzino@mejia.com.co'),
(1,1093,78,0,'luislleras@links-solutions.com'),
(1,1095,29,1,'dmartinez@inmov.com'),
(1,1100,60,1,'recepcion@inmov.com'),
(1,1101,53,1,'operadorsap@inmov.com'),
(1,1102,68,1,'cduenas@inmov.com'),
(1,1103,83,1,'scruz@inmov.com'),
(1,1108,90,1,'dalbarracin@inmov.com'),
(1,1109,35,1,'mperez@inmov.com'),
(2,47,41,0,'flower.burgos@mejiaasociados.com'),
(2,1074,64,1,'amejia@mejiaasociados.com'),
(4,1015,13,1,'comercial@inmov.com'),
(4,1054,28,1,'ereatiga@inmov.com'),
(5,377,27,0,'mmiranda@mejia.com.co'),
(5,1070,62,1,'oarciniegas@inmov.com'),
(6,1071,36,1,'lpenagos@inmov.com'),
(7,377,27,0,'mmiranda@bancodebogota.com.co'),
(8,1090,15,1,'cbernal-ct@inmov.com'),
(9,83,87,1,'jbotero@inmov.com'),
(9,1094,86,1,'rw@inmov.com'),
(9,1104,85,1,'gforrest@inmov.com'),
(9,1105,86,1,'rwest@inmov.com'),
(9,1106,88,1,'bblank@inmov.com'),
(9,1107,89,1,'aclarrissimeaux@inmov.com'),
(10,1096,80,1,'Beatriz.zapata@claro.com.co'),
(10,1097,81,1,'andres.montoya@claro.com.co'),
(10,1099,82,1,'Pedro.montagut@claro.com.co'),
(11,1062,84,0,'lserrano@axmarketing.com.co');

/*Table structure for table `communityComments` */

DROP TABLE IF EXISTS `communityComments`;

CREATE TABLE `communityComments` (
  `idCommunityComments` int(11) NOT NULL AUTO_INCREMENT,
  `idCommunity` int(11) NOT NULL,
  `isUser` varchar(80) COLLATE utf8mb4_bin NOT NULL,
  `communityComments` varchar(150) COLLATE utf8mb4_bin DEFAULT NULL,
  `communityCommentsTimeStamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `communityCommentsStatus` enum('e','d') COLLATE utf8mb4_bin DEFAULT 'e',
  PRIMARY KEY (`idCommunityComments`),
  KEY `isUser` (`isUser`),
  KEY `idCommunity` (`idCommunity`)
) ENGINE=MyISAM AUTO_INCREMENT=227 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `communityComments` */

insert  into `communityComments`(`idCommunityComments`,`idCommunity`,`isUser`,`communityComments`,`communityCommentsTimeStamp`,`communityCommentsStatus`) values 
(100,142,'jorduz','Feliz cumpleaños ','2017-12-01 13:14:31','e'),
(99,142,'lserrano','Feliz cumple Serge!','2017-12-01 10:31:09','e'),
(98,142,'oarciniegas','Sergio! Feliz cumpleaños Dios te bendiga! ','2017-12-01 10:24:12','e'),
(101,142,'fburgos','Feliz cumpleaños','2017-12-01 16:03:32','e'),
(102,144,'mmiranda','Felicidades ? ','2017-12-04 10:25:07','e'),
(103,144,'lserrano','Carito feliz cumpleaños! Pásala felíz!!','2017-12-04 10:35:59','e'),
(104,144,'agutierrez','??? Feliz cumple caro ???','2017-12-04 10:41:35','e'),
(105,144,'okappler','Feliz cumpleaños caro!!! ???','2017-12-04 11:13:50','e'),
(106,144,'jorduz','Feliz Cumpleaños ','2017-12-04 14:00:21','e'),
(107,158,'jlondono','Tenía que plublicarla !','2017-12-07 17:16:27','e'),
(108,158,'mmiranda','Jajaja','2017-12-07 19:07:45','e'),
(109,159,'mmiranda','Super foto ? ','2017-12-08 11:40:04','e'),
(110,160,'okappler','Feliz cumpleaños naranjas','2017-12-11 10:21:20','e'),
(111,160,'lserrano','Feliz cumpleaños don naranjas!!','2017-12-11 10:21:25','e'),
(112,160,'mmiranda ','?Felicidades','2017-12-11 10:27:33','e'),
(113,161,'okappler','Y las gorras y camisetas donde se reclaman? ','2017-12-11 13:44:47','e'),
(114,161,'lserrano','Nidea! Imagino que con Dallan.. las dieron al final','2017-12-11 13:45:43','e'),
(115,154,'agutierrez','Re aspera','2017-12-15 11:11:56','e'),
(116,182,'csigua','Vamoooo Kappler','2017-12-18 11:53:18','e'),
(117,186,'mmiranda ','Si existe, es él!!! ?','2017-12-20 07:38:22','e'),
(118,190,'lserrano','Nelsiton querido! Feliz cumpleaños. Que te celbren mucho  en tu quilla!','2017-12-21 10:22:37','e'),
(119,190,'okappler','Viejo Nelson feliz cumpleaños ','2017-12-21 14:40:32','e'),
(120,190,'mmiranda','Feliz cumpleaños carita ','2017-12-21 14:58:30','e'),
(121,190,'fburgos','Feliz cumpleaños','2017-12-21 22:42:00','e'),
(122,192,'lserrano','Feliz cumpleaños querido Ka!','2017-12-22 10:21:50','e'),
(123,192,'lgutierrez','Kapplersin felíz cumpleaños!!! Que nuestro padre celestial te bendiga en abundancia!!! ','2017-12-22 10:23:18','e'),
(124,192,'mmiranda ','Viejo feliz cumpleaños, que siga así de buena gente y noble. ','2017-12-22 10:26:20','e'),
(125,192,'jorduz','Kappler Feliz Cumpleaños ??','2017-12-22 11:00:17','e'),
(126,192,'agutierrez','Viejo kappler que la vida lo siga llenando de éxitos y nunca cambie ','2017-12-22 11:13:05','e'),
(127,192,'fburgos','Feliz cumpleaños ','2017-12-22 20:53:18','e'),
(128,192,'okappler','Gracias a todos!!! ','2017-12-24 12:18:53','e'),
(129,197,'jorduz','Gina feliz cumpleaños ','2017-12-26 10:52:47','e'),
(131,197,'agutierrez','Gina feliz cumple que la pases super','2017-12-27 12:57:43','e'),
(132,197,'gmendez','Gracias :)','2017-12-27 12:58:24','e'),
(133,197,'agutierrez','Éxitos ','2017-12-27 12:58:42','e'),
(135,197,'bpinilla','Feliz cumpleaños Gina ?','2017-12-29 14:41:30','e'),
(136,202,'fburgos','Feliz cumpleaños','2018-01-02 10:23:06','e'),
(137,202,'jtorres','Felicitaciones!!!','2018-01-02 22:37:41','e'),
(138,207,'djauregui','Mi querido Ingeniero um feliz feliz cumpleanos cuantos cumples, 33? Besos y abrazos que te consientan Dally','2018-01-21 10:22:07','e'),
(139,207,'jtorres','Buena Flow','2018-01-21 11:41:47','e'),
(140,207,'mmiranda','Feliz cumpleaños Blogger  ? ','2018-01-21 13:21:12','e'),
(141,209,'lserrano','Gracias a todos! Que bonito detalle','2018-01-23 19:09:51','e'),
(142,210,'mmiranda','Muchos éxitos!!! ','2018-01-26 15:32:06','e'),
(143,211,'lserrano','Feliz cumpleaños Cami!','2018-01-27 10:35:49','e'),
(144,214,'jtorres','Se ven felices, se nota que a pesar de lo duro, disfrutamos lo que hacemos \n\nMuchas gracias equipo?','2018-02-01 07:10:54','e'),
(145,216,'jtorres','Impecables !','2018-02-02 11:29:08','e'),
(146,218,'mmiranda','Que bkno ','2018-02-02 20:20:26','e'),
(147,221,'djauregui','Congratulations.','2018-02-03 13:06:25','e'),
(148,223,'mmiranda','Feliz cumpleaños ? ','2018-02-04 10:24:05','e'),
(149,223,'agutierrez','Gracias pana ','2018-02-04 10:24:47','e'),
(150,224,'agutierrez','Juli Feliz Cumple que la pases super','2018-02-04 10:25:34','e'),
(151,223,'jorduz ','Feliz cumpleaños ','2018-02-04 11:51:47','e'),
(152,224,'jorduz ','Juli feliz cumpleaños ','2018-02-04 11:52:05','e'),
(153,227,'mmiranda','Fotaza!!! ','2018-02-05 19:31:10','e'),
(154,208,'fburgos','Gracias','2018-02-09 09:33:38','e'),
(155,208,'agutierrez','Super ','2018-02-09 09:34:57','e'),
(156,217,'mmiranda','Bkno','2018-02-09 09:38:49','e'),
(157,208,'fburgos','Súper','2018-02-09 09:57:50','e'),
(158,223,'acarvajal','Feliz cumpleaños','2018-02-09 10:04:30','e'),
(159,208,'acarvajal','Los quiero... ','2018-02-09 10:05:30','e'),
(160,229,'agutierrez','Feliz cumple pato que la pases super bien ','2018-02-17 13:08:31','e'),
(161,231,'agutierrez','Feliz cumpleaños señorita ','2018-02-22 10:23:14','e'),
(162,231,'jorduz','Luisa te deseo un muy feliz cumpleaños ','2018-02-22 10:48:47','e'),
(163,229,'mmiranda','Felicidades ahora q si tienes la App','2018-02-27 10:34:01','e'),
(164,229,'pdaza','Gracias Aric ?','2018-02-27 10:34:06','e'),
(165,242,'jtorres','Bravo Dallan y Sergio!!!\nGran trabajo\nAl equipo una felicitación también !!','2018-03-13 13:46:02','e'),
(166,241,'oarciniegas','Lo\nMejor para uatedes equipazo!!!!!','2018-03-13 13:46:09','e'),
(167,242,'oarciniegas','Felicitaciones!!!! ','2018-03-13 13:46:38','e'),
(168,241,'oarciniegas','Gracias Flo, pude ver a Juan!! ','2018-03-13 13:47:39','e'),
(169,243,'oarciniegas','Eso así es la cosa!!!','2018-03-14 16:26:29','e'),
(170,243,'csigua','Se ve cool','2018-03-14 16:27:12','e'),
(171,242,'csigua','Que grandes. ','2018-03-14 16:27:31','e'),
(172,249,'ouseche','Gran Equipo ☺','2018-03-14 22:05:36','e'),
(173,246,'zdelgado','Que mas se puede esperar de esa hermosa ciudad ... Lo que es bello es bello','2018-03-15 09:54:35','e'),
(174,252,'djauregui','El senor te llene de bendiciones, pass un Feliz cumpleanos','2018-03-18 10:21:30','e'),
(175,252,'jorduz ','Feliz Cumple Katherine ','2018-03-18 14:41:14','e'),
(176,257,'fburgos','Feliz cumpleaños','2018-03-30 19:33:23','e'),
(177,267,'mmiranda','Feliz cumple ? ','2018-04-20 10:21:04','e'),
(178,278,'jperez','Me alegra mucho que la pasado super felicidades ? ','2018-05-18 20:32:27','e'),
(179,283,'mmiranda','? ? ','2018-05-21 12:34:30','e'),
(180,283,'jlondono','Promociones del ONLY !','2018-05-21 13:01:25','e'),
(181,285,'liregui',' Me encantó! Gran iniciativa \n','2018-05-29 13:15:31','e'),
(182,286,'mmiranda','Felicidades ? ','2018-05-31 10:01:22','e'),
(183,288,'mmiranda','Feliz cumpleaños ? ','2018-05-31 10:01:34','e'),
(184,288,'djauregui','Claus abrazos gigantes feliz cumpleaños ????','2018-05-31 12:19:40','e'),
(185,286,'djauregui','Angie feliz cumpleaños ??? ','2018-05-31 12:20:21','e'),
(186,288,'jbotero','Feliz cmple!!!!','2018-06-01 18:35:32','e'),
(187,294,'djauregui','Señorita un feliz feliz cumpleaños el señor te llene de bendiciones ','2018-06-11 14:12:32','e'),
(188,293,'djauregui','Carlos un feliz cumpleaños abrazos ','2018-06-11 14:13:10','e'),
(189,296,'mmiranda','Felicidades lindo ?','2018-06-14 10:12:27','e'),
(190,295,'cbernal','Estas son las mañanitas que cantaba el Rey David!!!  Así se canta en México ','2018-06-14 17:29:18','e'),
(191,296,'cbernal','Feliz cumple al creativo mayor!! ','2018-06-14 17:29:52','e'),
(192,295,'jremolina','Jajajaja','2018-06-14 17:34:36','e'),
(193,300,'mmiranda','Paseito de 2 días ','2018-06-22 12:14:17','e'),
(194,300,'zdelgado','Paseo Deli!!!','2018-06-22 12:15:28','e'),
(195,300,'oarciniegas','Gracias Zulma y Milton! Esperando respuestas mi gente!','2018-06-22 12:20:31','e'),
(196,300,'itibana','Paseo ?','2018-06-22 16:10:04','e'),
(197,300,'llopez','paseo ?','2018-06-22 16:10:35','e'),
(198,300,'oarciniegas','Uy si deli! Gracias!','2018-06-22 16:11:19','e'),
(199,300,'sramirez','Paseíto....','2018-06-22 16:14:26','e'),
(200,300,'ncuentas','Paseo ???','2018-06-22 16:20:09','e'),
(201,300,'aacuna','Paseooo con actividades extremas ?','2018-06-22 16:42:02','e'),
(202,300,'oarciniegas','Gracias! No pierdan el impulso! Antes de que se vayan a descansar cuentenme que prefieren!','2018-06-22 16:43:45','e'),
(203,300,'djauregui','Obviamente un buen paseo con súper rumba','2018-06-22 16:48:23','e'),
(204,300,'slopez','Me han contado que los paseos son sabrosos... Paseo!!!','2018-06-22 16:55:16','e'),
(205,300,'hvelez','Un buen paseito... De varios dias','2018-06-22 16:55:51','e'),
(206,300,'fburgos','Un paseo\n?️?','2018-06-22 16:58:47','e'),
(207,300,'yballen','Paseo 2 días ','2018-06-22 17:28:29','e'),
(208,300,'oarciniegas','Súper gracias por sus respuestas! Que descansen. Sigo esperando por los demás!','2018-06-22 17:30:40','e'),
(209,300,'abarahona','Paseito y full Rumba ?✌️','2018-06-22 17:38:47','e'),
(210,300,'koviedo','Paseo y rumba ?','2018-06-25 11:53:32','e'),
(211,300,'abecerra','Paseo y rumba con tequila','2018-06-27 19:06:08','e'),
(212,302,'pmazzino','Arriba Colombia!!!!!','2018-06-30 17:01:52','e'),
(213,307,'djauregui','Que tengas un feliz feliz cumpleaños','2018-07-12 19:40:31','e'),
(214,300,'jlondono','Playaaaaaa !!!','2018-07-13 19:43:24','e'),
(215,308,'jtorres','Felicitaciones ','2018-07-21 10:26:48','e'),
(216,304,'jtorres','Saludos panas','2018-07-21 10:27:18','e'),
(217,311,'mmiranda ','H-bday ? ','2018-07-21 11:50:23','e'),
(218,314,'jperez ','Felicitaciones ','2018-08-01 10:10:44','e'),
(219,315,'mmiranda ','Feliz cumpleaños ? ','2018-08-01 17:06:56','e'),
(220,315,'jorduz','Feliz cumpleaños Juan Fernando ','2018-08-01 17:56:14','e'),
(221,317,'jpulido','Felicitaciones en tu días y bendiciones','2018-08-11 14:46:40','e'),
(222,319,'oarciniegas','Felicitaciones!!!!!','2018-08-18 21:42:06','e'),
(223,319,'slopez','Felicidades Milton ','2018-08-19 18:22:11','e'),
(224,320,'jperez ','Muchas felicidades Andres','2018-08-23 11:01:34','e'),
(225,323,'slopez','Feliz cumpleaños Bud Brayan (y)','2018-08-30 11:06:36','e'),
(226,325,'oarciniegas','Si señores sin duda!!!! ','2018-08-31 21:52:35','e');

/*Table structure for table `communityLikes` */

DROP TABLE IF EXISTS `communityLikes`;

CREATE TABLE `communityLikes` (
  `idCommunityLikes` int(11) NOT NULL AUTO_INCREMENT,
  `idCommunityPost` int(11) NOT NULL,
  `communityStatsLikes` int(1) DEFAULT '1',
  `idUser` varchar(80) NOT NULL,
  `timeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idCommunityLikes`),
  UNIQUE KEY `idCommunityPost` (`idCommunityPost`,`idUser`),
  KEY `idUser` (`idUser`)
) ENGINE=MyISAM AUTO_INCREMENT=963 DEFAULT CHARSET=latin1;

/*Data for the table `communityLikes` */

insert  into `communityLikes`(`idCommunityLikes`,`idCommunityPost`,`communityStatsLikes`,`idUser`,`timeStamp`) values 
(306,119,1,'agutierrez','2017-11-28 08:49:31'),
(312,119,1,'okappler','2017-11-29 14:43:44'),
(310,121,1,'agutierrez','2017-11-29 09:22:17'),
(311,119,1,'csigua','2017-11-29 11:02:07'),
(307,119,1,'fburgos','2017-11-28 14:05:18'),
(313,119,1,'jremolina','2017-11-30 10:57:16'),
(314,119,1,'mmiranda','2017-11-30 14:37:17'),
(315,119,1,'oarciniegas','2017-11-30 23:07:20'),
(316,142,1,'mmiranda','2017-12-01 10:20:46'),
(317,142,1,'oarciniegas','2017-12-01 10:23:43'),
(318,142,1,'lserrano','2017-12-01 10:30:58'),
(319,143,1,'oarciniegas','2017-12-01 15:59:59'),
(320,142,1,'fburgos','2017-12-01 16:03:15'),
(321,143,1,'djauregui','2017-12-01 16:44:14'),
(322,143,1,'mmiranda','2017-12-01 18:14:37'),
(323,144,1,'mmiranda','2017-12-04 10:20:45'),
(324,144,1,'jremolina','2017-12-04 10:51:37'),
(325,144,1,'okappler','2017-12-04 11:13:13'),
(326,143,1,'okappler','2017-12-04 11:13:54'),
(327,144,1,'hvargas','2017-12-04 13:04:33'),
(328,145,1,'djauregui','2017-12-05 17:15:59'),
(329,145,1,'fburgos','2017-12-05 17:16:34'),
(330,145,1,'mmiranda','2017-12-05 19:29:17'),
(331,145,1,'opaez','2017-12-05 23:35:39'),
(332,146,1,'fburgos','2017-12-06 06:25:21'),
(333,146,1,'mmiranda','2017-12-06 06:27:06'),
(334,147,1,'lgutierrez','2017-12-06 18:59:23'),
(335,147,1,'cmarino','2017-12-06 19:24:32'),
(336,147,1,'jlondono','2017-12-06 19:25:02'),
(337,147,1,'mmiranda','2017-12-06 20:06:37'),
(338,148,1,'mmiranda','2017-12-06 20:06:43'),
(339,148,1,'jlondono','2017-12-06 20:08:06'),
(340,148,1,'sramirez','2017-12-06 21:03:39'),
(341,149,1,'mmiranda','2017-12-06 21:04:07'),
(342,148,1,'agutierrez','2017-12-06 21:07:18'),
(343,150,1,'sramirez','2017-12-06 21:07:27'),
(344,150,1,'mmiranda','2017-12-06 21:07:40'),
(346,152,1,'fburgos','2017-12-06 21:12:44'),
(347,153,1,'fburgos','2017-12-06 21:14:00'),
(348,150,1,'fburgos','2017-12-06 21:14:10'),
(349,149,1,'fburgos','2017-12-06 21:14:14'),
(350,148,1,'fburgos','2017-12-06 21:14:18'),
(351,147,1,'fburgos','2017-12-06 21:14:24'),
(352,152,1,'agutierrez','2017-12-06 21:14:49'),
(353,152,1,'mmiranda','2017-12-06 21:27:05'),
(354,148,1,'cmarino','2017-12-06 21:37:20'),
(355,155,1,'fburgos','2017-12-06 22:02:51'),
(356,155,1,'mmiranda','2017-12-06 22:02:51'),
(357,156,1,'mmiranda','2017-12-07 13:46:47'),
(358,157,1,'fburgos','2017-12-07 19:05:52'),
(359,158,1,'fburgos','2017-12-07 19:05:57'),
(360,156,1,'fburgos','2017-12-07 19:06:07'),
(361,158,1,'agutierrez','2017-12-07 19:07:00'),
(362,158,1,'jlondono','2017-12-07 19:07:23'),
(363,159,1,'mmiranda','2017-12-08 11:39:39'),
(364,159,1,'sramirez','2017-12-10 08:21:31'),
(365,160,1,'okappler','2017-12-11 10:21:00'),
(366,159,1,'lserrano','2017-12-11 10:22:01'),
(367,160,1,'mmiranda ','2017-12-11 10:22:07'),
(368,159,1,'bpinilla','2017-12-11 13:42:09'),
(369,157,1,'bpinilla','2017-12-11 13:42:23'),
(370,157,1,'gmendez','2017-12-12 19:06:17'),
(371,152,1,'gmendez','2017-12-12 19:06:54'),
(372,161,1,'djauregui','2017-12-13 11:40:04'),
(373,157,1,'mmiranda','2017-12-13 16:14:47'),
(374,161,1,'jtorres','2017-12-13 21:13:12'),
(375,159,1,'jtorres','2017-12-13 21:13:20'),
(376,158,1,'jtorres','2017-12-13 21:13:24'),
(377,157,1,'jtorres','2017-12-13 21:13:28'),
(378,156,1,'jtorres','2017-12-13 21:13:32'),
(379,152,1,'jtorres','2017-12-13 21:13:41'),
(380,148,1,'jtorres','2017-12-13 21:13:48'),
(381,147,1,'jtorres','2017-12-13 21:14:07'),
(382,146,1,'jtorres','2017-12-13 21:14:15'),
(383,154,1,'fburgos','2017-12-13 21:25:05'),
(384,161,1,'lserrano','2017-12-13 21:54:31'),
(387,154,1,'agutierrez','2017-12-15 11:10:38'),
(388,182,1,'mmiranda','2017-12-16 17:56:23'),
(389,182,1,'okappler','2017-12-17 08:32:30'),
(390,182,1,'sramirez','2017-12-17 18:48:42'),
(391,182,1,'csigua','2017-12-18 11:52:59'),
(392,161,1,'csigua','2017-12-18 11:53:05'),
(393,182,1,'fburgos','2017-12-18 13:46:03'),
(394,160,1,'fburgos','2017-12-18 13:46:27'),
(395,182,1,'jorduz','2017-12-19 09:24:18'),
(396,187,1,'mmiranda ','2017-12-20 07:37:24'),
(397,186,1,'mmiranda ','2017-12-20 07:37:37'),
(398,189,1,'mmiranda ','2017-12-20 10:34:42'),
(399,189,1,'jorduz','2017-12-20 11:00:06'),
(400,190,1,'mmiranda','2017-12-21 11:10:54'),
(401,187,1,'jremolina ','2017-12-21 18:37:17'),
(402,186,1,'jremolina ','2017-12-21 18:37:28'),
(403,190,1,'jtorres','2017-12-21 20:53:57'),
(404,190,1,'fburgos','2017-12-21 22:41:28'),
(405,189,1,'fburgos','2017-12-21 22:42:08'),
(406,187,1,'fburgos','2017-12-21 22:42:13'),
(407,186,1,'fburgos','2017-12-21 22:42:17'),
(408,187,1,'okappler','2017-12-22 06:49:16'),
(409,190,1,'okappler','2017-12-22 06:49:24'),
(410,189,1,'okappler','2017-12-22 06:49:27'),
(411,186,1,'okappler','2017-12-22 06:49:52'),
(412,187,1,'gmendez','2017-12-22 10:29:11'),
(413,193,1,'fburgos','2017-12-22 20:52:45'),
(414,192,1,'fburgos','2017-12-22 20:53:03'),
(415,191,1,'fburgos','2017-12-22 20:53:30'),
(416,192,1,'mmiranda','2017-12-22 20:53:53'),
(417,193,1,'jtorres','2017-12-23 11:14:50'),
(418,192,1,'jtorres','2017-12-23 11:14:55'),
(419,191,1,'jtorres','2017-12-23 11:15:02'),
(420,189,1,'jtorres','2017-12-23 11:15:20'),
(421,187,1,'jtorres','2017-12-23 11:15:23'),
(422,186,1,'jtorres','2017-12-23 11:15:32'),
(423,182,1,'jtorres','2017-12-23 11:15:35'),
(430,197,1,'mmiranda ','2017-12-29 11:49:00'),
(425,197,1,'fburgos','2017-12-26 12:46:46'),
(426,197,1,'jtorres','2017-12-26 20:43:58'),
(428,197,1,'gmendez','2017-12-27 12:55:20'),
(429,201,1,'mmiranda ','2017-12-29 11:46:47'),
(431,186,1,'bpinilla','2017-12-29 14:41:52'),
(432,201,1,'fburgos','2018-01-01 14:48:42'),
(433,202,1,'fburgos','2018-01-02 10:22:36'),
(434,202,1,'mmiranda ','2018-01-02 10:23:05'),
(435,193,1,'pmurcia','2018-01-02 10:39:26'),
(436,202,1,'bpinilla','2018-01-02 13:50:48'),
(437,201,1,'jlondono','2018-01-02 14:53:40'),
(438,202,1,'jtorres','2018-01-02 22:37:24'),
(439,201,1,'jtorres','2018-01-02 22:37:54'),
(440,149,1,'llopez','2018-01-04 09:19:42'),
(441,204,1,'gmendez','2018-01-04 10:20:42'),
(442,202,1,'gmendez','2018-01-04 10:20:53'),
(443,204,1,'mmiranda','2018-01-04 10:41:53'),
(444,204,1,'cocampo','2018-01-04 11:02:20'),
(445,204,1,'yjimenez','2018-01-04 21:07:45'),
(446,202,1,'yjimenez','2018-01-04 21:07:49'),
(447,201,1,'yjimenez','2018-01-04 21:07:52'),
(448,205,1,'mmiranda','2018-01-07 12:47:41'),
(449,205,1,'llopez','2018-01-07 13:15:59'),
(450,201,1,'gmendez','2018-01-11 07:17:19'),
(451,205,1,'fburgos','2018-01-12 09:59:15'),
(452,204,1,'fburgos','2018-01-12 09:59:26'),
(453,205,1,'jorduz ','2018-01-14 14:33:30'),
(454,206,1,'mmiranda','2018-01-17 10:21:30'),
(455,206,1,'darias','2018-01-17 10:27:51'),
(456,207,1,'cocampo','2018-01-21 10:21:03'),
(457,207,1,'mmiranda','2018-01-21 10:22:27'),
(458,202,1,'djauregui','2018-01-21 10:29:53'),
(459,207,1,'djauregui','2018-01-21 10:30:02'),
(460,205,1,'djauregui','2018-01-21 10:30:11'),
(461,207,1,'jtorres','2018-01-21 11:41:31'),
(462,207,1,'llopez','2018-01-21 16:13:38'),
(463,208,1,'mmiranda','2018-01-22 13:33:10'),
(464,208,1,'agutierrez','2018-01-22 13:33:54'),
(465,208,1,'amejia','2018-01-22 13:46:42'),
(466,207,1,'bpinilla','2018-01-22 17:33:51'),
(467,208,1,'bpinilla','2018-01-22 17:33:57'),
(468,209,1,'bpinilla','2018-01-22 17:38:47'),
(469,209,1,'mmiranda','2018-01-22 17:39:18'),
(470,209,1,'lserrano','2018-01-23 19:09:35'),
(471,208,1,'lserrano','2018-01-23 19:10:05'),
(472,209,1,'fburgos','2018-01-24 09:15:08'),
(473,208,1,'fburgos','2018-01-24 09:15:11'),
(474,207,1,'fburgos','2018-01-24 09:15:55'),
(475,210,1,'mmiranda','2018-01-26 15:31:49'),
(476,210,1,'bpinilla','2018-01-26 19:46:57'),
(477,211,1,'mmiranda','2018-01-27 10:22:53'),
(478,211,1,'darias','2018-01-27 10:24:57'),
(479,210,1,'hvargas','2018-01-27 10:34:19'),
(480,211,1,'agutierrez','2018-01-27 10:37:08'),
(481,213,1,'agutierrez','2018-01-28 10:36:28'),
(482,212,1,'agutierrez','2018-01-28 10:36:31'),
(483,213,1,'mmiranda','2018-01-28 13:17:31'),
(484,212,1,'mmiranda','2018-01-28 13:17:35'),
(485,212,1,'bpinilla','2018-01-28 17:36:50'),
(486,214,1,'fburgos','2018-01-31 11:44:52'),
(487,214,1,'mmiranda','2018-01-31 11:45:19'),
(488,214,1,'jtorres','2018-02-01 07:09:33'),
(489,214,1,'jremolina ','2018-02-01 07:10:10'),
(490,214,1,'cromero','2018-02-01 08:31:32'),
(491,215,1,'djauregui','2018-02-01 22:51:17'),
(492,215,1,'fburgos','2018-02-01 22:51:40'),
(493,215,1,'jorduz ','2018-02-02 06:39:42'),
(494,216,1,'jtorres','2018-02-02 11:28:00'),
(495,215,1,'jtorres','2018-02-02 11:28:04'),
(496,215,1,'jlondono','2018-02-02 11:34:37'),
(497,214,1,'jlondono','2018-02-02 11:34:40'),
(498,215,1,'bpinilla','2018-02-02 14:40:19'),
(499,216,1,'bpinilla','2018-02-02 14:41:00'),
(500,216,1,'agutierrez','2018-02-02 16:22:30'),
(501,217,1,'mmiranda','2018-02-02 17:19:43'),
(502,216,1,'mmiranda','2018-02-02 17:19:55'),
(503,220,1,'mmiranda','2018-02-02 20:20:31'),
(504,216,1,'djauregui','2018-02-03 06:28:47'),
(505,217,1,'djauregui','2018-02-03 06:28:53'),
(506,218,1,'djauregui','2018-02-03 06:28:57'),
(507,219,1,'djauregui','2018-02-03 06:29:02'),
(508,220,1,'agutierrez','2018-02-03 08:38:45'),
(509,219,1,'agutierrez','2018-02-03 08:38:49'),
(510,220,1,'fburgos','2018-02-03 11:45:56'),
(511,219,1,'fburgos','2018-02-03 11:46:05'),
(512,218,1,'fburgos','2018-02-03 11:46:10'),
(513,217,1,'fburgos','2018-02-03 11:46:15'),
(514,216,1,'fburgos','2018-02-03 11:46:21'),
(515,210,1,'fburgos','2018-02-03 11:46:45'),
(516,221,1,'mmiranda','2018-02-03 12:57:45'),
(517,221,1,'jlondono','2018-02-03 12:58:50'),
(518,221,1,'jorduz ','2018-02-03 13:03:08'),
(519,221,1,'djauregui','2018-02-03 13:05:46'),
(520,222,1,'fburgos','2018-02-03 13:09:14'),
(521,222,1,'ncuentas','2018-02-03 13:17:50'),
(522,222,1,'mmiranda','2018-02-03 13:21:36'),
(523,222,1,'jremolina ','2018-02-03 13:37:37'),
(524,221,1,'jremolina ','2018-02-03 13:37:43'),
(525,222,1,'abecerra','2018-02-03 13:58:20'),
(526,221,1,'abecerra','2018-02-03 13:58:25'),
(527,220,1,'abecerra','2018-02-03 13:58:29'),
(528,219,1,'abecerra','2018-02-03 13:58:33'),
(529,218,1,'abecerra','2018-02-03 13:58:36'),
(530,217,1,'abecerra','2018-02-03 13:58:40'),
(531,216,1,'abecerra','2018-02-03 13:58:45'),
(532,215,1,'abecerra','2018-02-03 13:58:49'),
(533,214,1,'abecerra','2018-02-03 13:58:53'),
(534,220,1,'jorduz ','2018-02-03 15:19:06'),
(535,222,1,'bpinilla','2018-02-04 09:31:59'),
(536,220,1,'bpinilla','2018-02-04 09:32:08'),
(537,218,1,'bpinilla','2018-02-04 09:32:33'),
(538,220,1,'gmendez','2018-02-04 09:48:18'),
(539,217,1,'gmendez','2018-02-04 09:48:38'),
(540,214,1,'gmendez','2018-02-04 09:49:01'),
(541,224,1,'mmiranda','2018-02-04 10:23:46'),
(542,223,1,'mmiranda','2018-02-04 10:24:07'),
(543,221,1,'agutierrez','2018-02-04 10:25:53'),
(544,221,1,'jtorres','2018-02-04 15:01:02'),
(545,222,1,'jtorres','2018-02-04 18:43:52'),
(546,224,1,'gmendez','2018-02-05 13:49:51'),
(547,223,1,'gmendez','2018-02-05 13:49:54'),
(548,227,1,'mmiranda','2018-02-05 19:31:19'),
(549,226,1,'mmiranda','2018-02-05 19:31:24'),
(550,228,1,'mmiranda','2018-02-08 10:24:54'),
(551,228,1,'cmarino','2018-02-08 13:41:58'),
(552,228,1,'jplata','2018-02-08 13:51:18'),
(553,227,1,'agutierrez','2018-02-08 15:14:43'),
(554,225,1,'agutierrez','2018-02-08 15:15:05'),
(555,226,1,'agutierrez','2018-02-08 15:15:10'),
(556,228,1,'agutierrez','2018-02-08 15:15:18'),
(557,222,1,'agutierrez','2018-02-09 09:36:19'),
(558,208,1,'acarvajal','2018-02-09 10:05:36'),
(559,228,1,'bpinilla','2018-02-16 23:56:09'),
(560,229,1,'mmiranda','2018-02-17 15:37:10'),
(561,230,1,'mmiranda','2018-02-18 14:23:46'),
(562,217,1,'oarciniegas','2018-02-21 17:32:14'),
(563,214,1,'oarciniegas','2018-02-21 17:32:25'),
(564,231,1,'mmiranda','2018-02-22 10:26:10'),
(565,233,1,'fburgos','2018-02-22 13:15:02'),
(566,233,1,'mmiranda','2018-02-22 13:15:31'),
(567,231,1,'lsoto','2018-02-22 13:15:38'),
(568,232,1,'mmiranda','2018-02-22 13:15:40'),
(569,233,1,'jorduz','2018-02-22 13:38:54'),
(570,232,1,'jorduz','2018-02-22 13:39:14'),
(571,233,1,'jtorres','2018-02-22 13:39:38'),
(572,232,1,'jtorres','2018-02-22 13:39:47'),
(573,226,1,'jtorres','2018-02-22 13:40:31'),
(574,233,1,'slopez','2018-02-22 13:53:18'),
(575,232,1,'slopez','2018-02-22 13:53:34'),
(576,232,1,'fburgos','2018-02-22 17:29:42'),
(577,234,1,'fburgos','2018-02-22 23:52:33'),
(578,234,1,'mmiranda','2018-02-23 00:29:47'),
(579,234,1,'pmurcia','2018-02-23 15:35:25'),
(580,234,1,'cromero','2018-02-23 17:15:06'),
(581,233,1,'cromero','2018-02-23 17:15:15'),
(584,237,1,'bpinilla','2018-03-02 19:56:52'),
(583,234,1,'bpinilla','2018-03-02 11:08:53'),
(585,236,1,'bpinilla','2018-03-02 19:57:01'),
(586,237,1,'agutierrez','2018-03-02 20:01:56'),
(587,236,1,'jtorres','2018-03-02 20:49:13'),
(588,234,1,'jtorres','2018-03-02 20:49:23'),
(589,237,1,'slopez','2018-03-03 12:12:21'),
(590,236,1,'slopez','2018-03-03 12:12:36'),
(591,237,1,'mmiranda','2018-03-04 14:20:13'),
(592,234,1,'slopez','2018-03-05 20:13:50'),
(593,238,1,'hvargas','2018-03-08 12:43:12'),
(594,238,1,'mmiranda','2018-03-08 14:32:47'),
(595,239,1,'jlondono','2018-03-09 16:17:07'),
(596,237,1,'oarciniegas','2018-03-11 08:57:30'),
(597,241,1,'mmiranda ','2018-03-13 12:37:38'),
(598,240,1,'mmiranda ','2018-03-13 12:37:43'),
(599,241,1,'jtorres','2018-03-13 12:38:19'),
(600,240,1,'jtorres','2018-03-13 12:39:32'),
(601,241,1,'djauregui','2018-03-13 13:44:35'),
(602,240,1,'djauregui','2018-03-13 13:44:40'),
(603,242,1,'jtorres','2018-03-13 13:45:25'),
(604,241,1,'oarciniegas','2018-03-13 13:45:45'),
(605,242,1,'mmiranda ','2018-03-13 13:46:04'),
(606,242,1,'slopez','2018-03-13 13:46:10'),
(607,240,1,'oarciniegas','2018-03-13 13:47:48'),
(608,242,1,'jlondono','2018-03-13 13:49:56'),
(609,242,1,'jorduz ','2018-03-13 14:27:28'),
(610,242,1,'djauregui','2018-03-13 15:39:35'),
(611,242,1,'cromero','2018-03-13 17:14:58'),
(612,241,1,'liregui','2018-03-13 18:06:09'),
(613,243,1,'oarciniegas','2018-03-14 16:26:15'),
(614,243,1,'csigua','2018-03-14 16:26:59'),
(615,242,1,'csigua','2018-03-14 16:27:20'),
(616,241,1,'csigua','2018-03-14 16:27:35'),
(617,244,1,'amejia','2018-03-14 16:30:27'),
(618,243,1,'amejia','2018-03-14 16:30:33'),
(619,242,1,'amejia','2018-03-14 16:30:43'),
(620,244,1,'mmiranda ','2018-03-14 16:30:48'),
(621,243,1,'mmiranda ','2018-03-14 16:30:52'),
(622,243,1,'jorduz ','2018-03-14 16:40:01'),
(623,244,1,'jorduz ','2018-03-14 16:40:10'),
(624,244,1,'djauregui','2018-03-14 16:41:12'),
(625,243,1,'djauregui','2018-03-14 16:41:27'),
(626,244,1,'jbotero','2018-03-14 16:50:33'),
(627,243,1,'jbotero','2018-03-14 16:50:54'),
(628,242,1,'jbotero','2018-03-14 16:51:01'),
(629,241,1,'jbotero','2018-03-14 16:51:18'),
(630,234,1,'jbotero','2018-03-14 16:51:51'),
(631,244,1,'liregui','2018-03-14 16:52:45'),
(632,244,1,'fburgos','2018-03-14 16:52:52'),
(633,243,1,'fburgos','2018-03-14 16:52:59'),
(634,243,1,'liregui','2018-03-14 16:53:00'),
(635,245,1,'liregui','2018-03-14 16:54:38'),
(636,244,1,'pmurcia','2018-03-14 17:06:29'),
(637,245,1,'pmurcia','2018-03-14 17:06:33'),
(638,243,1,'cromero','2018-03-14 17:14:59'),
(639,245,1,'csigua','2018-03-14 17:21:23'),
(640,245,1,'jtorres','2018-03-14 18:08:21'),
(641,244,1,'jtorres','2018-03-14 18:08:29'),
(642,244,1,'cromero','2018-03-14 18:09:17'),
(643,242,1,'liregui','2018-03-14 18:09:21'),
(644,246,1,'cromero','2018-03-14 18:09:37'),
(645,245,1,'djauregui','2018-03-14 18:10:56'),
(646,246,1,'csigua','2018-03-14 18:23:03'),
(647,245,1,'jorduz ','2018-03-14 18:59:40'),
(648,249,1,'mmiranda ','2018-03-14 19:25:19'),
(649,248,1,'mmiranda ','2018-03-14 19:26:46'),
(650,249,1,'jremolina','2018-03-14 19:35:26'),
(651,245,1,'jremolina','2018-03-14 19:35:41'),
(652,249,1,'liregui','2018-03-14 19:37:39'),
(653,246,1,'liregui','2018-03-14 19:38:40'),
(654,249,1,'slopez','2018-03-14 20:48:25'),
(655,248,1,'slopez','2018-03-14 20:48:39'),
(656,249,1,'csigua','2018-03-14 20:51:37'),
(657,249,1,'djauregui','2018-03-14 21:12:59'),
(658,249,1,'hvargas','2018-03-14 21:50:32'),
(659,249,1,'ouseche','2018-03-14 22:01:13'),
(660,248,1,'ouseche','2018-03-14 22:02:03'),
(661,245,1,'ouseche','2018-03-14 22:05:02'),
(662,246,1,'ouseche','2018-03-14 22:05:06'),
(663,244,1,'ouseche','2018-03-14 22:05:16'),
(664,249,1,'adiaz','2018-03-15 08:08:30'),
(665,249,1,'zdelgado','2018-03-15 09:53:32'),
(666,249,1,'jtorres','2018-03-15 09:53:48'),
(667,246,1,'zdelgado','2018-03-15 09:53:57'),
(668,243,1,'zdelgado','2018-03-15 09:55:29'),
(669,242,1,'zdelgado','2018-03-15 09:55:34'),
(670,251,1,'slopez','2018-03-15 18:09:32'),
(671,250,1,'slopez','2018-03-15 18:55:31'),
(672,251,1,'mmiranda ','2018-03-16 10:35:14'),
(673,241,1,'fburgos','2018-03-16 12:41:15'),
(674,251,1,'jorduz ','2018-03-16 20:50:46'),
(675,246,1,'bpinilla','2018-03-17 08:34:22'),
(676,242,1,'bpinilla','2018-03-17 08:34:41'),
(677,249,1,'bpinilla','2018-03-17 08:35:25'),
(678,252,1,'djauregui','2018-03-18 10:21:52'),
(679,252,1,'mmiranda ','2018-03-18 10:39:11'),
(680,251,1,'jtorres','2018-03-19 11:50:59'),
(681,250,1,'jtorres','2018-03-19 11:51:07'),
(682,252,1,'jtorres','2018-03-19 11:51:35'),
(683,255,1,'jtorres','2018-03-22 15:33:35'),
(684,253,1,'jtorres','2018-03-22 15:34:18'),
(685,254,1,'jtorres','2018-03-22 15:34:29'),
(686,255,1,'jlondono','2018-03-22 17:36:50'),
(687,253,1,'slopez','2018-03-22 18:15:38'),
(688,255,1,'slopez','2018-03-22 18:48:36'),
(689,254,1,'slopez','2018-03-22 20:51:23'),
(690,255,1,'mmiranda ','2018-03-23 06:32:35'),
(691,254,1,'mmiranda ','2018-03-23 11:10:55'),
(692,253,1,'mmiranda ','2018-03-23 11:11:03'),
(693,256,1,'mmiranda ','2018-03-28 15:02:56'),
(694,256,1,'llopez','2018-03-28 20:38:25'),
(695,256,1,'slopez','2018-03-29 11:46:28'),
(696,253,1,'djauregui','2018-03-29 23:38:44'),
(697,257,1,'fburgos','2018-03-30 19:33:11'),
(698,255,1,'fburgos','2018-03-30 19:33:31'),
(699,254,1,'fburgos','2018-03-30 19:33:37'),
(700,253,1,'fburgos','2018-03-30 19:33:44'),
(701,251,1,'fburgos','2018-03-30 19:33:54'),
(702,259,1,'mmiranda','2018-04-04 17:06:36'),
(703,259,1,'slopez','2018-04-05 05:52:43'),
(704,260,1,'mmiranda','2018-04-06 10:22:42'),
(705,261,1,'slopez','2018-04-06 14:53:46'),
(706,260,1,'slopez','2018-04-06 14:53:51'),
(707,261,1,'mmiranda','2018-04-08 15:45:49'),
(708,259,1,'djauregui','2018-04-10 18:58:31'),
(709,262,1,'mmiranda','2018-04-11 10:21:54'),
(710,262,1,'bpinilla','2018-04-12 07:41:28'),
(711,259,1,'bpinilla','2018-04-12 07:42:15'),
(712,258,1,'bpinilla','2018-04-12 07:42:27'),
(713,254,1,'bpinilla','2018-04-12 07:45:23'),
(714,252,1,'bpinilla','2018-04-12 07:46:17'),
(715,262,1,'slopez','2018-04-12 13:59:38'),
(716,263,1,'djauregui','2018-04-13 21:24:48'),
(717,262,1,'djauregui','2018-04-13 21:24:57'),
(718,263,1,'slopez','2018-04-14 09:21:59'),
(719,263,1,'mmiranda','2018-04-14 10:59:26'),
(720,265,1,'mmiranda','2018-04-15 14:23:13'),
(721,265,1,'djauregui','2018-04-17 09:52:44'),
(722,267,1,'mmiranda','2018-04-20 10:21:08'),
(723,266,1,'slopez','2018-04-20 16:21:00'),
(724,267,1,'slopez','2018-04-21 12:00:32'),
(725,265,1,'slopez','2018-04-21 12:00:37'),
(726,264,1,'slopez','2018-04-21 12:00:40'),
(727,267,1,'lpenagos','2018-04-21 13:38:26'),
(728,267,1,'djauregui','2018-04-22 06:18:41'),
(729,268,1,'slopez','2018-04-22 17:39:17'),
(730,268,1,'mmiranda','2018-04-22 17:39:53'),
(731,268,1,'djauregui','2018-04-22 18:13:01'),
(732,265,1,'yjimenez','2018-04-24 13:46:19'),
(733,270,1,'hvargas','2018-05-07 10:20:27'),
(734,270,1,'mmiranda','2018-05-07 10:20:44'),
(735,270,1,'llopez','2018-05-07 11:29:42'),
(736,270,1,'pmurcia','2018-05-07 19:52:43'),
(737,270,1,'slopez','2018-05-09 11:00:21'),
(738,272,1,'slopez','2018-05-09 14:42:41'),
(739,272,1,'mmiranda','2018-05-09 15:06:32'),
(740,272,1,'djauregui','2018-05-09 20:08:42'),
(741,272,1,'fburgos','2018-05-11 16:01:38'),
(742,270,1,'fburgos','2018-05-11 16:01:43'),
(743,261,1,'fburgos','2018-05-11 16:02:06'),
(744,274,1,'djauregui','2018-05-13 14:53:22'),
(745,274,1,'jorduz ','2018-05-13 16:55:16'),
(746,274,1,'mmiranda','2018-05-15 09:31:04'),
(747,275,1,'slopez','2018-05-15 14:13:37'),
(748,276,1,'slopez','2018-05-15 19:24:52'),
(749,276,1,'koviedo','2018-05-16 15:27:10'),
(750,277,1,'bpinilla','2018-05-18 20:09:59'),
(751,277,1,'jorduz','2018-05-18 20:10:03'),
(752,277,1,'yballen','2018-05-18 20:10:06'),
(753,276,1,'bpinilla','2018-05-18 20:10:07'),
(754,276,1,'jorduz','2018-05-18 20:10:10'),
(755,275,1,'bpinilla','2018-05-18 20:10:15'),
(756,274,1,'bpinilla','2018-05-18 20:10:20'),
(757,277,1,'djauregui','2018-05-18 20:10:54'),
(758,277,1,'koviedo','2018-05-18 20:11:35'),
(759,276,1,'djauregui','2018-05-18 20:11:37'),
(760,275,1,'djauregui','2018-05-18 20:11:40'),
(761,277,1,'llopez','2018-05-18 20:12:23'),
(762,278,1,'jperez','2018-05-18 20:31:24'),
(763,278,1,'djauregui','2018-05-18 20:48:39'),
(764,279,1,'zdelgado','2018-05-19 07:37:46'),
(765,279,1,'slopez','2018-05-19 09:26:07'),
(766,279,1,'koviedo','2018-05-19 10:42:01'),
(767,278,1,'koviedo','2018-05-19 10:42:26'),
(768,279,1,'mmiranda','2018-05-19 13:46:29'),
(769,280,1,'llopez','2018-05-19 15:43:22'),
(770,280,1,'zdelgado','2018-05-19 15:48:53'),
(771,280,1,'djauregui','2018-05-19 16:59:48'),
(772,279,1,'djauregui','2018-05-19 17:00:00'),
(773,280,1,'pmurcia','2018-05-19 18:27:35'),
(774,280,1,'slopez','2018-05-19 21:02:25'),
(775,279,1,'bpinilla','2018-05-20 18:07:12'),
(776,278,1,'bpinilla','2018-05-20 18:07:22'),
(777,280,1,'bpinilla','2018-05-20 18:08:12'),
(778,282,1,'mmiranda','2018-05-21 08:30:22'),
(779,281,1,'mmiranda','2018-05-21 08:30:26'),
(780,282,1,'slopez','2018-05-21 11:10:07'),
(781,281,1,'slopez','2018-05-21 11:10:10'),
(782,283,1,'djauregui','2018-05-21 12:33:24'),
(783,282,1,'djauregui','2018-05-21 12:33:32'),
(784,283,1,'hvargas','2018-05-21 12:33:37'),
(785,281,1,'djauregui','2018-05-21 12:33:40'),
(786,283,1,'koviedo','2018-05-21 12:33:44'),
(787,283,1,'mmiranda','2018-05-21 12:34:46'),
(788,283,1,'jorduz','2018-05-21 12:34:57'),
(789,283,1,'slopez','2018-05-21 12:35:37'),
(790,283,1,'zdelgado','2018-05-21 12:48:12'),
(791,282,1,'csigua','2018-05-21 15:38:21'),
(792,283,1,'csigua','2018-05-21 15:39:10'),
(793,281,1,'csigua','2018-05-21 15:39:16'),
(794,283,1,'adiaz','2018-05-22 10:56:19'),
(795,281,1,'koviedo','2018-05-23 19:38:26'),
(796,280,1,'koviedo','2018-05-23 19:38:39'),
(797,281,1,'abarahona','2018-05-24 16:53:35'),
(798,274,1,'fburgos','2018-05-25 09:39:03'),
(799,284,1,'slopez','2018-05-25 15:08:27'),
(800,284,1,'djauregui','2018-05-26 06:42:06'),
(801,284,1,'mmiranda','2018-05-26 11:35:20'),
(802,285,1,'djauregui','2018-05-29 12:08:55'),
(803,285,1,'liregui','2018-05-29 13:14:48'),
(804,285,1,'mmiranda','2018-05-29 14:39:45'),
(805,285,1,'pmurcia','2018-05-29 21:57:59'),
(806,285,1,'jremolina','2018-05-30 19:25:28'),
(807,285,1,'dgarcia','2018-05-31 07:30:04'),
(808,288,1,'cbernal','2018-06-01 18:54:28'),
(809,284,1,'koviedo','2018-06-05 17:42:42'),
(810,291,1,'fburgos','2018-06-07 10:40:00'),
(811,285,1,'fburgos','2018-06-07 10:40:11'),
(812,284,1,'fburgos','2018-06-07 10:40:15'),
(813,282,1,'fburgos','2018-06-07 10:40:31'),
(814,281,1,'fburgos','2018-06-07 10:40:36'),
(815,280,1,'fburgos','2018-06-07 10:40:43'),
(816,291,1,'mmiranda','2018-06-07 10:44:28'),
(817,291,1,'slopez','2018-06-07 11:17:44'),
(818,291,1,'jperez','2018-06-07 11:52:19'),
(819,289,1,'jpena','2018-06-08 15:35:44'),
(820,285,1,'jpena','2018-06-08 15:35:59'),
(821,292,1,'slopez','2018-06-10 11:08:15'),
(822,292,1,'mmiranda','2018-06-10 16:09:34'),
(823,294,1,'djauregui','2018-06-11 14:12:39'),
(824,292,1,'djauregui','2018-06-11 14:12:50'),
(825,294,1,'llopez','2018-06-11 16:39:37'),
(826,294,1,'mmiranda','2018-06-12 14:39:58'),
(827,295,1,'mmiranda','2018-06-14 10:12:32'),
(828,295,1,'jremolina','2018-06-14 10:15:20'),
(829,296,1,'jremolina','2018-06-14 10:15:23'),
(830,296,1,'cbernal','2018-06-19 10:46:37'),
(831,295,1,'cbernal','2018-06-19 10:46:43'),
(832,298,1,'mmiranda','2018-06-20 12:56:03'),
(833,299,1,'mmiranda','2018-06-20 12:56:06'),
(834,298,1,'slopez','2018-06-20 17:20:30'),
(835,299,1,'jorduz','2018-06-20 18:40:52'),
(836,298,1,'oarciniegas','2018-06-22 11:52:36'),
(837,285,1,'aacuna','2018-06-22 16:43:03'),
(838,300,1,'slopez','2018-06-22 16:57:29'),
(839,300,1,'koviedo','2018-06-25 11:53:40'),
(840,301,1,'djauregui','2018-06-27 11:20:09'),
(841,301,1,'abecerra','2018-06-27 12:46:56'),
(842,301,1,'slopez','2018-06-27 12:47:35'),
(843,301,1,'abarahona','2018-06-27 19:15:09'),
(844,301,1,'lpenagos','2018-06-28 06:08:03'),
(845,302,1,'jorduz','2018-06-28 08:56:30'),
(846,301,1,'mmiranda','2018-06-28 18:41:15'),
(847,302,1,'mmiranda','2018-06-28 18:41:33'),
(848,303,1,'djauregui','2018-06-30 09:08:11'),
(849,302,1,'pmazzino','2018-06-30 17:01:38'),
(850,304,1,'lpenagos','2018-07-01 12:11:56'),
(851,303,1,'bpinilla','2018-07-01 16:42:41'),
(852,302,1,'bpinilla','2018-07-01 16:42:50'),
(853,304,1,'oarciniegas','2018-07-01 17:12:30'),
(854,304,1,'mmiranda','2018-07-01 22:09:59'),
(855,305,1,'bpinilla','2018-07-06 13:54:39'),
(856,305,1,'mmiranda','2018-07-06 15:30:02'),
(857,305,1,'slopez','2018-07-06 19:24:20'),
(858,283,1,'oarciniegas','2018-07-10 12:48:44'),
(859,282,1,'oarciniegas','2018-07-10 12:48:51'),
(860,281,1,'oarciniegas','2018-07-10 12:48:56'),
(861,280,1,'oarciniegas','2018-07-10 12:49:00'),
(862,278,1,'oarciniegas','2018-07-10 12:49:08'),
(863,277,1,'oarciniegas','2018-07-10 12:49:12'),
(864,276,1,'oarciniegas','2018-07-10 12:49:16'),
(865,274,1,'oarciniegas','2018-07-10 12:49:24'),
(866,272,1,'oarciniegas','2018-07-10 12:49:33'),
(867,267,1,'oarciniegas','2018-07-10 12:49:42'),
(868,305,1,'koviedo','2018-07-10 19:10:49'),
(869,306,1,'jpena','2018-07-11 07:38:49'),
(870,306,1,'slopez','2018-07-11 10:26:09'),
(871,306,1,'mmiranda ','2018-07-11 13:02:02'),
(872,307,1,'fburgos','2018-07-12 10:00:39'),
(873,307,1,'mmiranda ','2018-07-12 10:00:50'),
(874,306,1,'lpenagos','2018-07-12 16:10:26'),
(875,307,1,'slopez','2018-07-12 19:15:49'),
(876,307,1,'djauregui','2018-07-12 19:39:59'),
(877,306,1,'djauregui','2018-07-12 19:40:42'),
(878,305,1,'djauregui','2018-07-12 19:40:47'),
(879,304,1,'djauregui','2018-07-12 19:40:56'),
(880,306,1,'pmurcia','2018-07-13 19:44:36'),
(881,306,1,'koviedo','2018-07-13 19:46:07'),
(882,306,1,'bpinilla','2018-07-14 09:37:36'),
(883,308,1,'bpinilla','2018-07-15 12:42:41'),
(884,308,1,'jlondono','2018-07-15 12:43:09'),
(885,308,1,'slopez','2018-07-15 13:12:12'),
(886,308,1,'djauregui','2018-07-15 13:33:32'),
(887,308,1,'ncuentas','2018-07-15 13:39:23'),
(888,308,1,'pmazzino','2018-07-15 19:49:55'),
(889,308,1,'mmiranda ','2018-07-16 08:07:10'),
(890,308,1,'abecerra','2018-07-18 09:32:14'),
(891,309,1,'mmiranda ','2018-07-19 20:51:35'),
(892,311,1,'fburgos','2018-07-21 10:26:02'),
(893,310,1,'fburgos','2018-07-21 10:26:07'),
(894,305,1,'jtorres','2018-07-21 10:27:04'),
(895,302,1,'jtorres','2018-07-21 10:27:31'),
(896,301,1,'jtorres','2018-07-21 10:27:36'),
(897,310,1,'mmiranda ','2018-07-22 11:47:07'),
(898,309,1,'jpulido','2018-07-24 10:00:52'),
(899,310,1,'jpulido','2018-07-24 10:00:57'),
(900,313,1,'mmiranda ','2018-07-24 19:50:50'),
(901,314,1,'mmiranda ','2018-07-24 19:50:53'),
(902,314,1,'fburgos','2018-07-26 16:54:51'),
(903,313,1,'fburgos','2018-07-26 16:54:57'),
(904,312,1,'fburgos','2018-07-26 16:55:09'),
(905,308,1,'fburgos','2018-07-26 16:55:22'),
(906,314,1,'djauregui','2018-07-27 06:24:18'),
(907,314,1,'jperez ','2018-08-01 10:10:26'),
(908,315,1,'mmiranda ','2018-08-01 17:06:46'),
(909,315,1,'djauregui','2018-08-03 06:03:37'),
(910,316,1,'mmiranda ','2018-08-10 10:00:39'),
(911,316,1,'djauregui','2018-08-10 18:09:55'),
(912,317,1,'jlondono','2018-08-11 10:00:33'),
(913,317,1,'mmiranda ','2018-08-11 10:07:13'),
(914,317,1,'zdelgado','2018-08-11 15:19:31'),
(915,317,1,'bpinilla','2018-08-12 22:22:41'),
(916,318,1,'llopez','2018-08-18 17:46:17'),
(917,318,1,'jorduz','2018-08-18 17:46:40'),
(918,318,1,'jtorres','2018-08-18 17:48:23'),
(919,318,1,'fburgos','2018-08-18 17:50:47'),
(920,318,1,'jperez ','2018-08-18 17:59:25'),
(921,318,1,'abecerra','2018-08-18 17:59:30'),
(922,319,1,'abecerra','2018-08-18 17:59:34'),
(923,318,1,'jremolina','2018-08-18 18:03:20'),
(924,319,1,'jtorres','2018-08-18 18:04:43'),
(925,319,1,'zdelgado','2018-08-18 18:28:39'),
(926,319,1,'oarciniegas','2018-08-18 21:41:34'),
(927,319,1,'slopez','2018-08-19 18:21:38'),
(928,318,1,'slopez','2018-08-19 18:21:54'),
(929,319,1,'jbotero','2018-08-20 11:45:57'),
(930,318,1,'jbotero','2018-08-20 11:46:11'),
(931,319,1,'fburgos','2018-08-21 17:08:22'),
(932,306,1,'oarciniegas','2018-08-21 21:58:46'),
(933,320,1,'jperez ','2018-08-23 11:01:09'),
(934,320,1,'mmiranda ','2018-08-23 11:45:35'),
(935,320,1,'amejia','2018-08-24 17:00:35'),
(936,321,1,'djauregui','2018-08-29 07:39:33'),
(937,319,1,'djauregui','2018-08-29 07:39:46'),
(938,318,1,'djauregui','2018-08-29 07:39:58'),
(939,321,1,'mmiranda ','2018-08-29 11:48:04'),
(940,321,1,'slopez','2018-08-29 22:13:35'),
(941,322,1,'slopez','2018-08-29 22:13:39'),
(942,318,1,'lserrano','2018-08-30 10:01:14'),
(943,323,1,'slopez','2018-08-30 11:06:04'),
(944,323,1,'mmiranda ','2018-08-30 14:46:50'),
(945,325,1,'zdelgado','2018-08-31 21:50:22'),
(946,325,1,'oarciniegas','2018-08-31 21:51:07'),
(947,325,1,'jlondono','2018-08-31 21:51:47'),
(948,325,1,'lgutierrez','2018-08-31 21:52:20'),
(949,325,1,'amorales','2018-08-31 21:58:31'),
(950,325,1,'abecerra','2018-08-31 22:04:39'),
(951,325,1,'jorduz','2018-08-31 22:24:43'),
(952,325,1,'mmiranda ','2018-09-01 11:50:34'),
(953,326,1,'fburgos','2018-09-03 15:20:31'),
(954,326,1,'slopez','2018-09-04 06:47:26'),
(955,327,1,'slopez','2018-09-04 21:10:49'),
(956,327,1,'mmiranda ','2018-09-05 12:41:31'),
(957,327,1,'jtorres','2018-09-10 15:46:22'),
(958,327,1,'aclarrissimeaux','2018-09-10 18:59:33'),
(959,327,1,'fburgos','2018-09-10 19:00:01'),
(960,327,1,'lserrano','2018-09-13 10:44:11'),
(961,327,1,'amorales','2018-09-13 13:35:00'),
(962,329,1,'mperez','2018-09-13 18:33:17');

/*Table structure for table `communityPosts` */

DROP TABLE IF EXISTS `communityPosts`;

CREATE TABLE `communityPosts` (
  `idCommunity` int(11) NOT NULL AUTO_INCREMENT,
  `communityPost` text CHARACTER SET utf8mb4,
  `communityPic` varchar(40) CHARACTER SET latin1 DEFAULT NULL,
  `communityDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idUser` varchar(80) COLLATE utf8mb4_bin NOT NULL,
  `communityPostsStatus` enum('e','d') CHARACTER SET latin1 DEFAULT 'e',
  `type` enum('post','bday','link') COLLATE utf8mb4_bin DEFAULT 'post',
  `hits` int(3) DEFAULT '0',
  PRIMARY KEY (`idCommunity`),
  KEY `idUser` (`idUser`)
) ENGINE=MyISAM AUTO_INCREMENT=330 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `communityPosts` */

insert  into `communityPosts`(`idCommunity`,`communityPost`,`communityPic`,`communityDate`,`idUser`,`communityPostsStatus`,`type`,`hits`) values 
(119,'☀? Diciembre 6️⃣ y 7⃣. Pronto mas información sobre puntos de encuentro y recomendaciones para el viaje ','1511876794199-632.jpg','2017-11-28 08:48:46','djauregui','e','post',0),
(143,'<a href=\"https://inmov.typeform.com/to/H7w8y8\" class=\"button button-big button-fill external\">Escoger Lugar</a>','puntosFinDeAno.jpg?v2','2017-12-01 15:28:22','djauregui','e','link',0),
(142,NULL,'Birthday-Cake.jpg','2017-12-01 10:20:01','sgarzon','e','bday',13),
(144,NULL,'Birthday-Cake.jpg','2017-12-04 10:20:01','dgarcia','e','bday',40),
(145,'Recomendaciones generales para mañana.','1512512039610-177-v1.jpg','2017-12-05 17:14:36','djauregui','e','post',0),
(146,'De fiesta con inmov ','1512559495531-1051.jpg','2017-12-06 06:25:13','fburgos','e','post',0),
(147,'Despidiendo el año, Camilo y sus niñas. ??','1512604590735-475.jpg','2017-12-06 18:57:14','jlondono','e','post',1),
(148,'Bolos !!','1512606427637-439.jpg','2017-12-06 19:27:23','jlondono','e','post',0),
(149,'Disfrutando con inmov....','1512612193013-509.jpg','2017-12-06 21:03:33','sramirez','e','post',0),
(150,'Palabras del presidente ???','1512612392090-235.jpg','2017-12-06 21:07:11','agutierrez','e','post',0),
(152,'Almuerzo inmov...','1512612687394-1021.jpg','2017-12-06 21:11:39','sramirez','e','post',0),
(153,'Fiesta inmov','1512612814973-393.jpg','2017-12-06 21:13:55','fburgos','e','post',0),
(154,'? Fiesta Inmov ','1512615366032-465.jpg','2017-12-06 21:56:30','mmiranda','e','post',2),
(155,'Nos fuimos !','1512615733636-607.jpg','2017-12-06 22:02:42','jtorres','e','post',0),
(156,'Momentos de integración de la familia INMOV','1512617990656-998.jpg','2017-12-06 22:40:13','agutierrez','e','post',1),
(157,'Innovación y calidad INMOV','1512676148135-1086.jpg','2017-12-07 14:49:28','agutierrez','e','post',1),
(158,'Juan con las chicas !!','1512684911580-926.jpg','2017-12-07 17:15:41','jlondono','e','post',13),
(159,'INMOV SAS','1512699163352-976.jpg','2017-12-07 21:14:31','lgutierrez','e','post',15),
(160,NULL,'Birthday-Cake.jpg','2017-12-11 10:20:02','dnaranjo','e','bday',10),
(161,'Equipazo CAX','1513005792726-240.jpg','2017-12-11 10:23:54','lserrano','e','post',17),
(182,'Listos en Medellín para la caravana Coca cola','1513455198891-547.jpg','2017-12-16 15:13:42','okappler','e','post',8),
(186,'Caravana Coca Cola Barranquilla','1513768189102-1040.jpg','2017-12-20 06:10:22','lgutierrez','e','post',6),
(187,'Equipo de Barranquilla  - Caravana Coca Cola','1513768324481-475.jpg','2017-12-20 06:12:52','lgutierrez','e','post',3),
(189,'Navidades INMOV','1513783517053-549.jpg','2017-12-20 10:32:43','agutierrez','e','post',0),
(190,NULL,'Birthday-Cake.jpg','2017-12-21 10:20:01','ncuentas','e','bday',20),
(191,'Fiesta despedida ETB CAX','1513946391562-1045.jpg','2017-12-22 07:41:08','lserrano','e','post',0),
(192,NULL,'Birthday-Cake.jpg','2017-12-22 10:20:01','okappler','e','bday',27),
(193,'Novenas a cargo de producción','1513956134660-653.jpg','2017-12-22 10:22:46','agutierrez','e','post',5),
(197,NULL,'Birthday-Cake.jpg','2017-12-26 10:20:01','gmendez','e','bday',27),
(201,'Don Carlos ganador obsequio de parte RRHH novena inmov  2017','1514565873000-210.jpg','2017-12-29 11:46:06','bpinilla','e','post',5),
(202,NULL,'Birthday-Cake.jpg','2018-01-02 10:20:01','cromero','e','bday',23),
(204,NULL,'Birthday-Cake.jpg','2018-01-04 10:20:01','hvelez','e','bday',0),
(205,'\r\n<a href=\"https://m.facebook.com/story.php?story_fbid=10155958695621963&id=753331962\" class=\"button button-big button-fill external\">Video Paintball Inmov</a>','1515282763219-358.jpg','2018-01-06 20:29:33','mmiranda ','e','link',2),
(206,NULL,'Birthday-Cake.jpg','2018-01-17 10:20:01','jorduz','e','bday',2),
(207,NULL,'Birthday-Cake.jpg','2018-01-21 10:20:01','fburgos','e','bday',11),
(208,'Cumpleaños de Flojer','1516645940992-322.jpg','2018-01-22 13:32:49','acarvajal','e','post',9),
(209,'Baby shower de Laura Serrano ¡Felicidades!','1516660516598-319.jpg','2018-01-22 17:36:41','bpinilla','e','post',3),
(210,'Agradecido con esta gran familia, hay una gran calidad humana en sus empleados y en los que aprendimos con el tiempo a valorar los equpos, y los grandes talentos!','1516998570369-1041.jpg','2018-01-26 15:30:34','okappler','e','post',6),
(211,NULL,'Birthday-Cake.jpg','2018-01-27 10:20:01','mvargas','e','bday',8),
(212,NULL,'Birthday-Cake.jpg','2018-01-28 10:20:01','darias','e','bday',0),
(213,NULL,'Birthday-Cake.jpg','2018-01-28 10:20:01','crico','e','bday',0),
(214,'Convención Coca-Cola','1517417060858-548.jpg','2018-01-31 11:44:42','fburgos','e','post',8),
(215,'Los crack de inmov','1517543382142-614.jpg','2018-02-01 22:50:15','fburgos','e','post',4),
(216,'Avianca 2020','1517579700887-627.jpg','2018-02-02 08:55:34','jlondono','e','post',6),
(217,'Con Nairo en Avianca','1517606118066-270.jpg','2018-02-02 16:15:46','agutierrez','e','post',1),
(218,'Antonio Sanin en Avianca','1517616969835-256.jpg','2018-02-02 19:17:32','agutierrez','e','post',8),
(219,'Sanint Avianca 2020','1517617910697-913.jpg','2018-02-02 19:32:36','jlondono','e','post',0),
(220,'Manuel Medrano Avianca 2020','1517619633839-1010.jpg','2018-02-02 20:00:54','jlondono','e','post',0),
(221,'Cerrando el encuentro directivo de Avianca, un éxito!! Gracias equipo. Estamos felices','1517680549373-358.jpg','2018-02-03 12:56:29','jtorres','e','post',6),
(222,'Avianca, otro reto al 100%','1517681303218-1064.jpg','2018-02-03 13:09:02','fburgos','e','post',3),
(223,NULL,'Birthday-Cake.jpg','2018-02-04 10:20:01','agutierrez','e','bday',12),
(224,NULL,'Birthday-Cake.jpg','2018-02-04 10:20:01','jplata','e','bday',9),
(225,'Feliz cumpleaños Aric','1517858494850-694.jpg','2018-02-05 14:22:41','jlondono','e','post',0),
(226,'Feliz cumpleaños años Juliana','1517858586432-238.jpg','2018-02-05 14:23:39','jlondono','e','post',0),
(227,'Feliz cumpleaños ?','1517858776512-666.jpg','2018-02-05 14:26:45','gmendez','e','post',6),
(228,NULL,'Birthday-Cake.jpg','2018-02-08 10:20:01','lgutierrez','e','bday',5),
(229,NULL,'Birthday-Cake.jpg','2018-02-17 10:20:01','pdaza','e','bday',15),
(230,NULL,'Birthday-Cake.jpg','2018-02-18 10:20:02','yballen','e','bday',0),
(231,NULL,'Birthday-Cake.jpg','2018-02-22 10:20:01','lsoto','e','bday',12),
(232,'Con nuestro cliente y equipo iniciando Market Conference 2018','1519317627298-174.jpg','2018-02-22 11:40:40','jtorres','e','post',0),
(233,'Citi Markets Conference 2018, comenzamos!','1519323214380-770.jpg','2018-02-22 13:14:44','fburgos','e','post',2),
(234,'La sacamos del estadio!','1519359854810-465.jpg','2018-02-22 23:24:37','jtorres','e','post',1),
(236,'Celebración del día de la mujer','1520010470191-282.jpg','2018-03-02 12:08:49','agutierrez','e','post',1),
(237,'Soldado caído ?? que comelonaa !!','1520013713482-739.jpg','2018-03-02 13:02:59','bpinilla','e','post',2),
(238,NULL,'Birthday-Cake.jpg','2018-03-08 10:20:01','dracines','e','bday',1),
(239,NULL,'Birthday-Cake.jpg','2018-03-09 10:20:01','dbuitrago','e','bday',0),
(240,'Política de Sostenibilidad','1520873022692-812.jpg','2018-03-12 11:44:11','slopez','e','post',0),
(241,'Syngenta','1520962569858-470.jpg','2018-03-13 12:36:33','fburgos','e','post',12),
(242,'Celebremos es logro de todos.','1520966595208-672.jpg','2018-03-13 13:44:21','djauregui','e','post',22),
(243,'Creer es poder!,  Inmov para Terpel. 50 años \nConvención Nacional de Ventas \nBmanga!','1521062644315-1001.jpg','2018-03-14 16:25:38','jtorres','e','post',12),
(244,'Cerrando con éxito el tercer día de convención Syngenta en Cancún.','1521062897405-165.jpg','2018-03-14 16:30:07','csigua','e','post',0),
(245,'Livent en vivo!','1521064425596-881.jpg','2018-03-14 16:54:04','liregui','e','post',0),
(246,'Un atardecer espectacular en Bucaramanga','1521068944962-1015.jpg','2018-03-14 18:09:26','jtorres','e','post',2),
(248,'Difuminado es el logo de Terpel rotado','1521068976186-918.jpg','2018-03-14 18:14:55','jtorres','e','post',1),
(249,'<iframe width=\"100%\" src=\"https://www.youtube.com/embed/UdRS9Wt5vgs?rel=0&amp;showinfo=0\" frameborder=\"0\" allowfullscreen></iframe>\r\n<br>\r\nEjecución impecable en Cancún',NULL,'2018-03-14 19:10:58','csigua','e','post',14),
(250,'Inicio mundial Syngenta 2018','1521151049024-968.jpg','2018-03-15 16:57:53','liregui','e','post',0),
(251,'campeones Mundial.','1521151110843-451.jpg','2018-03-15 16:59:13','liregui','e','post',1),
(252,NULL,'Birthday-Cake.jpg','2018-03-18 10:20:01','dlagos','e','bday',12),
(253,'Día Mundial del Agua','1521744359308-1063.jpg','2018-03-22 13:46:24','slopez','e','post',2),
(254,'Montaje en festival estéreo picnic','1521746450343-246.jpg','2018-03-22 14:21:50','pmurcia','e','post',1),
(255,'Montaje interno FEP','1521746563876-714.jpg','2018-03-22 14:23:28','pmurcia','e','post',1),
(256,NULL,'Birthday-Cake.jpg','2018-03-28 10:20:01','mmartinez','e','bday',1),
(257,NULL,'Birthday-Cake.jpg','2018-03-30 10:20:01','abecerra','e','bday',6),
(258,NULL,'Birthday-Cake.jpg','2018-03-31 10:20:01','karregoces','e','bday',0),
(259,'Les cuento que se entrego el material reciclado en el mes de Marzo a una empresa recolectora autorizada, obteniendo el siguiente resultado.','1522879530771-374.jpg','2018-04-04 17:05:59','slopez','e','post',0),
(260,NULL,'Birthday-Cake.jpg','2018-04-06 10:20:01','jnino','e','bday',0),
(261,'Cumpleaños Adriana Becerra!!!','1523038090264-968.jpg','2018-04-06 13:09:26','jremolina','e','post',2),
(262,NULL,'Birthday-Cake.jpg','2018-04-11 10:20:02','yjimenez','e','bday',0),
(263,'¡Anímate!','1523671682229-885.jpg','2018-04-13 21:08:51','slopez','e','post',0),
(264,NULL,'Birthday-Cake.jpg','2018-04-14 10:20:01','hvargas','e','bday',0),
(265,NULL,'Birthday-Cake.jpg','2018-04-15 10:20:02','lbuitrago','e','bday',2),
(266,NULL,'Birthday-Cake.jpg','2018-04-20 10:20:01','cmarino','e','bday',0),
(267,NULL,'Birthday-Cake.jpg','2018-04-20 10:20:01','lpenagos','e','bday',7),
(268,'Día mundial de la tierra!?','1524436676567-552.jpg','2018-04-22 17:39:06','slopez','e','post',0),
(269,NULL,'Birthday-Cake.jpg','2018-04-30 10:20:01','aacuna','e','bday',1),
(270,NULL,'Birthday-Cake.jpg','2018-05-07 10:20:01','pmurcia','e','bday',1),
(271,'<iframe width=\"100%\" src=\"https://www.youtube.com/embed/wNIVxmTgD7c?rel=0&amp;showinfo=0\" frameborder=\"0\" allowfullscreen></iframe>\r\n<br>\r\nInvertir en Talento Humano es garantizar un mejor ambiente de trabajo, preocuparnos por cada persona como individuo y generar un esquema de profundidad. <b>¡Bienvenidos!</b>  Y que soplen nuevos vientos en Talento Humano.',NULL,'2018-05-09 11:22:21','oarciniegas','d','post',0),
(272,'Vamos por muy buen camino!','1525894816313-677.jpg','2018-05-09 14:40:55','slopez','e','post',0),
(273,NULL,'Birthday-Cake.jpg','2018-05-11 10:20:02','ereatiga','e','bday',0),
(274,'Felíz Día Madres!','1526241079578-948.jpg','2018-05-13 14:52:22','slopez','e','post',0),
(275,NULL,'Birthday-Cake.jpg','2018-05-15 10:20:01','djauregui','e','bday',0),
(276,'Celebrando el cumpleaños de Dally. Con mucho cariño...','1526411644602-770.jpg','2018-05-15 14:15:39','slopez','e','post',2),
(277,'Cumpleaños  de Dally','1526691868455-132.jpg','2018-05-18 20:07:12','slopez','e','post',1),
(278,'Fiesta Dallan','1526692528138-228.jpg','2018-05-18 20:15:52','fburgos','e','post',14),
(279,'Damos inicio a nuestro Mundialito Inmov','1526731850689-402.jpg','2018-05-19 07:11:26','slopez','e','post',0),
(280,'Subcampeonas','1526762392285-1062.jpg','2018-05-19 15:40:04','djauregui','e','post',1),
(281,'Tercer Lugar! Fuerza Cardenal...','1526867316714-103.jpg','2018-05-20 20:50:27','slopez','e','post',0),
(282,'Creatinmov!','1526867567142-353.jpg','2018-05-20 20:53:15','slopez','e','post',1),
(283,'Nuevo chat de Inmov , QUE NOS PONEMOS HOY!!','1526923911667-398.jpg','2018-05-21 12:32:45','jtorres','e','post',31),
(284,'Jornada deportiva!','1527275044025-1001.jpg','2018-05-25 14:04:28','slopez','e','post',0),
(285,'¿Te gustó el refrigerio de hoy?','1527611369796-287.jpg','2018-05-29 11:30:46','slopez','e','post',11),
(286,NULL,'Birthday-Cake.jpg','2018-05-31 10:00:01','adiaz','e','bday',11),
(287,NULL,'Birthday-Cake.jpg','2018-05-31 10:00:01','lsanchez','e','bday',0),
(288,NULL,'Birthday-Cake.jpg','2018-05-31 10:00:01','cbernal','e','bday',11),
(289,NULL,'Birthday-Cake.jpg','2018-06-03 10:00:02','crincon','e','bday',0),
(290,NULL,'Birthday-Cake.jpg','2018-06-04 10:00:01','apatino','e','bday',1),
(291,'Livent, capacitando a los lideres de votación','1528385945625-397.jpg','2018-06-07 10:39:53','fburgos','e','post',1),
(292,'Final liga aguila chocquibtown #masunidosquenunca','1528633374639-174.jpg','2018-06-10 07:25:06','pmurcia','e','post',0),
(293,NULL,'Birthday-Cake.jpg','2018-06-11 10:00:01','cpreciado','e','bday',5),
(294,NULL,'Birthday-Cake.jpg','2018-06-11 10:00:01','llopez','e','bday',9),
(295,NULL,'Birthday-Cake.jpg','2018-06-14 10:00:01','jremolina','e','bday',9),
(296,NULL,'Birthday-Cake.jpg','2018-06-14 10:00:01','csigua','e','bday',8),
(297,NULL,'Birthday-Cake.jpg','2018-06-17 10:00:01','msantana','e','bday',0),
(298,'Colombia fest en cc fontanar realizado por Inmov.','1529457079498-1029.jpg','2018-06-19 20:11:36','pmurcia','e','post',0),
(299,NULL,'Birthday-Cake.jpg','2018-06-20 10:00:01','cocampo','e','bday',0),
(300,'Ya estamos planeando actividades para fin de año, ¿qué te gustaría? Fiesta elegante, día de actividades extremas, paseo o ¿qué se te ocurre? Ya hablé con varios y dijeron que paseito, y ¿tú?','1529686139902-988.jpg','2018-06-22 11:52:27','oarciniegas','e','post',126),
(301,'¡Felicitaciones Inmov!','1530116092301-970.jpg','2018-06-27 11:16:00','slopez','e','post',0),
(302,'Vamos mi selección ??','1530194143477-237.jpg','2018-06-28 08:56:20','jorduz','e','post',6),
(303,'Aprovecha que es viernes hora de almuerzo y me cuentas que quieres para celebrar el fin de año. ¡Muchos ya opinaron!','1530292542326-562.jpg','2018-06-29 12:18:23','oarciniegas','e','post',5),
(304,'Sábado. Inmov Argentina trabajando.','1530399260803-518.jpg','2018-06-30 17:55:23','pmazzino','e','post',3),
(305,'?','1530903207503-692.jpg','2018-07-06 13:54:25','bpinilla','e','post',0),
(306,'No siempre le damos con el gusto al cliente... pero seguimos explorando y mejorando en el proceso. ??','1531312427228-346.jpg','2018-07-11 07:38:26','jpena','e','post',1),
(307,NULL,'Birthday-Cake.jpg','2018-07-12 10:00:01','jalba','e','bday',5),
(308,'Ganadores Polla mundialista INMOV...','1531675990820-1020.jpg','2018-07-15 12:33:51','slopez','e','post',5),
(309,NULL,'Birthday-Cake.jpg','2018-07-19 10:00:01','jlondono','e','bday',3),
(310,NULL,'Birthday-Cake.jpg','2018-07-21 10:00:01','agualtero','e','bday',1),
(311,NULL,'Birthday-Cake.jpg','2018-07-21 10:00:01','jtorres','e','bday',5),
(312,NULL,'Birthday-Cake.jpg','2018-07-23 10:00:01','dcadavid','e','bday',0),
(313,NULL,'Birthday-Cake.jpg','2018-07-24 10:00:01','scamargo','e','bday',0),
(314,NULL,'Birthday-Cake.jpg','2018-07-24 10:00:01','acuello','e','bday',5),
(315,NULL,'Birthday-Cake.jpg','2018-08-01 10:00:01','jbotero','e','bday',7),
(316,NULL,'Birthday-Cake.jpg','2018-08-10 10:00:01','jpulido','e','bday',1),
(317,NULL,'Birthday-Cake.jpg','2018-08-11 10:00:02','zdelgado','e','bday',6),
(318,'Tremendas palabras y ceremonia. Milton y Carolina se casaron en Salgar frente al mar. Felicidades todas!','1534631997400-1043.jpg','2018-08-18 17:41:18','jtorres','e','post',4),
(319,'Matri... De milton','1534632703276-124.jpg','2018-08-18 17:52:09','fburgos','e','post',15),
(320,NULL,'Birthday-Cake.jpg','2018-08-23 10:00:01','acarvajal','e','bday',4),
(321,'Buen miércoles para todos. Mañana tenemos votaciones de COPASST, Comite de Convivencia y Brigada de Emergencia. Atentos a ejercer su derecho.','1535546092080-618.jpg','2018-08-29 07:38:08','oarciniegas','e','post',0),
(322,'Mañana día de votaciones, ¿preparad@s?','1535570122881-783.jpg','2018-08-29 15:16:51','oarciniegas','e','post',0),
(323,NULL,'Birthday-Cake.jpg','2018-08-30 10:00:01','bpinilla','e','bday',5),
(324,NULL,'Birthday-Cake.jpg','2018-08-31 10:00:01','hrico','e','bday',0),
(325,'El mejor equipo de trabajo !\nCompras/Producción INMOV','1535770122553-179.jpg','2018-08-31 21:49:46','jlondono','e','post',29),
(326,NULL,'Birthday-Cake.jpg','2018-09-01 10:00:01','jherrera','e','bday',0),
(327,'Saludos para todos del equipo Inmov Estados Unidos!!','1536087373637-803.jpg','2018-09-04 13:56:59','jbotero','e','post',1),
(329,NULL,'Birthday-Cake.jpg','2018-09-13 10:00:01','mmiranda','e','bday',0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
