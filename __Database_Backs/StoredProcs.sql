DROP PROCEDURE IF EXISTS sp_company_users;
DELIMITER //
CREATE PROCEDURE sp_company_users
(IN id INT(11))
BEGIN
  SELECT `user`.* FROM `user` 
  INNER JOIN `usercompany` ON `usercompany`.`USER_UserId` = `user`.`UserId` 
  INNER JOIN `company` ON `usercompany`.`EMPRESA_EmpresaId` = `company`.`CompanyId` 
  WHERE `company`.`CompanyId` = id;
END //
DELIMITER ;

CALL  sp_company_users (13)
