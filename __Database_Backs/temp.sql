/*
SQLyog Ultimate v12.2.4 (64 bit)
MySQL - 5.6.34-log : Database - mydigita_dbv1
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`mydigita_dbv1` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `mydigita_dbv1`;

/*Table structure for table `news` */

DROP TABLE IF EXISTS `news`;

CREATE TABLE `news` (
  `NewsId` int(11) NOT NULL AUTO_INCREMENT,
  `NewsTitle` varchar(255) DEFAULT NULL,
  `NewsIntro` mediumtext,
  `NewsFull` mediumtext,
  `NewsCover` varchar(200) DEFAULT NULL,
  `NewsCreated` datetime DEFAULT NULL,
  `NewsCreated_by` int(11) DEFAULT NULL,
  `NewsPublishUp` datetime DEFAULT NULL,
  `NewsPublishDown` datetime DEFAULT '0000-00-00 00:00:00',
  `NewsStatus` enum('A','S') DEFAULT 'A',
  `NewsHits` int(3) DEFAULT '0',
  `idGallery` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`NewsId`),
  KEY `created_idx` (`NewsCreated_by`),
  KEY `idGallery` (`idGallery`),
  FULLTEXT KEY `NewName` (`NewsTitle`),
  FULLTEXT KEY `NewContent` (`NewsFull`),
  CONSTRAINT `NEWS_ibfk_1` FOREIGN KEY (`idGallery`) REFERENCES `newsgallery` (`idGallery`),
  CONSTRAINT `created` FOREIGN KEY (`NewsCreated_by`) REFERENCES `user` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
