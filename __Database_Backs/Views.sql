CREATE VIEW vw_userBasicInfo AS
SELECT  Us.UserId AS id,  Us.UserLogin AS username,  Us.UserPassword AS `password`,  Us.userEmail AS email, CONCAT( Us.UserName, ' ',  Us.UserLastName) AS fullname,  Us.UserAvatar AS pic, Pf.ProfileName AS `profile`, Pf.ProfileId AS `profileId`
FROM  `user` AS Us
INNER JOIN PROFILE AS Pf ON Pf.ProfileId = Us.profile_ProfileId;

SELECT * FROM vw_userBasicInfo
