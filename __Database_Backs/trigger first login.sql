DELIMITER $$
CREATE TRIGGER FirstLogin BEFORE UPDATE ON USER
    FOR EACH ROW
    BEGIN
        IF old.UserFirstLogin  IS NOT NULL  AND new.UserFirstLogin  IS NOT NULL THEN 
         SET new.UserFirstLogin = old.UserFirstLogin;
        END IF;
    END
$$
