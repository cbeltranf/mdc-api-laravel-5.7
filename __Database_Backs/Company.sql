/*
SQLyog Ultimate v12.2.4 (64 bit)
MySQL - 5.6.34-log : Database - mydigita_dbv1
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`mydigita_dbv1` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `mydigita_dbv1`;

/*Table structure for table `company` */

DROP TABLE IF EXISTS `company`;

CREATE TABLE `company` (
  `CompanyId` int(11) NOT NULL AUTO_INCREMENT,
  `CompanyName` char(150) DEFAULT NULL,
  `CompanyExtra` varchar(80) DEFAULT NULL,
  `CompanyCode` char(20) DEFAULT NULL,
  `CompanyStatus` enum('A','S') DEFAULT NULL,
  `Companylogo` varchar(100) DEFAULT 'nologo.png',
  `CompanyAlias` varchar(45) DEFAULT NULL,
  `CompanyAddress` varchar(150) DEFAULT NULL,
  `CompanyPhone` varchar(45) DEFAULT NULL,
  `CompanyLat` varchar(20) DEFAULT NULL,
  `CompanyLog` varchar(20) DEFAULT NULL,
  `CompanyMapPlaceID` varchar(80) DEFAULT NULL,
  `CompanyCityContry` varchar(45) DEFAULT NULL,
  `CompanyWeb` varchar(250) DEFAULT NULL,
  `CompanyFB` varchar(250) DEFAULT NULL,
  `CompanyIG` varchar(250) DEFAULT NULL,
  `CompanyIN` varchar(250) DEFAULT NULL,
  `CompanyTW` varchar(250) DEFAULT NULL,
  `CompanyYT` varchar(250) DEFAULT NULL,
  `CompanyGP` varchar(250) DEFAULT NULL,
  `CompanyColor` varchar(12) DEFAULT NULL,
  `CompanyColorText` varchar(12) DEFAULT NULL,
  `CompanyLogoClass` varchar(100) DEFAULT NULL,
  `CompanyLang` enum('es','en') DEFAULT 'es',
  PRIMARY KEY (`CompanyId`),
  UNIQUE KEY `CompanyAlias` (`CompanyAlias`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
