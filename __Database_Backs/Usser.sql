/*
SQLyog Ultimate v12.2.4 (64 bit)
MySQL - 5.6.34-log : Database - mydigita_dbv1
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`mydigita_dbv1` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `mydigita_dbv1`;

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `UserId` int(11) NOT NULL AUTO_INCREMENT,
  `UserCedula` int(11) DEFAULT NULL,
  `UserName` varchar(100) DEFAULT NULL,
  `UserLastName` varchar(100) DEFAULT NULL,
  `UserLogin` varchar(45) NOT NULL,
  `UserPassword` varchar(35) DEFAULT NULL,
  `UserStatus` enum('A','S') DEFAULT 'A',
  `UserCountryPhoneCode` varchar(4) DEFAULT '+57',
  `UserMobile` varchar(45) DEFAULT NULL,
  `PROFILE_ProfileId` int(11) NOT NULL DEFAULT '2',
  `UserNotification` enum('SI','NO') DEFAULT 'NO',
  `UserAvatar` varchar(60) DEFAULT 'nouser.png',
  `UserUpdated` int(2) DEFAULT '0',
  `UserFirstLogin` datetime DEFAULT NULL,
  `UserLastLogin` datetime DEFAULT NULL,
  `UserUpdateTime` datetime DEFAULT NULL,
  `UserLastOpenTime` datetime DEFAULT NULL,
  `UserUUID` varchar(80) DEFAULT NULL,
  `UserModel` varchar(60) DEFAULT NULL,
  `UserPlatform` varchar(60) DEFAULT NULL,
  `UserVersion` varchar(60) DEFAULT NULL,
  `UserToken` varchar(134) DEFAULT NULL,
  `UserHits` int(3) DEFAULT '0',
  `UserBDay` date DEFAULT NULL,
  `UserLang` enum('es','en') DEFAULT 'es',
  PRIMARY KEY (`UserId`,`PROFILE_ProfileId`),
  UNIQUE KEY `UserLogin` (`UserLogin`),
  KEY `fk_USER_PROFILE1_idx` (`PROFILE_ProfileId`),
  CONSTRAINT `fk_USER_PROFILE1` FOREIGN KEY (`PROFILE_ProfileId`) REFERENCES `profile` (`ProfileId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1111 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
