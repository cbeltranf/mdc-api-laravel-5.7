<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class DocumentTypes extends Model
{
    use SoftDeletes;
    protected $table = "documenttypes";
    protected $fillable = [ 'DocumentTypesName',    'DocumentTypesState'];
    protected $primaryKey = 'DocumentTypesId';
    protected $guarded = ['DocumentTypesId'];
    protected $dates = ['deleted_at'];

    public function CompanyDocumentss()
    {
        return $this->hasMany('App\Models\CompanyDocuments', 'DocumentTypes_DocumentTypesId', 'DocumentTypesId');

    }
}
