<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentPlans extends Model
{
    use SoftDeletes;
    protected $table = "paymentplans";
    protected $fillable = ['Plans_PlansId',    'Company_CompanyId' ,    'PaymentPlansPrice',    'paymentPlansDate',    'paymentPlansReference',    'paymentPlansState'];
    protected $primaryKey = 'PaymentPlansId';
    protected $guarded = ['PaymentPlansId'];
    protected $dates = ['deleted_at'];



    public function Plans()
    {
        return $this->belongsTo('App\Models\Plans', 'Plans_PlansId', 'PlansId');

    }

}
