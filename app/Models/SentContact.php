<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SentContact extends Model
{
    use SoftDeletes;

    protected $table = "sentcontact";
    protected $fillable = ['CompanyDocuments_CompanyDocumentsId', 'User_UserId', 'CompanyCards_CompanyCardsId', 'Contact_ContactId', 'SentContactDate',  'SentContactLat',  'SentContactLon'];
    protected $primaryKey = 'SentContactId';
    protected $guarded = ['SentContactId'];
    protected $dates = ['deleted_at'];


    public function Contact()
    {
        return $this->belongsTo('App\Models\Contact', 'Contact_ContactId', 'ContactId');
    }

    public function CompanyDocuments()
    {
        return $this->belongsTo('App\Models\CompanyDocuments', 'CompanyDocuments_CompanyDocumentsId', 'SentContactId');
    }

    public function CompanyCards()
    {
        return $this->belongsTo('App\Models\CompanyCards', 'CompanyDocuments_CompanyDocumentsId', 'SentContactId');
    }

    public function User()
    {
        return $this->belongsTo('App\Models\User', 'User_UserId', 'UserId');
    }
}
