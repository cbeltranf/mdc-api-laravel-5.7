<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    use SoftDeletes;
    protected $table = "user";
    protected $guarded = ['UserId'];
    public $timestamps = false;
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'UserId';
    protected $fillable = ['UserCedula', 'UserPassword', 'UserName', 'UserLastName', 'UserEmail', 'UserLogin', 'UserStatus', 'UserCountryPhoneCode', 'UserMobile', 'PROFILE_ProfileId', 'UserNotification', 'UserAvatar', 'UserUpdated', 'UserFirstLogin', 'UserLastLogin', 'UserUpdateTime', 'UserLastOpenTime', 'UserUUID', 'UserModel', 'UserPlatform', 'UserVersion', 'UserToken', 'UserHits', 'UserBDay', 'UserLang'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     *//*
    protected $hidden = [
        'UserPassword'
    ];
*/
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getAuthPassword()
    {
        return $this->UserPassword;
    }

    public static function UserCompany($id)
    {
        $query = User::where('user.UserId', $id)
            ->join('usercompany', 'usercompany.USER_UserId', '=', 'user.UserId')
            ->join('company', 'usercompany.EMPRESA_EmpresaId', '=', 'company.CompanyId')
            ->select('company.*', 'usercompany.*')->get();
        return $query;
    }

    public function scopeGetPostion($query)
    {
        $query->select("user.*", "usercompany.POSITION_PositionId as PositionId", "usercompany.USERCOMPANYdefault as CompanyId")
            ->join('usercompany', 'usercompany.USER_UserId', '=', 'user.UserId');
    }

    public function scopeGetByCompany($query, $id)
    {
        $query->where('company.CompanyId', $id)
            ->join('usercompany', 'usercompany.USER_UserId', '=', 'user.UserId')
            ->join('company', 'usercompany.EMPRESA_EmpresaId', '=', 'company.CompanyId')
            ->select("user.*", "usercompany.POSITION_PositionId as PositionId", "company.CompanyName");
    }

    public function scopeGetUserAll($query)
    {
        $query->where('usercompany.USER_UserId', $this->UserId)
            ->join('usercompany', 'usercompany.USER_UserId', '=', 'user.UserId')
            ->join('company', 'usercompany.EMPRESA_EmpresaId', '=', 'company.CompanyId')
            ->join('position', 'position.PositionId', '=', 'usercompany.POSITION_PositionId')
            ->select("user.*", "usercompany.POSITION_PositionId as PositionId", "company.CompanyName","position.PositionName");
    }

    public function scopefilterValue($query, $param)
    {
        $query->orwhere("user.UserName", 'like', "%$param%");
        $query->orWhere("user.UserLastName", 'like', "%$param%");
        $query->orWhere("user.UserEmail", 'like', "%$param%");
        $query->orWhere("user.UserLogin", 'like', "%$param%");
        $query->orwhere("user.UserMobile", 'like', "%$param%");
        $query->orWhere("user.UserCedula", 'like', "%$param%");
    }


    public function Profile()
    {
        return $this->belongsTo('App\Models\Profile', 'PROFILE_ProfileId', 'ProfileId');
    }

    /**
     * @deprecated v1 use userCompany
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function UserCompanys()
    {
        return $this->hasMany('App\Models\UserCompany', 'USER_UserId', 'User_id');

    }

    // TODO Cambiar nombre en migracion
    public function UserCompanyName()
    {
        return $this->belongsTo('App\Models\UserCompany', 'USER_UserId', 'USER_UserId');

    }

    public function Contacts()
    {
        return $this->hasMany('App\Models\Contact', 'ContactCreated_by', 'UserId');

    }

    public function SalesHelper()
    {
        return $this->hasMany('App\Models\SalesHelper', 'SalesHelperCreatedBy', 'UserId');

    }

    public function SentContacts()
    {
        return $this->hasMany('App\Models\SentContact', 'User_UserId', 'UserId');

    }

    public function News()
    {
        return $this->hasMany('App\Models\News', 'NewsCreated_by', 'UserId');

    }

    public function NewsLikes()
    {
        return $this->hasMany('App\Models\NewsLikes', 'idUser', 'UserId');

    }


}
