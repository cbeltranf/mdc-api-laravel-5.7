<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;
    protected $table = "company";
    protected $fillable = ['CompanyName', 'CompanyExtra', 'CompanyCode', 'CompanyStatus', 'Companylogo', 'CompanyAlias', 'CompanyAddress', 'CompanyPhone', 'CompanyLat', 'CompanyLog', 'CompanyMapPlaceID', 'CompanyCityContry', 'CompanyWeb', 'CompanyFB', 'CompanyIG', 'CompanyIN', 'CompanyTW', 'CompanyYT', 'CompanyGP', 'CompanyColor', 'CompanyColorText', 'CompanyLogoClass', 'CompanyLang'];
    public $timestamps = false;
    protected $primaryKey = 'CompanyId';
    protected $dates = ['deleted_at'];

    protected $guarded = ['CompanyId'];

    public function UserCompanys()
    {
        return $this->hasMany('App\Models\UserCompany', 'EMPRESA_EmpresaId', 'CompanyId');

    }

    public function CompanyCardss()
    {
        return $this->hasMany('App\Models\CompanyCards', 'Company_CompanyId', 'CompanyId');

    }

    public function CompanyDocumentss()
    {
        return $this->hasMany('App\Models\CompanyDocuments', 'Company_CompanyId', 'CompanyId');

    }

    public function SalesHelpers()
    {
        return $this->hasMany('App\Models\SalesHelper', 'SalesHelperCompany_CompanyId', 'CompanyId');

    }

    public function scopefilterValue($query, $param)
    {
        $query->orWhere("CompanyName", 'like', "%$param%");
        $query->orWhere("CompanyExtra", 'like', "%$param%");
        $query->orWhere("CompanyAlias", 'like', "%$param%");
        $query->orWhere("CompanyAddress", 'like', "%$param%");
        $query->orwhere("CompanyCityContry", 'like', "%$param%");
        $query->orWhere("CompanyWeb", 'like', "%$param%");
    }

}
