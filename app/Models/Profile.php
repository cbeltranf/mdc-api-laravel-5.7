<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends Model
{
    use SoftDeletes;
    protected $table = "profile";
    protected $fillable = ['ProfileName', 'ProfileStatus', 'ProfileApp'];
    protected $guarded = ['ProfileId'];
    public $timestamps = false;
    protected $primaryKey = 'ProfileId';
    protected $dates = ['deleted_at'];

    public function Users()
    {
        return $this->hasMany('App\Models\User', 'PROFILE_ProfileId', 'ProfileId');

    }

    public function scopefilterValue($query, $param)
    {
        $query->orWhere("ProfileName", 'like', "%$param%");
    }

}
