<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Plans extends Model
{
    use SoftDeletes;
    protected $table = "plans";
    protected $fillable = ['PlansName',  'PlansDescription',  'PlansPrice' ,  'PlansTime',  'PlansUsersQuantity' ];
    protected $primaryKey = 'PlansId';
    protected $guarded = ['PlansId'];
    protected $dates = ['deleted_at'];

    public function PaymentPlanss()
    {
        return $this->hasMany('App\Models\PaymentPlans', 'Plans_PlansId', 'PlansId');

    }
}
