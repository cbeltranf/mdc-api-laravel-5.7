<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class CompanyCards extends Model
{
    use SoftDeletes;

    protected $table = "companycards";
    protected $fillable = ['Company_CompanyId',  'CompanyCardsName',  'CompanyCardsInfo',  'CompanyCardsState'];
    protected $primaryKey = 'CompanyCardsId';
    protected $guarded = ['CompanyCardsId'];
    protected $dates = ['deleted_at'];


    public function Company()
    {
        return $this->belongsTo('App\Models\Company', 'Company_CompanyId', 'CompanyId');

    }

    public function SentContacts()
    {
        return $this->hasMany('App\Models\SentContact', 'CompanyCards_CompanyCardsId', 'SentContactId');

    }


}
