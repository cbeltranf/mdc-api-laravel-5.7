<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NewsGallery extends Model
{
    use SoftDeletes;
    protected $table = "newsgallery";
    protected $fillable = ['galleryName', 'folder', 'hits'];
    public $timestamps = false;
    protected $primaryKey = 'idGallery';
    protected $guarded = ['idGallery'];
    protected $dates = ['deleted_at'];

    public function scopefilterValue($query, $param)
    {
            $query->orWhere("galleryName", 'like', "%$filterValue%");
            $query->orWhere("folder", 'like', "%$filterValue%");
    }

    public function News()
    {
        return $this->hasMany('App\Models\News', 'idGallery', 'idGallery');

    }

}
