<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    use SoftDeletes;
    protected $table = "news";
    protected $fillable = ['NewsTitle', 'NewsIntro', 'NewsCreated', 'NewsFull' , 'NewsCover', 'NewsCreated_by', 'NewsPublishUp', 'NewsPublishDown', 'NewsStatus', 'NewsHits', 'idGallery'];
    public $timestamps = false;
    protected $primaryKey = 'NewsId';
    protected $guarded = ['NewsId'];
    protected $dates = ['deleted_at',  'NewsCreated', 'NewsPublishUp', 'NewsPublishDown'];

    public function NewsLikes()
    {
        return $this->hasMany('App\Models\NewsLikes', 'idNews', 'NewsId');

    }

    public function NewsGallery()
    {
        return $this->belongsTo('App\Models\NewsGallery', 'idGallery', 'idGallery');

    }

    public function User()
    {
        return $this->belongsTo('App\Models\User', 'NewsCreated_by', 'UserId');

    }

    public function scopefilterValue($query, $param)
    {
            $query->orWhere("NewsTitle", 'like', "%$param%");
            $query->orWhere("NewsIntro", 'like', "%$param%");
            $query->orWhere("NewsFull", 'like', "%$param%");
    }
}
