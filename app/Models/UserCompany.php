<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\MultiplePrimaryKey;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserCompany extends Model
{
    use SoftDeletes;
    protected $table = "usercompany";
    protected $fillable = ['Empresa_EmpresaId', 'USER_UserId', 'POSITION_PositionId', 'USERCOMPANYdefault', 'USER_Mail'];
    public $timestamps = false;
    protected $primaryKey = ['Empresa_EmpresaId', 'USER_UserId'];
    public $incrementing = false;
    protected $dates = ['deleted_at'];


    public function User()
    {
        return $this->belongsTo('App\Models\User', 'USER_UserId', 'UserId');

    }

    public function Company()
    {
        return $this->belongsTo('App\Models\Company', 'EMPRESA_EmpresaId', 'CompanyId');

    }

    public function Position()
    {
        return $this->belongsTo('App\Models\Position', 'POSITION_PositionId', 'PositionId');

    }


}
