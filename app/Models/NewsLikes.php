<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NewsLikes extends Model
{

    use SoftDeletes;
    protected $table = "newslikes";
    protected $fillable = ['idNews', 'newsStatsLikes', 'idUser', 'timeStamp'];
    public $timestamps = false;
    protected $primaryKey = 'idNewsStats';
    protected $guarded = ['idNewsStats'];
    protected $dates = ['deleted_at',  'timeStamp'];

    public function User()
    {
        return $this->belongsTo('App\Models\User', 'idUser', 'UserId');
    }

    public function NewsGallery()
    {
        return $this->belongsTo('App\Models\News', 'idNews', 'NewsId');

    }
}
