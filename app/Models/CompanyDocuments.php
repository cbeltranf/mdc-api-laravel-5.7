<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyDocuments extends Model
{
    use SoftDeletes;

    protected $table = "companydocuments";
    protected $fillable = [ 'Company_CompanyId',   'DocumentTypes_DocumentTypesId',    'CompanyDocumentsName',    'CompanyDocumentsFile',    'CompanyDocumentsDescription',    'CompanyDocumentsNameState'];
    protected $primaryKey = 'CompanyDocumentsId';
    protected $guarded = ['CompanyDocumentsId'];
    protected $dates = ['deleted_at'];


    public function Company()
    {
        return $this->belongsTo('App\Models\Company', 'Company_CompanyId', 'CompanyId');

    }

    public function DocumentTypes()
    {
        return $this->belongsTo('App\Models\DocumentTypes', 'DocumentTypes_DocumentTypesId', 'DocumentTypesId');

    }

    public function SentContacts()
    {
        return $this->hasMany('App\Models\SentContact', 'CompanyDocuments_CompanyDocumentsId', 'SentContactId');
    }
}
