<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Position extends Model
{ 
    use SoftDeletes;
    protected $table = "position";
    protected $fillable = ['PositionName', 'CompanyId'];
    public $timestamps = false;
    protected $primaryKey = 'PositionId';
    protected $guarded = ['PositionId'];
    protected $dates = ['deleted_at'];

    public function UserCompanys()
    {
        return $this->hasMany('App\Models\UserCompany', 'POSITION_PositionId', 'PositionId');

    }
    public function scopefilterValue($query, $param)
    {
        $query->orWhere("PositionName", 'like', "%$param%");
        $query->orWhere("CompanyId", 'like', "%$param%");
    }
}
