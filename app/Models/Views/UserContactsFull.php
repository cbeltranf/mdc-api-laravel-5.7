<?php

namespace App\Models\Views;

use Illuminate\Database\Eloquent\Model;

class UserContactsFull extends Model
{
    protected $table = 'VS_USERCONTACTFULL';
}
