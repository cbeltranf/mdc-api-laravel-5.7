<?php

namespace App\Models\Views;

use Illuminate\Database\Eloquent\Model;

class UsersBasicInfo extends Model
{
    protected $table = 'vw_userbasicinfo';

}
