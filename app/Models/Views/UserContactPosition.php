<?php

namespace App\Models\Views;

use Illuminate\Database\Eloquent\Model;

class UserContactPosition extends Model
{
   protected $table = 'VS_USERCONTACTPOSITION';
}
