<?php

namespace App\Models\Views;

use Illuminate\Database\Eloquent\Model;

class UserContacts extends Model
{
    protected $table = 'VS_USERCONTACT';
}
