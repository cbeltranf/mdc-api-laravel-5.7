<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalesHelper extends Model
{
    use SoftDeletes;
    protected $table = "saleshelper";
    protected $fillable = [ 'SalesHelperTitle',    'SalesHelperIntro',    'SalesHelperFull',    'SalesHelperCover',    'SalesHelperCompany_CompanyId',    'SalesHelperCreated',    'SalesHelperCreatedBy',    'SalesHelperPublishUp',    'SalesHelperTitlePublishDown',    'SalesHelperTitleStatus',    'SalesHelperHits'];
    protected $primaryKey = 'SalesHelperId';
    protected $guarded = ['SalesHelperId'];
    protected $dates = ['deleted_at'];

    public function Company()
    {
        return $this->belongsTo('App\Models\Company', 'SalesHelperCompany_CompanyId', 'CompanyId');

    }

    public function User()
    {
        return $this->belongsTo('App\Models\User', 'SalesHelperCreatedBy', 'UserId');

    }

}

