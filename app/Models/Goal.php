<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Goal extends Model
{
    use SoftDeletes;
    protected $table = "goal";
    protected $fillable = ['GoalName','GoalStatus','GoalQuantity'];
    protected $guarded = ['GoalId'];
    public $timestamps = false;
    protected $primaryKey = 'GoalId';
    protected $dates = ['deleted_at'];

}
