<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = "contact";
    protected $guarded = ['ContactId'];
    protected $primaryKey = 'ContactId';
    protected $fillable = ['ContactCreated_by',    'ContactCreated',    'ContactPhone',    'ContactName',    'ContactCompany',    'ContactPosition'];

    public function User()
    {
        return $this->belongsTo('App\Models\User', 'ContactCreated_by', 'UserId');
    }

    public function SentContacts()
    {
        return $this->hasMany('App\Models\SentContact', 'Contact_ContactId', 'ContactId');

    }


}
