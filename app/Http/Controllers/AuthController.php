<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User as User;
use App\Models\Views\UsersBasicInfo as UserInfo;
use Illuminate\Http\Request;


class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = array('UserLogin' => $request->UserLogin, 'password' => $request->UserPassword);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized. Check Credentials']);
        }

        $Auth_user = auth()->user();
        $MyUser = User::where('UserId', $Auth_user->UserId)->first();
        $MyUser->UserLastLogin = $request->timeStamps;
        $MyUser->UserLang = $request->lang;
        $MyUser->UserModel = $request->model;
        $MyUser->UserPlatform = $request->plat;
        $MyUser->UserVersion = $request->ver;
        $MyUser->UserUUID = $request->uuid;
        $MyUser->UserToken = $token;

        $MyUser->UserFirstLogin = $request->timeStamps;
        $MyUser->save();

        $response = UserInfo::where('id', $Auth_user->UserId)->get()->toArray();
        $response = $response[0];
        $response['pic'] = asset('/profiles_images/' . $response['pic']);
        $roles[] = strtoupper($response["profile"]); //array("id"=>$response["profileId"], "name"=>strtoupper($response["profile"]));
        $roles[] = "ADMIN"; //array("id"=>2, "name"=>"ADMIN");
        $response["expires_in"] = auth()->factory()->getTTL() * 60;
        $response["accessToken"] = $token;
        $response["refreshToken"] = $token;
        $response["roles"] = $roles;
        unset($response["profileId"]);
        unset($response["profile"]);
        return response()->json($response);

    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function loginApp(Request $request)
    {
        $credentials = array('UserLogin' => $request->UserLogin, 'password' => $request->UserPassword);


        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized. Access Denied'], 401);
        }
        $Auth_user = auth()->user();
        $MyUser = User::where('UserId', $Auth_user->UserId)->first();
        $MyUser->UserLastLogin = $request->timeStamps;
        $MyUser->UserLang = $request->lang;
        $MyUser->UserModel = $request->model;
        $MyUser->UserPlatform = $request->plat;
        $MyUser->UserVersion = $request->ver;
        $MyUser->UserUUID = $request->uuid;
        $MyUser->UserToken = $token;
        $MyUser->UserFirstLogin = $request->timeStamps;
        $MyUser->save();

        return $this->respondWithToken($token);
    }


    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    public function invalidate()
    {
        auth()->invalidate();

        // Pass true as the first param to force the token to be blacklisted "forever".
        auth()->invalidate(true);

        return response()->json(['message' => 'Invalid Token.']);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
