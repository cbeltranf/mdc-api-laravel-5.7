<?php

namespace App\Http\Controllers;

use App\Models\NewsGallery;
use Illuminate\Http\Request;
use App\Http\Resources\GlobalCollection;

class NewsGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->input("filterColumn");
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");
        $sortField = $request->input("sortField");
        $sortOrder = ($request->input("sortOrder") == "asc") ? "asc" : "desc";
        if (empty($sortField)) {
            $sortField = "galleryName";
        }

        $item = NewsGallery::orderBy($sortField, $sortOrder);
        if (empty($filter) || $filter == "*") {
            $item->filterValue($filterValue);
        } else {
            $item->where($filter, 'like', "%$filterValue%");
        }

        if (empty($pageSize)) {$pageSize = 10;}

        return new GlobalCollection($item->paginate($pageSize));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\NewsGallery  $newsGallery
     * @return \Illuminate\Http\Response
     */
    public function show(NewsGallery $newsGallery)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\NewsGallery  $newsGallery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NewsGallery $newsGallery)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\NewsGallery  $newsGallery
     * @return \Illuminate\Http\Response
     */
    public function destroy(NewsGallery $newsGallery)
    {
        //
    }
}
