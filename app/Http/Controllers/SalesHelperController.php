<?php

namespace App\Http\Controllers;

use App\Models\SalesHelper;
use Illuminate\Http\Request;

class SalesHelperController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SalesHelper  $salesHelper
     * @return \Illuminate\Http\Response
     */
    public function show(SalesHelper $salesHelper)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SalesHelper  $salesHelper
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SalesHelper $salesHelper)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SalesHelper  $salesHelper
     * @return \Illuminate\Http\Response
     */
    public function destroy(SalesHelper $salesHelper)
    {
        //
    }
}
