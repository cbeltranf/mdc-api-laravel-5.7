<?php

namespace App\Http\Controllers;

use App\Http\Resources\GlobalCollection;
use App\Models\News;
use Carbon\Carbon;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->input("filterColumn");
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");
        $sortField = $request->input("sortField");
        $sortOrder = ($request->input("sortOrder") == "asc") ? "asc" : "desc";
        if (empty($sortField)) {
            $sortField = "NewsTitle";
        }

        $item = News::orderBy($sortField, $sortOrder);
        if (empty($filter) || $filter == "*") {
            $item->filterValue($filterValue);
        } else {
            $item->where($filter, 'like', "%$filterValue%");
        }

        if (empty($pageSize)) {
            $pageSize = 10;
        }

        return new GlobalCollection($item->paginate($pageSize));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            "NewsTitle" => "required|max:255",
            "NewsIntro" => "required",
            "NewsFull" => "required",
            "NewsPublishUp" => "required",
            "NewsCreated" => "required",
            "NewsCover" => "required|file|max:5120|mimes:jpg,gif,jpeg,png",

            "NewsPublishDown" => "nullable",
            "NewsStatus" => "nullable",
            "idGallery" => "nullable"
        ]);

        $new_name = time() . '.' . $request->NewsCover->extension();
        $path = $request->NewsCover->storeAs('news_images', $new_name, 'public');

        $data["NewsCover"] = $path;

        if (!empty($request->input("NewsCreated"))) {
            $data["NewsCreated"] = new Carbon($request->input("NewsCreated"));
            $data["NewsCreated"] = $data["NewsCreated"]->format('Y-m-d');
        }
        if (!empty($request->input("NewsPublishUp"))) {
            $data["NewsPublishUp"] = new Carbon($request->input("NewsPublishUp"));
            $data["NewsPublishUp"] = $data["NewsPublishUp"]->format('Y-m-d');
        }
        if (!empty($request->input("NewsPublishDown"))) {
            $data["NewsPublishDown"] = new Carbon($request->input("NewsPublishDown"));
            $data["NewsPublishDown"] = $data["NewsPublishDown"]->format('Y-m-d');
        }


        $InsertId = News::insertGetId($data);
        $inserted = News::where("NewsId", $InsertId)->get();


        return response()->json($inserted);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\News $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        return response()->json($news);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\News $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        $format = 'Y-m-d';
        $inputs = $request->validate([
            "NewsTitle" => "required|max:255",
            "NewsIntro" => "required",
            "NewsFull" => "required",
            "NewsPublishUp" => "required",
            "NewsCover" => "nullable|file|max:5120|mimes:jpg,gif,jpeg,png",

            "NewsPublishDown" => "nullable",
            "NewsStatus" => "nullable",
            "idGallery" => "nullable"
        ]);

        // $inputs = $request->except("NewsCover", "_method");
        if (!empty($request->file("NewsCover"))) {
            $new_name = time() . '.' . $request->NewsCover->extension();
            $path = $request->NewsCover->storeAs('news_images', $new_name, 'public');
            $inputs["NewsCover"] = $path;
        }

        if (!empty($request->input("NewsPublishUp"))) {
            $inputs["NewsPublishUp"] = new Carbon($request->input("NewsPublishUp"));
            $inputs["NewsPublishUp"] = $inputs["NewsPublishUp"]->format('Y-m-d H:m:i');
        }

        if (!empty($request->input("NewsPublishDown"))) {
            $inputs["NewsPublishDown"] = new Carbon($request->input("NewsPublishDown"));
            $inputs["NewsPublishDown"] = $inputs["NewsPublishDown"]->format('Y-m-d H:m:i');
        }

        $news->update($inputs);

        return response()->json($news);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\News $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        $item = $news->delete();

        $response["deleted"] = $item;
        if ($item) {
            $response["status"] = 200;
        } else {
            $response["status"] = 401;
        }

        return response()->json($response);
    }

    public function uploadTMP(Request $request)
    {
//        dd($request);
        if (!empty($request->file("file"))) {
            $new_name = time() . '.' . $request->file->extension();
            $path = $request->file->storeAs('tmp_news_images', $new_name, 'public');
            $response["status"] = true;
            $response["originalName"] = true;
            $response["generatedName"] = true;
            $response["msg"] = true;
            $response["imageUrl"] = asset($path);
        }
        return response()->json($response);
    }
}
