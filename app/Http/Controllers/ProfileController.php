<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use Illuminate\Http\Request;
use App\Http\Resources\ProfileCollection;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->input("filterColumn");
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");
        $sortField = $request->input("sortField");
        $sortOrder = ($request->input("sortOrder") == "asc") ? "asc" : "desc";
        if (empty($sortField)) {
            $sortField = "ProfileName";
        }

        $position = Profile::orderBy($sortField, $sortOrder);

        if (empty($filter) || $filter == "*") {
            $position->filterValue($filterValue);
        } else {
            $position->where($filter, 'like', "%$filterValue%");
        }
        if (empty($pageSize)) {
            $pageSize = 10;
        }
        return new ProfileCollection($position->paginate($pageSize));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->validate([
            "ProfileName" => "required|max:45",
            "ProfileStatus" => "in:A,S",
            "ProfileApp" => "in:SI,NO",
        ]);

        $InsertId = Profile::insertGetId($data);


        $inserted = Profile::where("ProfileId", $InsertId)->get();
        return response()->json($inserted);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Profile $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
        return response()->json($profile);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Profile $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profile $profile)
    {
        $data = $request->validate([
            "ProfileName" => "required|max:45",
            "ProfileStatus" => "in:A,S",
            "ProfileApp" => "in:SI,NO",
        ]);

        $profile->update($data);

        return response()->json($profile);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Profile $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        $item = $profile->delete();

        $response["deleted"] = $item;
        if ($item) {
            $response["status"] = 200;
        } else {
            $response["status"] = 401;
        }

        return response()->json($response);
    }
}
