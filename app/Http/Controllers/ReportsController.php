<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Views\ContactsCompany as vw_Contacts;
use App\Models\Views\UserContacts as kpi1;
use App\Models\Views\UserContactsFull as VS_USERCONTACTFULL;
use App\Models\Contact;
use App\Http\Resources\GlobalCollection;
use App\Models\User;
use Carbon\Carbon;
use DB;

class ReportsController extends Controller
{


    public function contacts(Request $request)
    {
        $request->validate([
            "startDate" => "required|date",
            "endDate" => "required|date"
        ]);

        $start = new Carbon($request->input("startDate"));
        $end = new Carbon($request->input("endDate"));

        $item = vw_Contacts::where('SentContactDate', '>=', $start->format('Y-m-d') . " 00:00:01")
            ->where('SentContactDate', '<=', $end->format('Y-m-d') . " 23:59:59");
        if (!empty($request->input("UserId"))) {
            $item->where("userId", $request->input("userId"));
            $company = User::UserCompany($request->input("userId"));
        } else {
            $session_user = auth()->user();
            $item->where("userId", $session_user->UserId);
            $company = User::UserCompany($session_user->UserId);
        }

        $item->where("CompanyId", $company[0]->CompanyId);
        $result["quantity"] = $item->count();

        return response()->json($result);
    }


    public function companies(Request $request)
    {
        $data = $request->validate([
            "startDate" => "required|date",
            "endDate" => "required|date",
            "companyId" => "nullable",
            "cantUsers" => "nullable",
            "orderBy" => "nullable"
        ]);

        $start = new Carbon($request->input("startDate"));
        $end = new Carbon($request->input("endDate"));

        $item = vw_Contacts::where('SentContactDate', '>=', $start->format('Y-m-d') . " 00:00:01")
            ->where('SentContactDate', '<=', $end->format('Y-m-d') . " 23:59:59");

        if (empty($request->input("companyId"))) {
            $session_user = auth()->user();
            $company = User::UserCompany($session_user->UserId);
            $item->where("CompanyId", $company[0]->CompanyId);
        } else {
            $item->where("CompanyId", $request->input("companyId"));
        }

        if (empty($request->input("orderBy"))) { // max default
            $item->orderBy('cant', 'desc');
        } else {
            $item->orderBy('cant', ($request->input("orderBy") == "max") ? "desc" : "asc");
        }

        if (!empty($request->input("cantUsers"))) {
            $item->limit($request->input("cantUsers"));
        }

        $result["result"] = $item->groupBy("UserCedula", "UserName", "UserLastName")->select(DB::raw('count(sentContactId) as cant'), "UserCedula", "UserName", "UserLastName")->get();

        return response()->json($result);
    }


    public function news(Request $request)
    {
        //
    }

    public function dashboard(Request $request)
    {
        $result = kpi1::all();
        return response()->json($result);
    }

    public function kpi2(Request $request)
    {
        $results = DB::select('select * from VS_PROFILEDASH ');
        return response()->json($results);
    }

    public function contactGeneral(Request $request)
    {
        if (!empty($request->get("startDate")) && !empty($request->get("endDate"))) {
            $start = new Carbon($request->get("startDate") ?: today());
            $end = new Carbon($request->get("endDate") ?: today());
        }
        $userName = $request->get("UserName") ?: "";
        $query = VS_USERCONTACTFULL::query();
        if (!empty($start))
            $query = $query->where('ContactCreated', '>=', $start->format('Y-m-d') . " 00:00:01");
        if (!empty($end))
            $query = $query->where('ContactCreated', '<=', $end->format('Y-m-d') . " 23:59:59");
        if (!empty($userName))
            $query = $query->where("UserName", 'like', "%$userName%");
        $query = $query->groupBy("UserName")->select(DB::raw('count(UserName) as Count'), "UserName", "UserId", "UserAvatar","PositionName");
        $data = $query->get()->toArray();
        $data = array_map(function ($d) {
            $d['UserAvatar'] = asset('/profiles_images/' . $d['UserAvatar']);
            return $d;
        }, $data);

        return response()->json(['data' => $data]);
    }

    public function contactDetail(Request $request, $id)
    {
        $start = $request->input("startDate");
        $end = $request->input("endDate");
        $pageSize = $request->input("pageSize");
        $sortField = $request->input("sortField");
        $sortOrder = ($request->input("sortOrder") == "asc") ? "asc" : "desc";

        if (empty($sortField)) {
            $sortField = "ContactName";
        }

        $item = VS_USERCONTACTFULL::orderBy($sortField, $sortOrder)->where('userId', $id);


        if (!empty($start)) {
            $start = new Carbon($start);
            $start = $start->format('Y-m-d');
            $item->where('ContactCreated', '>=', $start . " 00:00:01");
        }
        if (!empty($end)) {
            $end = new Carbon($end);
            $end = $end->format('Y-m-d');
            $item->where('ContactCreated', '<=', $end . " 00:00:01");
        }

        if (empty($pageSize)) {
            $pageSize = 10;
        }

        return new GlobalCollection($item->paginate($pageSize));

    }

    public function points(Request $request)
    {
        $query = VS_USERCONTACTFULL::query();
        $query = $query->where('ContactMonth', '=', date('m'));
        $query = $query->whereNotNull('ContactLat');
        $query = $query->whereNotNull("ContactLong");
        $query = $query->select('ContactLat', "ContactLong", "ContactName", "ContactCreated", "UserName");
        return response()->json($query->get());
    }


    public function companiesContacts(Request $request)
    {
        $query = Contact::groupBy('ContactCompany');
        $query->select(DB::raw("COUNT(ContactId) as TOTAL"), "ContactCompany");
        $query->where('ContactCompany', '!=', "");
        $query->orderBy('TOTAL', 'DESC');
        return response()->json($query->get());
    }

    public function positionsContacts(Request $request)
    {
        $query = Contact::groupBy('ContactPosition');
        $query->select(DB::raw("count(ContactId) as TOTAL"), "ContactPosition");
        $query->where('ContactCompany', '!=', "");
        $query->orderBy('TOTAL', 'DESC');
        return response()->json($query->get());
    }


}
