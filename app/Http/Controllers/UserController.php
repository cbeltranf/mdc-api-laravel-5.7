<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserCollection;
use App\Models\User;
use App\Models\UserCompany;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use PhpParser\ErrorHandler\Collecting;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->input("filterColumn");
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");
        $sortField = $request->input("sortField");
        $sortOrder = ($request->input("sortOrder") == "asc") ? "asc" : "desc";
        if (empty($sortField)) {
            $sortField = "UserName";
        }

        $item = User::orderBy($sortField, $sortOrder);
        if (empty($filter) || $filter == "*") {

            $item->filterValue($filterValue);

        } else {
            $item->where($filter, 'like', "%$filterValue%");
        }

        if (empty($pageSize)) {
            $pageSize = 10;
        }

        return new UserCollection($item->GetPostion()->with('Profile')->paginate($pageSize));
    }

    public function CompanyUsers($id, Request $request)
    {

        $filter = $request->input("filterColumn");
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");
        $sortField = $request->input("sortField");
        $sortOrder = ($request->input("sortOrder") == "asc") ? "asc" : "desc";
        if (empty($sortField)) {
            $sortField = "user.UserName";
        }

        $item = User::orderBy($sortField, $sortOrder);
        if (empty($filter) || $filter == "*") {

            $item->filterValue($filterValue);

        } else {
            $item->where($filter, 'like', "%$filterValue%");
        }

        if (empty($pageSize)) {
            $pageSize = 10;
        }

        return new UserCollection(
            $item->GetByCompany($id)->paginate($pageSize)
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            "UserCedula" => "required|max:20",
            "UserName" => "required|max:100",
            "UserLastName" => "required|max:100",
            "UserEmail" => "required|max:200",
            "UserLogin" => "required|max:45",
            "UserPassword" => "required|max:255",
            "UserMobile" => "required|max:45",
            "PROFILE_ProfileId" => "required|integer",
            "UserLang" => "in:es,en",
            "PositionId" => "required|integer",
        ]);

        $inputs = $request->except("PositionId", "CompanyId");

        $session_user = auth()->user();
        $company = User::UserCompany($session_user->UserId);

        $InsertId = DB::transaction(function () use ($inputs, $company, $request) {

            $InsertId = User::insertGetId($inputs);

            $user_company = array(
                "EMPRESA_EmpresaId" => $request->get('CompanyId'),
                "USER_UserId" => $InsertId,
                "POSITION_PositionId" => $request->input("PositionId"),
                "USERCOMPANYdefault" => $request->get("CompanyId"),
                "USER_Mail" => $request->get("UserEmail"));
            UserCompany::insert($user_company);
            return compact('InsertId');
        }, 2);

        $insertedUser = User::with('Profile')->where("UserId", $InsertId)->getPostion()->get();

        return response()->json($insertedUser);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $_user = current($user->getUserAll()->get()->toArray());
        $_user['UserAvatar'] = asset('/profiles_images/' . $_user['UserAvatar']);
        return response()->json($_user);
    }

    public function userLoginValidate(Request $request)
    {
        $response["available"] = true;
        $data = $request->validate([
            "UserLogin" => "required|max:45",
        ]);
        $Count = User::where($data)->count();
        if ($Count > 0) {
            $response["available"] = false;
        }

        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {

        $request->validate([
            "UserCedula" => "required|max:20",
            "UserName" => "required|max:100",
            "UserLastName" => "required|max:100",
            "UserEmail" => "required|max:200",
            "UserLogin" => "required|max:45",
            "UserPassword" => "required|max:255",
            "UserMobile" => "required|max:45",
            "PROFILE_ProfileId" => "required|integer",
            "UserLang" => "in:es,en",
            "PositionId" => "required|integer",
        ]);

        $inputs = $request->except("PositionId");
        $session_user = auth()->user();
        $rowId = $session_user->UserId;
        $company = User::UserCompany($rowId);

        DB::transaction(function () use ($inputs, $company, $request, $user, $rowId) {

            $user->update($inputs);
            $dataUserCompany = array(
                "USER_UserId" => $request->get("UserId")
            );
            $userCompany = UserCompany::where($dataUserCompany)
                ->update([
                    "POSITION_PositionId" => $request->input("PositionId"),
                    "USER_Mail" => $request->get("UserEmail"),
                    "EMPRESA_EmpresaId" => $request->get('CompanyId')
                ]);

        }, 2);

        $UpdatedUser = User::with('Profile')->where("UserId", $rowId)->getPostion()->get();

        return response()->json($UpdatedUser);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $item = $user->delete();

        $response["deleted"] = $item;
        if ($item) {
            $response["status"] = 200;
        } else {
            $response["status"] = 401;
        }

        return response()->json($response);
    }

    public function status(Request $request)
    {
        $users = $request->get('userForUpdate');
        $searchUser = [];
        foreach ($users as $user) {
            $searchUser[] = $user['UserId'];
        }
        DB::table('user')
            ->whereIn('UserId', $searchUser)
            ->update(["UserStatus" => $request->get('newStatus')]);
        $response["status"] = 200;
        return response()->json($response);
    }

    public function goals(Request $request)
    {
        $users = $request->get('userForUpdate');
        $goal = $request->get('goal');
        $searchUser = [];
        foreach ($users as $user) {
            $searchUser[] = ['GoalID' => $goal, 'UserID' => $user['UserId']];
        }

        DB::table('goaluser')->insert($searchUser);
        $response["status"] = 200;
        return response()->json($response);
    }

    public function Notification($id, Request $request)
    {
        $user = User::where(['UserId' => $id])->update(['UserNotification' => $request->get('UserNotification')]);
        $response["status"] = 200;
        return response()->json($user);

    }


}
