<?php

namespace App\Http\Controllers;

use App\Models\Goal;
use Illuminate\Http\Request;
use App\Http\Resources\GoalCollection;

class GoalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return new GoalCollection(Goal::orderBy("GoalName", "asc")->paginate(15));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            "GoalName" => "required|max:45",
            "GoalStatus" => "in:A,S",
            "GoalQuantity" =>"required"
        ]);
        $InsertId = Goal::insertGetId($data);


        $inserted = Goal::where("GoalId", $InsertId)->get();
        return response()->json($inserted);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Goal $goal
     * @return \Illuminate\Http\Response
     */
    public function show(Goal $goal)
    {
        return response()->json($goal);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Goal $goal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Goal $goal)
    {
        $data = $request->validate([
            "GoalName" => "required|max:45",
            "GoalStatus" => "in:A,S",
            "GoalQuantity" =>"required"
        ]);

        $goal->update($data);

        return response()->json($goal);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Goal $goal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Goal $goal)
    {
        $item = $goal->delete();

        $response["deleted"] = $item;
        if ($item) {
            $response["status"] = 200;
        } else {
            $response["status"] = 401;
        }

        return response()->json($response);
    }
}
