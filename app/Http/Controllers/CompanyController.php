<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Http\Resources\CompanyCollection;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $filter = $request->input("filterColumn");
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");
        $sortField = $request->input("sortField");
        $sortOrder = ($request->input("sortOrder") == "asc") ? "asc" : "desc";

        if (empty($sortField)) {
            $sortField = "CompanyName";
        }

        $company = Company::orderBy($sortField, $sortOrder);

        if (empty($filter) || $filter == "*") {

            $company->filterValue($filterValue);

        } else {

            $company->where($filter, 'like', "%$filterValue%");

        }

        if (empty($pageSize)) {
            $pageSize = 10;
        }

        return new CompanyCollection($company->paginate($pageSize));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            "CompanyName" => "required|max:150",
            "CompanyStatus" => "in:A,S",
            "CompanyAddress" => "required|max:150",
            "CompanyLang" => "in:es,en",
            "CompanyAlias" => "required|unique:company|max:45",
            "Companylogo" => "file|max:5120|mimes:jpg,gif,jpeg,png",

            "CompanyExtra" => "nullable|max:80",
            "CompanyCode" => "nullable|max:20",
            "CompanyPhone" => "nullable|max:45",
            "CompanyLat" => "nullable|max:20",
            "CompanyMapPlaceID" => "nullable|max:80",
            "CompanyLog" => "nullable|max:20",
            "CompanyCityContry" => "nullable|max:45",
            "CompanyWeb" => "nullable|max:250",
            "CompanyFB" => "nullable|max:250",
            "CompanyIG" => "nullable|max:250",
            "CompanyIN" => "nullable|max:250",
            "CompanyTW" => "nullable|max:250",
            "CompanyYT" => "nullable|max:250",
            "CompanyGP" => "nullable|max:250",
            "CompanyColor" => "nullable|max:12",
            "CompanyColorText" => "nullable|max:12",
            "CompanyLogoClass" => "nullable|max:100"
        ]);

        // $data = $request->except("clear", "CompanyId");

        $new_name = time() . '.' . $request->Companylogo->extension();
        $path = $request->Companylogo->storeAs('company_images', $new_name, 'public');
        if (!$path)
            $data["Companylogo"] = 'nologo.png';
        else
            $data["Companylogo"] = $path;

        $InsertId = Company::insertGetId($data);
        $inserted = Company::where("CompanyId", $InsertId)->get();

        //$response["data"] = $inserted;
        return response()->json(current($inserted));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Company $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {

        return response()->json($company);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Company $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        $data = $request->validate([
            "CompanyName" => "required|max:150",
            "CompanyStatus" => "in:A,S",
            "CompanyAddress" => "required|max:150",
            "CompanyLang" => "in:es,en",
            "CompanyAlias" => "required|max:45",
//            "Companylogo" => "file|max:5120|mimes:jpg,gif,jpeg,png",

            "CompanyExtra" => "nullable|max:80",
            "CompanyCode" => "nullable|max:20",
            "CompanyPhone" => "nullable|max:45",
            "CompanyLat" => "nullable|max:20",
            "CompanyLog" => "nullable|max:20",
            "CompanyMapPlaceID" => "nullable|max:80",
            "CompanyCityContry" => "nullable|max:45",
            "CompanyWeb" => "nullable|max:250",
            "CompanyFB" => "nullable|max:250",
            "CompanyIG" => "nullable|max:250",
            "CompanyIN" => "nullable|max:250",
            "CompanyTW" => "nullable|max:250",
            "CompanyYT" => "nullable|max:250",
            "CompanyGP" => "nullable|max:250",
            "CompanyColor" => "nullable|max:12",
            "CompanyColorText" => "nullable|max:12",
            "CompanyLogoClass" => "nullable|max:100"
        ]);

        //$data = $request->except("Companylogo", "_method", "clear", "CompanyId");
        if (!empty($request->file("Companylogo"))) {
            $new_name = time() . '.' . $request->Companylogo->extension();
            $path = $request->Companylogo->storeAs('company_images', $new_name, 'public');
            $data["Companylogo"] = $path;
        }

        $company->update($data);

        //$response["data"] = $company;
        return response()->json($company);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Company $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        $item = $company->delete();

        $response["deleted"] = $item;
        if ($item) {
            $response["status"] = 200;
        } else {
            $response["status"] = 401;
        }

        return response()->json($response);
    }


    public function UpdateLocation($id, Request $request)
    {
        $updateCompany = [
            "CompanyLat" => $request->get("CompanyLat"),
            "CompanyLog" => $request->get("CompanyLog"),
            "CompanyMapPlaceID" => $request->get("CompanyMapPlaceID"),
        ];
        $company = Company::find($id)->update($updateCompany);
        return response()->json($company);
    }
}
