<?php

namespace App\Http\Controllers;

use App\Http\Resources\PositionCollection;
use App\Models\Position;
use App\Models\User;
use Illuminate\Http\Request;

class PositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->input("filterColumn");
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");
        $sortField = $request->input("sortField");
        $sortOrder = ($request->input("sortOrder") == "asc") ? "asc" : "desc";
        if (empty($sortField)) {
            $sortField = "PositionName";
        }

        $position = Position::orderBy($sortField, $sortOrder);

        if (empty($filter) || $filter == "*") {
            $position->filterValue($filterValue);
        } else {
            $position->where($filter, 'like', "%$filterValue%");
        }
        if (empty($pageSize)) {
            $pageSize = 10;
        }
        return new PositionCollection($position->paginate($pageSize));
    }

    function list()
    {

        $session_user = auth()->user();
        $company = User::UserCompany($session_user->UserId);
        $response = Position::select("PositionId", "PositionName")->where("CompanyId", $company[0]->CompanyId)->get();

        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            "PositionName" => "required|max:150"
        ]);

        $session_user = auth()->user();
        $rowId = $session_user->UserId;
        $company = User::UserCompany($rowId);
        $data["CompanyId"] = $company[0]->CompanyId;

        $InsertId = Position::insertGetId($data);


        $inserted = Position::where("PositionId", $InsertId)->get();
        return response()->json($inserted);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Position $position
     * @return \Illuminate\Http\Response
     */
    public function show(Position $position)
    {
        return response()->json($position);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Position $position
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Position $position)
    {
        $data = $request->validate([
            "PositionName" => "required|max:150"
        ]);

        $session_user = auth()->user();
        $rowId = $session_user->UserId;
        $company = User::UserCompany($rowId);
        $data["CompanyId"] = $company[0]->CompanyId;

        $position->update($data);
        return response()->json($position);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Position $position
     * @return \Illuminate\Http\Response
     */
    public function destroy(Position $position)
    {
        $item = $position->delete();

        $response["deleted"] = $item;
        if ($item) {
            $response["status"] = 200;
        } else {
            $response["status"] = 401;
        }

        return response()->json($response);
    }

}
