<?php

namespace App\Providers;

use Illuminate\Auth\EloquentUserProvider as UserProvider;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;


class InmovUserProvider extends UserProvider {

    public function validateCredentials(UserContract $user, array $credentials)
    {
        $plain = $credentials['password'];
        $hashed = $user->getAuthPassword();

        if (strlen($hashed) === 0) {
            return false;
        }

        if($hashed === md5($plain)){
            return true;
        }else{
            return false;
        }
    }

}
